﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Interpreter;
using Astro.Engine.Interpreter.ExecutionEnvironment;
using Astro.Engine.Parsing;
using Astro.Engine.Symbols;
using Astro.Engine.Tokenization;

namespace Astro.App
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window , IConsoleOutput, ICommandsEventListener
    {
        // Commands
        public ICommand BuildCommand { get; private set; }
        public ICommand RunCommand { get; private set; }
        public ICommand StopCommand { get; private set; }

        // Event listener events
        public event EventHandler<EventDetectedEventArgs> EventStartDetectedEvent;
        public event EventHandler<EventDetectedEventArgs> EventStopDetectedEvent;
        public event EventHandler<EventDetectedEventArgs> EventRepeatDetectedEvent;

        // Astro variables
        private InterpreterParseNodeFactory mNodeFactory;
        private InterpreterExecutionEnvironment mExecutionEnvironment;
        private SymbolTable mSymbolTable;
        private Tokenizer mTokenizer;
        private TokenStream mTokenStream;
        private Parser mParser;
        private ParseNode mProgramNode;
        private ErrorLog mErrorLog;

        // Program variables
        private Stopwatch mStopwatch = new Stopwatch();
        private bool mProgramRunning = false;
        private SolidColorBrush mErrorColor = new SolidColorBrush(Colors.Red);
        private SolidColorBrush mDefaultColor = new SolidColorBrush(Colors.Black);

        // Creates and initialises the program.
        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;

            // Initialise the commands
            this.BuildCommand = new RelayCommand(this.BuildCommand_Execute, () => true);
            this.RunCommand = new RelayCommand(this.RunCommand_Execute, () => true);
            this.StopCommand = new RelayCommand(this.StopCommand_Execute, () => mProgramRunning);

            // Initialise Astro variables
            mNodeFactory = new InterpreterParseNodeFactory();

            // Focus on the text editor
            this.TextEditor.Focus();
        }

        // Builds the program.
        private bool BuildProgram()
        {
            // Reset variables
            mErrorLog = new ErrorLog();
            mSymbolTable = new SymbolTable(mNodeFactory);
            mTokenizer = new Tokenizer(mSymbolTable, mErrorLog);
            mParser = new Parser(mSymbolTable, mNodeFactory, mErrorLog);

            // Check that the program is already running
            if (mProgramRunning)
            {
                mProgramRunning = false;
                mExecutionEnvironment.Dispose();
                this.AddOutputListBoxMessage("Stopped currently running program", mDefaultColor);
            }

            // Tokenize the source
            var source = this.TextEditor.Text;
            mStopwatch.Restart();
            var result = mTokenizer.Tokenize(source, out mTokenStream);
            mStopwatch.Stop();
            if (result)
            {
                this.AddOutputListBoxMessage(string.Format("Source Tokenized: {0}ms", mStopwatch.ElapsedMilliseconds), mDefaultColor);
            }
            else
            {
                this.AddOutputListBoxMessage("Error occurred during tokenization", mErrorColor);
                this.OutputErrorMessages();
                return false;
            }

            // Parse the program
            mStopwatch.Restart();
            result = mParser.Parse(mTokenStream, out mProgramNode);
            mStopwatch.Stop();
            if (result)
            {
                this.AddOutputListBoxMessage(string.Format("Tokens Parsed: {0}ms", mStopwatch.ElapsedMilliseconds), mDefaultColor);
            }
            else
            {
                this.AddOutputListBoxMessage("Error occurred during parsing", mErrorColor);
                this.OutputErrorMessages();
                return false;
            }

            return true;
        }

        // Add a message to the output list box.
        private void AddOutputListBoxMessage(string message, SolidColorBrush brush)
        {
            // Add the message
            this.OutputListBox.Items.Add(new ListBoxItem() 
            { 
                Content = new TextBlock() 
                { 
                    Text = message, Foreground = brush 
                } 
            });
            
            // Scroll to bottom
            var items = this.OutputListBox.Items;
            var bottomItem = items[items.Count - 1];
            this.OutputListBox.ScrollIntoView(bottomItem);
        }

        // Output the error log's messages to the output list box.
        private void OutputErrorMessages()
        {
            foreach (var entry in mErrorLog.Entries)
            {
                this.AddOutputListBoxMessage(entry.Message, mErrorColor);
                this.AddOutputListBoxMessage(string.Format("Line/Column: {0},{1}", entry.Line, entry.Column), mErrorColor);
            }
            mErrorLog.Entries.Clear();
        }

        // Prints a line
        public void PrintLine(string s)
        {
            this.Print(s);
            
            // Add a new line
            this.ConsoleListBox.Items.Add(new ListBoxItem() { Content = string.Empty });
        }

        // Prints
        public void Print(string s)
        {
            var items = this.ConsoleListBox.Items;

            // Ensure there is one item
            ListBoxItem item;
            if (items.Count == 0)
            {
                item = new ListBoxItem() { Content = string.Empty };
                items.Add(item);
            }
            else
            {
                item = (ListBoxItem)items[items.Count - 1];
            }

            // Add the message to the current line
            var line = (string)item.Content;
            line += s;
            item.Content = line;

            // Scroll to bottom
            var bottomItem = items[items.Count - 1];
            this.ConsoleListBox.ScrollIntoView(bottomItem);
        }

        #region Commands

        // Stops the program while it is executing
        private void StopCommand_Execute()
        {
            if (mProgramRunning)
            {
                mExecutionEnvironment.Dispose();
                mProgramRunning = false;

                this.AddOutputListBoxMessage("Program Execution Stopped", mDefaultColor);
            }
        }

        // Build the program
        private void BuildCommand_Execute()
        {
            this.OutputListBox.Items.Clear();
            this.BuildProgram();
        }

        // Build and run the program
        private void RunCommand_Execute()
        {
            this.OutputListBox.Items.Clear();
            this.ConsoleListBox.Items.Clear();

            // First build the program
            if (this.BuildProgram())
            {
                if (mExecutionEnvironment != null)
                {
                    mExecutionEnvironment.Dispose();
                }
                mExecutionEnvironment = new InterpreterExecutionEnvironment(mErrorLog, this, this);

                mStopwatch.Restart();
                var result = mExecutionEnvironment.ExecuteProgram((InterpreterParseNode)mProgramNode);
                mStopwatch.Stop();
                if (result)
                {
                    this.AddOutputListBoxMessage(string.Format("Program Executed: {0}ms", mStopwatch.ElapsedMilliseconds), mDefaultColor);
                }
                else
                {
                    this.AddOutputListBoxMessage("Error occurred during execution", mErrorColor);
                    this.OutputErrorMessages();
                    return;
                }

                // Check whether the program is still running
                if (mExecutionEnvironment.CommandsManager.HasCommands)
                {
                    mProgramRunning = true;
                    this.AddOutputListBoxMessage("Program is listening for events", mDefaultColor);

                    // Remove focus if we're listening for events
                    this.OutputListBox.Focus();
                }
                else
                {
                    mProgramRunning = false;
                }

            }
        }

        #endregion

        #region Events

        // Handles the window's preview key events
        private void Window_PreviewKey(object sender, KeyEventArgs e)
        {
            if (!mProgramRunning)
            {
                return;
            }

            // Get the key and check the state
            var key = e.Key.ToString();
            if (e.IsRepeat)
            {
                // Fire the repeat event
                if (this.EventRepeatDetectedEvent != null)
                {
                    this.EventRepeatDetectedEvent(this, new EventDetectedEventArgs(key));
                }
            }
            else if (e.IsDown)
            {
                // Fire the start event
                if (this.EventStartDetectedEvent != null)
                {
                    this.EventStartDetectedEvent(this, new EventDetectedEventArgs(key));
                }
            }
            else if (e.IsUp)
            {
                // Fire the stop event
                if (this.EventStopDetectedEvent != null)
                {
                    this.EventStopDetectedEvent(this, new EventDetectedEventArgs(key));
                }
            }

            // Print any errors
            if (mErrorLog.Entries.Count > 0)
            {
                this.AddOutputListBoxMessage("Error occured during event", mErrorColor);
                this.OutputErrorMessages();
            }
        }

        #endregion
    }
}
