﻿using System;
using System.Windows.Input;

namespace Astro.App
{
    class RelayCommand : ICommand
    {
        private Action mExecute;
        private Func<bool> mCanExecute;
        public delegate void EventExecute(EventArgs e);
        private EventExecute mEventExecute;

        public RelayCommand(Action execute, Func<bool> canExecute)
        {
            mExecute = execute;
            mEventExecute = null;
            mCanExecute = canExecute;
        }

        public RelayCommand(EventExecute eventExecute, Func<bool> canExecute)
        {
            mExecute = null;
            mEventExecute = eventExecute;
            mCanExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return mCanExecute();
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                if (this.mCanExecute != null)
                {
                    CommandManager.RequerySuggested += value;
                }
            }
            remove
            {
                if (this.mCanExecute != null)
                {
                    CommandManager.RequerySuggested -= value;
                }
            }
        }

        public void Execute(object parameter)
        {
            if (mExecute != null)
            {
                mExecute();
            }
            else
            {
                mEventExecute((EventArgs)parameter);
            }
        }
    }
}
