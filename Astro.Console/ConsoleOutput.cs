﻿using System;
using Astro.Engine.Interpreter.ExecutionEnvironment;

namespace Astro.Console
{
    /// <summary>
    /// Simple console output class.
    /// </summary>
    public class ConsoleOutput : IConsoleOutput
    {
        public void PrintLine(string s)
        {
            System.Console.WriteLine(s);
        }

        public void Print(string s)
        {
            System.Console.Write(s);
        }
    }
}
