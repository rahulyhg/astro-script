﻿using System;
using Astro.Engine.Tokenization;

namespace Astro.Engine.ErrorReporting
{
    /// <summary>
    /// Defines a syntax exception.
    /// </summary>
    public class InvalidSyntaxException : Exception
    {
        private ErrorCode mErrorCode;
        private Token mToken;

        /// <summary>
        /// Create a new invalid syntax exception.
        /// </summary>
        /// <param name="token">The token that this error occured at.<param>
        /// <param name="errorCode">The error code describing this syntax exception.</param>
        /// <param name="message">The message for this error.</param>
        public InvalidSyntaxException(Token token, ErrorCode errorCode, string message)
            : base(message)
        {
            mToken = token;
            mErrorCode = errorCode;
        }

        /// <summary>
        /// The token that this error occured at.
        /// </summary>
        public Token Token
        {
            get { return mToken; }
        }

        /// <summary>
        /// The error code describing this syntax exception.
        /// </summary>
        public ErrorCode ErrorCode
        {
            get { return mErrorCode; }
        }
    }
}
