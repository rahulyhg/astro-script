﻿
namespace Astro.Engine.ErrorReporting
{
    /// <summary>
    /// Defines errors that may occur.
    /// </summary>
    public enum ErrorCode
    {
        /// <summary>
        /// Error occurs when a closing brace is missing from a block.
        /// </summary>
        ClosingBraceMissing,

        /// <summary>
        /// Error occurs when an opening brace is missing from a block.
        /// </summary>
        OpeningBraceMissing,

        /// <summary>
        /// Error occurs when a closing parenthesis is missing from a parenthesis block.
        /// </summary>
        ClosingParenthesisMissing,

        /// <summary>
        /// Error occurs when an opening parenthesis is missing from a parenthesis block.
        /// </summary>
        OpeningParenthesisMissing,

        /// <summary>
        /// Error occurs when a closing bracket is missing from a bracket block.
        /// </summary>
        ClosingBracketMissing,

        /// <summary>
        /// Error occurs when an opening bracket is missing from a bracket block.
        /// </summary>
        OpeningBracketMissing,

        /// <summary>
        /// Error occurs when a floating point number is provided a decimal part but no following numbers.
        /// </summary>
        MissingFractionalNumbers,

        /// <summary>
        /// Error occurs when the expression parser encounters a term it cannot correctly parse.
        /// </summary>
        InvalidExpressionTerm,

        /// <summary>
        /// Error occurs when the parser expects the name of an object definition.
        /// </summary>
        ObjectDefinitionNameExpected,

        /// <summary>
        /// Error occurs when the parser expects a parameter call.
        /// </summary>
        ParameterCallExpected,

        /// <summary>
        /// Error occurs when the parser expects a parameter list.
        /// </summary>
        ParameterListExpected,

        /// <summary>
        /// Occurs when the parser expects a member name.
        /// </summary>
        MemberNameExpected,

        /// <summary>
        /// Occurs when the parser expects a name.
        /// </summary>
        NameExpected,

        /// <summary>
        /// Occurs when the parser encounters a second declaration of a variable.
        /// </summary>
        VariableAlreadyDeclared,

        /// <summary>
        /// Occurs when the parser attempts to create a symbol whose identifier is already in use.
        /// </summary>
        NameAlreadyInUse,

        /// <summary>
        /// Occurs when an expression is expected by the parser.
        /// </summary>
        ExpressionExpected,

        /// <summary>
        /// Occurs when a name is used which hasn't been declared in any scope so far.
        /// </summary>
        ScopeDoesNotContainName,

        /// <summary>
        /// Occurs when a function is used as a variable.
        /// </summary>
        FunctionUsedAsVariable,

        /// <summary>
        /// Occurs when an assignment operator is expected after a variable name.
        /// </summary>
        AssignmentOperatorExpected,

        /// <summary>
        /// Occurs when a parameter is expected in a parameter call.
        /// </summary>
        ParameterExpected,

        /// <summary>
        /// Occurs when an end of statement token is expected.
        /// </summary>
        EndOfStatementExpected,

        /// <summary>
        /// Occurs when the parser expects a block.
        /// </summary>
        BlockExpected,

        /// <summary>
        /// Occurs when a declaration is expected.
        /// </summary>
        DeclarationExpected,

        /// <summary>
        /// Occurs when a specific keyword is expected.
        /// </summary>
        KeywordExpected,

        /// <summary>
        /// Occurs when a variable is expected.
        /// </summary>
        VariableExpected,

        /// <summary>
        /// Occurs when both a prefix and a postfix increment/decrement are used on one variable.
        /// </summary>
        PrefixAndPostfix,

        /// <summary>
        /// Occurs when the parser encounters a second constructor for an object.
        /// </summary>
        ConstructorAlreadyExists,

        /// <summary>
        /// Occurs when a name which doesn't refer to an object is used as an object.
        /// </summary>
        NotAnObject,

        /// <summary>
        /// Occurs when the parser encounters a use of an object name it does not recognise.
        /// </summary>
        UnknownObjectName,

        /// <summary>
        /// Occurs when an accessor node cannot find a specific member on an object.
        /// </summary>
        DoesNotContainMember,

        /// <summary>
        /// Occurs when a return statement is used outside of a function block.
        /// </summary>
        ReturnNotInFunction,

        /// <summary>
        /// Occurs when a boolean data type is expected.
        /// </summary>
        NonBooleanDataType,

        /// <summary>
        /// Occurs when a numeric data type is expected.
        /// </summary>
        NonNumericDataType,

        /// <summary>
        /// Occurs when there is a type mismatch.
        /// </summary>
        TypeMismatch,

        /// <summary>
        /// Occurs when the parser expects a static variable declaration.
        /// </summary>
        StaticDeclarationExpected,

        /// <summary>
        /// Thrown when a dictionary is indexed with a key which is not found.
        /// </summary>
        KeyNotFound,

        /// <summary>
        /// Thrown when a key is added to a dictionary which already contains that key.
        /// </summary>
        KeyAlreadyExists,

        /// <summary>
        /// Thrown when a funciton is called with too few or too many parameters.
        /// </summary>
        InsufficientParameters,

        /// <summary>
        /// Thrown when a collection object is expected.
        /// </summary>
        CollectionObjectExpected,

        /// <summary>
        /// Thrown when the tokenizer encounters an unrecognised character.
        /// </summary>
        UnrecognisedCharacter,

        /// <summary>
        /// Thrown when an object's symbol is used as if it were a variable or a function.
        /// </summary>
        ObjectUsedAsVariableOrFunction,

        /// <summary>
        /// Thrown when the parser encounters an invalid statement.
        /// </summary>
        InvalidStatement,

        /// <summary>
        /// Thrown when the tokenizer does not find a double quotation mark.
        /// </summary>
        DoubleQuotationExpected,
    }
}