﻿using System;
using System.Collections.Generic;
using Astro.Engine.DataTypes;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Symbols
{
    /// <summary>
    /// Stores information on an object's definition.
    /// </summary>
    public class ObjectSymbol : Symbol
    {
        private bool mIsInitialised;
        private string[] mMemberVariables;
        private FunctionSymbol mConstructor;
        private Dictionary<string, DataSlot> mStaticVariables = new Dictionary<string, DataSlot>();
        private Dictionary<string, FunctionSymbol> mMemberFunctions = new Dictionary<string, FunctionSymbol>();
        private Dictionary<string, FunctionSymbol> mStaticFunctions = new Dictionary<string, FunctionSymbol>();
        private bool mHasConstructor;

        /// <summary>
        /// Creates an object symbol.
        /// </summary>
        /// <param name="token">This symbol's token.</param>
        /// <param name="scope">This symbol's scope.</param>
        public ObjectSymbol(Token token, Scope scope)
            :base(token, SymbolType.ObjectName, scope)
        { }

        /// <summary>
        /// Creates an object symbol.
        /// </summary>
        /// <param name="identifier">The identifier for this object symbol.</param>
        /// <param name="scope">This symbol's scope.</param>
        protected ObjectSymbol(string identifier, Scope scope)
            : base(identifier, SymbolType.ObjectName, scope)
        { }

        #region Properties

        /// <summary>
        /// Whether this object definition has been fully initialised.
        /// </summary>
        public bool IsInitialised
        {
            get { return mIsInitialised; }
        }

        /// <summary>
        /// This object's member variables.
        /// </summary>
        public string[] MemberVariables
        {
            get { return mMemberVariables; }
        }

        /// <summary>
        /// This object's member functions.
        /// </summary>
        public Dictionary<string, FunctionSymbol> MemberFunctions
        {
            get { return mMemberFunctions; }
        }

        /// <summary>
        /// This object's static functions.
        /// </summary>
        public Dictionary<string, FunctionSymbol> StaticFunctions
        {
            get { return mStaticFunctions; }
        }

        /// <summary>
        /// This object's constructor.
        /// </summary>
        public FunctionSymbol Constructor
        {
            get { return mConstructor; }
        }

        /// <summary>
        /// Whether this object has a constructor.
        /// </summary>
        public bool HasConstructor
        {
            get { return mHasConstructor; }
        }

        /// <summary>
        /// This object's static variables.
        /// </summary>
        public Dictionary<string, DataSlot> StaticVariables
        {
            get { return mStaticVariables; }
        }

        #endregion

        /// <summary>
        /// Initialise this object definition.
        /// </summary>
        /// <param name="memberVariables">This object's member variables.</param>
        /// <param name="staticVariables">This object's static variables.</param>
        /// <param name="memberFunctions">This object's member functions.</param>
        /// <param name="staticFunctions">This object's static functions.</param>
        /// <param name="constructor">This object's constructor.</permission>
        public void Initialise(string[] memberVariables, string[] staticVariables, FunctionSymbol[] memberFunctions, FunctionSymbol[] staticFunctions, FunctionSymbol constructor)
        {
            // Ensure we haven't already initialised this object
            if (mIsInitialised)
            {
                throw new Exception("Object definition has already been initialised.");
            }
            mIsInitialised = true;

            mMemberVariables = memberVariables;
            mConstructor = constructor;
            mHasConstructor = mConstructor != null;

            // Store the functions in a dictionary for fast access
            foreach (var function in memberFunctions)
            {
                mMemberFunctions.Add(function.Identifier, function);
            }

            // Store the static functions
            foreach (var function in staticFunctions)
            {
                mStaticFunctions.Add(function.Identifier, function);
            }

            // Create the static variable data slots
            foreach (var variable in staticVariables)
            {
                mStaticVariables.Add(variable, new DataSlot());
            }
        }

        /// <summary>
        /// Initialise this object symbol with no members.
        /// </summary>
        public void Initialise()
        {
            // Ensure we haven't already initialised this object
            if (mIsInitialised)
            {
                throw new Exception("Object definition has already been initialised.");
            }
            mIsInitialised = true;
            mHasConstructor = false;
            mMemberVariables = new string[0];
        }

        /// <summary>
        /// Creates new object data from this object symbol.
        /// </summary>
        /// <returns></returns>
        public virtual Data CreateNewObjectData()
        {
            return new ObjectData(this);
        }
    }
}