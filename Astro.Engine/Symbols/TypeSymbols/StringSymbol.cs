﻿using Astro.Engine.DataTypes;

namespace Astro.Engine.Symbols.TypeSymbols
{
    /// <summary>
    /// The symbol for string data.
    /// </summary>
    public class StringSymbol : ObjectSymbol
    {
        /// <summary>
        /// Create the string symbol.
        /// </summary>
        /// <param name="scope">The scope this symbol exists in.</param>
        public StringSymbol(Scope scope)
            : base("String", scope)
        {
            // Initialise with no members
            this.Initialise();
        }

        /// <summary>
        /// Creates a new string data object.
        /// </summary>
        /// <returns>The newly created string data object.</returns>
        public override Data CreateNewObjectData()
        {
            return new StringData(string.Empty);
        }
    }
}