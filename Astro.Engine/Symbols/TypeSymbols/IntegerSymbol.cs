﻿using Astro.Engine.DataTypes;

namespace Astro.Engine.Symbols.TypeSymbols
{
    /// <summary>
    /// The symbol for integer data.
    /// </summary>
    public class IntegerSymbol : ObjectSymbol
    {
        /// <summary>
        /// Create the integer symbol.
        /// </summary>
        /// <param name="scope">The scope this symbol exists in.</param>
        public IntegerSymbol(Scope scope)
            :base("Integer", scope)
        { 
            // Initialise with no members
            this.Initialise();
        }

        /// <summary>
        /// Creates a new integer data object.
        /// </summary>
        /// <returns>The newly created integer data object.</returns>
        public override Data CreateNewObjectData()
        {
            return new IntegerData(0);
        }
    }
}
