﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Astro.Engine.Parsing;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Symbols.TypeSymbols
{
    /// <summary>
    /// The symbol for the console.
    /// </summary>
    public class ConsoleSymbol : ObjectSymbol
    {
        /// <summary>
        /// Create the console symbol.
        /// </summary>
        /// <param name="symbolTable">The symbol table we are adding this symbol to.</param>
        /// <param name="parseNodeFactory">The parse node factory for the current program.</param>
        public ConsoleSymbol(SymbolTable symbolTable, ParseNodeFactory parseNodeFactory)
            :base("Console", symbolTable.CurrentScope)
        {
            // Push the scope while we create the functions
            symbolTable.PushScope(ScopeType.Object);
            FunctionSymbol[] staticFunctions;

            try
            {
                // The Print function
                var printFunction = symbolTable.CreateFunctionSymbol(new Token("Print"));
                printFunction.Initialise(parseNodeFactory.CreateConsolePrintNode(), new string[] { "*message" });

                // The PrintLine function
                var printLineFunction = symbolTable.CreateFunctionSymbol(new Token("PrintLine"));
                printLineFunction.Initialise(parseNodeFactory.CreateConsolePrintLineNode(), new string[] { "*message" });

                staticFunctions = new FunctionSymbol[] { printFunction, printLineFunction };
            }
            finally
            {
                symbolTable.PopScope();
            }

            // Initialise the symbol
            this.Initialise(new string[0], new string[0], new FunctionSymbol[0], staticFunctions, null);
        }
    }
}
