﻿using System.Collections.Generic;
using Astro.Engine.DataTypes;
using Astro.Engine.Parsing;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Symbols.TypeSymbols
{
    /// <summary>
    /// The symbol for dictionary data.
    /// </summary>
    public class DictionarySymbol : ObjectSymbol
    {
        /// <summary>
        /// Create the list symbol.
        /// </summary>
        /// <param name="symbolTable">The symbol table we are adding this symbol to.</param>
        /// <param name="parseNodeFactory">The parse node factory for the current program.</param>
        public DictionarySymbol(SymbolTable symbolTable, ParseNodeFactory parseNodeFactory)
            : base("Dictionary", symbolTable.CurrentScope)
        {
            // Push the scope while we create the functions
            symbolTable.PushScope(ScopeType.Object);
            FunctionSymbol[] memberFunctions;

            try
            {
                // The Add function
                var addFunction = symbolTable.CreateFunctionSymbol(new Token("Add"));
                addFunction.Initialise(parseNodeFactory.CreateDictionaryAddNode(), new string[] { "*key", "*value" });

                // The Remove function
                var removeFunction = symbolTable.CreateFunctionSymbol(new Token("Remove"));
                removeFunction.Initialise(parseNodeFactory.CreateDictionaryRemoveNode(), new string[] { "*key" });

                // The ContainsKey function
                var containsKeyFunction = symbolTable.CreateFunctionSymbol(new Token("ContainsKey"));
                containsKeyFunction.Initialise(parseNodeFactory.CreateDictionaryContainsKeyNode(), new string[] { "*key" });

                // The ContainsValue function
                var containsValueFunction = symbolTable.CreateFunctionSymbol(new Token("ContainsValue"));
                containsValueFunction.Initialise(parseNodeFactory.CreateDictionaryContainsValueNode(), new string[] { "*value" });

                // The Count function
                var countFunction = symbolTable.CreateFunctionSymbol(new Token("Count"));
                countFunction.Initialise(parseNodeFactory.CreateDictionaryCountNode(), new string[0]);

                // The Clear function
                var clearFunction = symbolTable.CreateFunctionSymbol(new Token("Clear"));
                clearFunction.Initialise(parseNodeFactory.CreateDictionaryClearNode(), new string[0]);

                memberFunctions = new FunctionSymbol[] { addFunction, removeFunction, containsKeyFunction, containsValueFunction, countFunction, clearFunction };
            }
            finally
            {
                symbolTable.PopScope();
            }

            // Initialise with no members
            this.Initialise(new string[0], new string[0], memberFunctions, new FunctionSymbol[0], null);
        }

        /// <summary>
        /// Creates a new list data object.
        /// </summary>
        /// <returns>The newly created list data object.</returns>
        public override Data CreateNewObjectData()
        {
            return new DictionaryData(this);
        }
    }
}
