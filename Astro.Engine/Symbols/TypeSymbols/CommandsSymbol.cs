﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Astro.Engine.Parsing;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Symbols.TypeSymbols
{
    /// <summary>
    /// The symbol forthe commands static object.
    /// </summary>
    public class CommandsSymbol : ObjectSymbol
    {
        /// <summary>
        /// Create the commands symbol.
        /// </summary>
        /// <param name="symbolTable">The symbol table we are adding this symbol to.</param>
        /// <param name="parseNodeFactory">The parse node factory for the current program.</param>
        public CommandsSymbol(SymbolTable symbolTable, ParseNodeFactory parseNodeFactory)
            :base("Commands", symbolTable.CurrentScope)
        {
            // Push the scope while we create the functions
            symbolTable.PushScope(ScopeType.Object);
            FunctionSymbol[] staticFunctions;

            try
            {
                // The Add function
                var addFunction = symbolTable.CreateFunctionSymbol(new Token("Add"));
                addFunction.Initialise(parseNodeFactory.CreateCommandsAddNode(), new string[] { "*commandObject", "*event" });

                // The Remove function
                var removeFunction = symbolTable.CreateFunctionSymbol(new Token("Remove"));
                removeFunction.Initialise(parseNodeFactory.CreateCommandsRemoveNode(), new string[] { "*commandObject", "*event" });

                staticFunctions = new FunctionSymbol[] { addFunction, removeFunction };
            }
            finally
            {
                symbolTable.PopScope();
            }

            // Initialise
            this.Initialise(new string[0], new string[0], new FunctionSymbol[0], staticFunctions, null);
        }
    }
}
