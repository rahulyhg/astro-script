﻿using Astro.Engine.DataTypes;

namespace Astro.Engine.Symbols.TypeSymbols
{
    /// <summary>
    /// The symbol for boolean data.
    /// </summary>
    public class BooleanSymbol : ObjectSymbol
    {
        /// <summary>
        /// Create the boolean symbol.
        /// </summary>
        /// <param name="scope">The scope this symbol exists in.</param>
        public BooleanSymbol(Scope scope)
            : base("Boolean", scope)
        {
            // Initialise with no members
            this.Initialise();
        }

        /// <summary>
        /// Creates a new boolean data object.
        /// </summary>
        /// <returns>The newly created boolean data object.</returns>
        public override Data CreateNewObjectData()
        {
            return BooleanData.FalseInstance;
        }
    }
}
