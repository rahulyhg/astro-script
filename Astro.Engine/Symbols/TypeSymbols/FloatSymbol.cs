﻿using Astro.Engine.DataTypes;

namespace Astro.Engine.Symbols.TypeSymbols
{
    /// <summary>
    /// The symbol for float data.
    /// </summary>
    public class FloatSymbol : ObjectSymbol
    {
        /// <summary>
        /// Create the float symbol.
        /// </summary>
        /// <param name="scope">The scope this symbol exists in.</param>
        public FloatSymbol(Scope scope)
            : base("Float", scope)
        {
            // Initialise with no members
            this.Initialise();
        }

        /// <summary>
        /// Creates a new float data object.
        /// </summary>
        /// <returns>The newly created float data object.</returns>
        public override Data CreateNewObjectData()
        {
            return new FloatData(0);
        }
    }
}
