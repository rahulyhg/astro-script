﻿using System.Collections.Generic;
using Astro.Engine.DataTypes;
using Astro.Engine.Parsing;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Symbols.TypeSymbols
{
    /// <summary>
    /// The symbol for list data.
    /// </summary>
    public class ListSymbol : ObjectSymbol
    {
        /// <summary>
        /// Create the list symbol.
        /// </summary>
        /// <param name="symbolTable">The symbol table we are adding this symbol to.</param>
        /// <param name="parseNodeFactory">The parse node factory for the current program.</param>
        public ListSymbol(SymbolTable symbolTable, ParseNodeFactory parseNodeFactory)
            : base("List", symbolTable.CurrentScope)
        {
            // Push the scope while we create the functions
            symbolTable.PushScope(ScopeType.Object);
            FunctionSymbol[] memberFunctions;

            try
            {
                // The Add function
                var addFunction = symbolTable.CreateFunctionSymbol(new Token("Add"));
                addFunction.Initialise(parseNodeFactory.CreateListAddNode(), new string[] { "*toAdd" });

                // The Remove function
                var removeFunction = symbolTable.CreateFunctionSymbol(new Token("Remove"));
                removeFunction.Initialise(parseNodeFactory.CreateListRemoveNode(), new string[] { "*toRemove" });

                // The RemoveAt function
                var removeAtFunction = symbolTable.CreateFunctionSymbol(new Token("RemoveAt"));
                removeAtFunction.Initialise(parseNodeFactory.CreateListRemoveAtNode(), new string[] { "*removePosition" });

                // The Count function
                var countFunction = symbolTable.CreateFunctionSymbol(new Token("Count"));
                countFunction.Initialise(parseNodeFactory.CreateListCountNode(), new string[0]);
                
                // The Contains function
                var containsFunction = symbolTable.CreateFunctionSymbol(new Token("Contains"));
                containsFunction.Initialise(parseNodeFactory.CreateListContainsNode(), new string[] { "*item" });

                // The Clear function
                var clearFunction = symbolTable.CreateFunctionSymbol(new Token("Clear"));
                clearFunction.Initialise(parseNodeFactory.CreateListClearNode(), new string[0]);

                memberFunctions = new FunctionSymbol[] { addFunction, removeFunction, removeAtFunction, countFunction, containsFunction, clearFunction };
            }
            finally
            {
                symbolTable.PopScope();
            }

            // Initialise with members
            this.Initialise(new string[0], new string[0], memberFunctions, new FunctionSymbol[0], null);
        }

        /// <summary>
        /// Creates a new list data object.
        /// </summary>
        /// <returns>The newly created list data object.</returns>
        public override Data CreateNewObjectData()
        {
            return new ListData(this);
        }
    }
}
