﻿using System.Collections.Generic;

namespace Astro.Engine.Symbols
{
    /// <summary>
    /// Defines a scope, containing symbols and child scopes.
    /// </summary>
    public class Scope
    {
        private Scope mParent;
        private int mDepth;
        private ScopeType mType;
        private Dictionary<string, Symbol> mSymbols = new Dictionary<string, Symbol>();

        /// <summary>
        /// Create this scope.
        /// </summary>
        /// <param name="type">The type of this scope.</param>
        /// <param name="parent">This scope's parent scope.</param>
        public Scope(ScopeType type, Scope parent)
        {
            mType = type;
            mParent = parent;
            if (mParent != null)
            {
                mDepth = mParent.Depth + 1;
            }
        }

        #region Properties

        /// <summary>
        /// This scope's parent scope.
        /// </summary>
        public Scope Parent
        {
            get { return mParent; }
        }

        /// <summary>
        /// The type of this scope.
        /// </summary>
        public ScopeType Type
        {
            get { return mType; }
        }

        /// <summary>
        /// The symbols in this scope.
        /// </summary>
        public Dictionary<string, Symbol> Symbols
        {
            get { return mSymbols; }
        }

        /// <summary>
        /// The depth of this scope.
        /// </summary>
        public int Depth
        {
            get { return mDepth; }
        }

        #endregion
    }
}
