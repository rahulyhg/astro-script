﻿using Astro.Engine.DataTypes;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// Parse node for the dictionary object's Remove function.
    /// </summary>
    public class DictionaryRemoveNode : InterpreterParseNode
    {
        /// <summary>
        /// Create a new dictionary remove node.
        /// </summary>
        public DictionaryRemoveNode()
            : base(new Token())
        { }

        /// <summary>
        /// Execute the node.
        /// </summary>
        public override Data Execute()
        {
            // Get the key
            var key = mExecutionEnvironment.GetDataSlot("*key").Data;

            // Get the dictionary
            var dictionary = ((EngineDictionaryData)mExecutionEnvironment.GetDataSlot("*dictionary").Data).Value;

            // Remove the item from the dictionary
            if (dictionary.Remove(key))
            {
                return BooleanData.TrueInstance;
            }
            else
            {
                return BooleanData.FalseInstance;
            }
        }
    }
}
