﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A node for an indexer call.
    /// </summary>
    public class IndexerCallNode : InterpreterParseNode
    {
        private InterpreterParseNode mCollectionNode;
        private InterpreterParseNode mExpressionNode;

        /// <summary>
        /// Create an indexer call.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="collectionNode">The node for the collection we are indexing.</param>
        /// <param name="expressionNode">The node for the indexer's expression.</param>
        public IndexerCallNode(Token token, InterpreterParseNode collectionNode, InterpreterParseNode expressionNode)
            :base(token)
        {
            mCollectionNode = collectionNode;
            mExpressionNode = expressionNode;
        }

        /// <summary>
        /// Execute this node.
        /// </summary>
        public override Data Execute()
        {
            // Get the collection node
            var collectionData = this.ExecuteNode(mCollectionNode);

            // Check whether we can index the data
            Data indexedData;
            if (collectionData.Type == DataType.Object)
            {
                // Execute the indexer expression
                var indexData = this.ExecuteNode(mExpressionNode);

                try
                {
                    // Retrieve the indexed data
                    indexedData = ((ObjectData)collectionData).Index(indexData);
                }
                catch (RuntimeException e)
                {
                    // Set the token for the runtime exception and rethrow
                    e.Token = mToken;
                    throw e;
                }
            }
            else
            {
                throw new RuntimeException(mToken, ErrorCode.TypeMismatch, "The indexer can only be used with object data types.");
            }

            return indexedData;
        }
    }
}