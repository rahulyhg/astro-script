﻿using System;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A postfix increment node.
    /// </summary>
    public class PostfixIncrementNode : InterpreterParseNode
    {
        private DataSlotNode mVariableNode;

        /// <summary>
        /// Create a postfix increment node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="variableNode">The variable we are incrementing.</param>
        public PostfixIncrementNode(Token token, InterpreterParseNode variableNode)
            : base(token)
        {
            mVariableNode = (DataSlotNode)variableNode;
        }

        /// <summary>
        /// Execute this node.
        /// </summary>
        public override Data Execute()
        {
            var data = this.ExecuteNode(mVariableNode);

            // Type checking
            Data returnData;
            if (data.Type == DataType.Integer)
            {
                // Store the data to return
                var integerData = (IntegerData)data;
                returnData = integerData.Copy();

                // Increment the value
                integerData.Value += 1;
            }
            else if (data.Type == DataType.Float)
            {
                // Store the data to return
                var floatData = (FloatData)data;
                returnData = floatData.Copy();

                // Increment the value
                floatData.Value += 1;
            }
            else
            {
                throw new RuntimeException(mVariableNode.Token, ErrorCode.NonNumericDataType, "Can only increment Integer and Float data types.");
            }

            return returnData;
        }
    }
}
