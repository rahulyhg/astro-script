﻿using System;
using System.Collections.Generic;
using System.Linq;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Parsing;
using Astro.Engine.Symbols;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// Parse node factory for the interpreter.
    /// </summary>
    public class InterpreterParseNodeFactory : ParseNodeFactory
    {
        /// <summary>
        /// Create a literal value node from a literal type token.
        /// </summary>
        /// <param name="token">The token to create the value from.</param>
        public override ParseNode CreateLiteralNode(Token token)
        {
            return new LiteralNode(token);
        }

        /// <summary>
        /// Create a boolean negation node.
        /// </summary>
        /// <param name="token">The token for the node.</param>
        /// <param name="node">The node to negate.</param>
        public override ParseNode CreateBooleanNegationNode(Token token, ParseNode node)
        {
            return new BooleanNegationNode(token, (InterpreterParseNode)node);
        }

        /// <summary>
        /// Create a numeric negation node.
        /// </summary>
        /// <param name="token">The token for the node.</param>
        /// <param name="node">The node to negate.</param>
        public override ParseNode CreateNumericNegationNode(Token token, ParseNode node)
        {
            return new NumericNegationNode(token, (InterpreterParseNode)node);
        }

        /// <summary>
        /// Create an operator node.
        /// </summary>
        /// <param name="token">The token for the node.</param>
        /// <param name="leftNode">The node for the left side of the operation.</param>
        /// <param name="rightNode">The node for the right side of the operation.</param>
        public override ParseNode CreateOperatorNode(Token token, ParseNode leftNode, ParseNode rightNode)
        {
            var left = (InterpreterParseNode)leftNode;
            var right = (InterpreterParseNode)rightNode;
            switch (token.Type)
            {
                case TokenType.Addition:
                    return new AdditionNode(token, left, right);
                case TokenType.Subtraction:
                    return new SubtractionNode(token, left, right);
                case TokenType.Multiplication:
                    return new MultiplicationNode(token, left, right);
                case TokenType.Division:
                    return new DivisionNode(token, left, right);
                case TokenType.Modulo:
                    return new ModuloNode(token, left, right);
                case TokenType.EqualsComparision:
                    return new EqualsComparisonNode(token, left, right);
                case TokenType.NotEqualComparison:
                    return new NotEqualComparisonNode(token, left, right);
                case TokenType.GreaterThanComparison:
                    return new GreaterThanComparisonNode(token, left, right);
                case TokenType.LessThanComparison:
                    return new LessThanComparisonNode(token, left, right);
                case TokenType.GreaterThanOrEqualToComparison:
                    return new GreaterThanOrEqualToComparisonNode(token, left, right);
                case TokenType.LessThanOrEqualToComparison:
                    return new LessThanOrEqualToComparisonNode(token, left, right);
                case TokenType.ConditionalAnd:
                    return new ConditionalAndNode(token, left, right);
                case TokenType.ConditionalOr:
                    return new ConditionalOrNode(token, left, right);
                case TokenType.LogicalAnd:
                    return new LogicalAndNode(token, left, right);
                case TokenType.LogicalOr:
                    return new LogicalOrNode(token, left, right);
                default:
                    throw new Exception("Operator token type not recognised.");
            }
        }

        /// <summary>
        /// Create a variable node.
        /// </summary>
        /// <param name="token">The token for the node.</param>
        /// <param name="variableSymbol">The symbol for this variable.</param>
        public override ParseNode CreateVariableNode(Token token, Symbol variableSymbol)
        {
            return new VariableNode(token, variableSymbol);
        }

        /// <summary>
        /// Create a declaration node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="variableSymbol">The symbol for this variable.</param>
        public override ParseNode CreateDeclarationNode(Token token, Symbol variableSymbol)
        {
            return new DeclarationNode(token, variableSymbol);
        }

        /// <summary>
        /// Create an assignment node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="variableNode">The variable node for the assignment.</param>
        /// <param name="expressionNode">The expression node for the assignment.</param>
        public override ParseNode CreateAssignmentNode(Token token, ParseNode variableNode, ParseNode expressionNode)
        {
            return new AssignmentNode(token, (InterpreterParseNode)variableNode, (InterpreterParseNode)expressionNode);
        }

        /// <summary>
        /// Create an accessor node.
        /// </summary>
        /// <param name="token">THe token responsible for this node.</param>
        /// <param name="node">The node we are accessing.</param>
        public override ParseNode CreateAccessorNode(Token token, ParseNode node)
        {
            return new AccessorNode(token, (InterpreterParseNode)node);
        }

        /// <summary>
        /// Create an addition assignment node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="variableNode">The variable node for the assignment.</param>
        /// <param name="expressionNode">The expression node for the assignment.</param>
        public override ParseNode CreateAdditionAssignmentNode(Token token, ParseNode variableNode, ParseNode expressionNode)
        {
            var additionNode = new AdditionNode(token, (InterpreterParseNode)variableNode, (InterpreterParseNode)expressionNode);
            return new AssignmentNode(token, (InterpreterParseNode)variableNode, additionNode);
        }

        /// <summary>
        /// Create a subtraction assignment node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="variableNode">The variable node for the assignment.</param>
        /// <param name="expressionNode">The expression node for the assignment.</param>
        public override ParseNode CreateSubtractionAssignmentNode(Token token, ParseNode variableNode, ParseNode expressionNode)
        {
            var subtractionNode = new SubtractionNode(token, (InterpreterParseNode)variableNode, (InterpreterParseNode)expressionNode);
            return new AssignmentNode(token, (InterpreterParseNode)variableNode, subtractionNode);
        }

        /// <summary>
        /// Create a multiplication assignment node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="variableNode">The variable node for the assignment.</param>
        /// <param name="expressionNode">The expression node for the assignment.</param>
        public override ParseNode CreateMultiplicationAssignmentNode(Token token, ParseNode variableNode, ParseNode expressionNode)
        {
            var multiplcationNode = new MultiplicationNode(token, (InterpreterParseNode)variableNode, (InterpreterParseNode)expressionNode);
            return new AssignmentNode(token, (InterpreterParseNode)variableNode, multiplcationNode);
        }

        /// <summary>
        /// Create a division assignment node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="variableNode">The variable node for the assignment.</param>
        /// <param name="expressionNode">The expression node for the assignment.</param>
        public override ParseNode CreateDivisionAssignmentNode(Token token, ParseNode variableNode, ParseNode expressionNode)
        {
            var divisionNode = new DivisionNode(token, (InterpreterParseNode)variableNode, (InterpreterParseNode)expressionNode);
            return new AssignmentNode(token, (InterpreterParseNode)variableNode, divisionNode);
        }

        /// <summary>
        /// Create a modulo assignment node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="variableNode">The variable node for the assignment.</param>
        /// <param name="expressionNode">The expression node for the assignment.</param>
        public override ParseNode CreateModuloAssignmentNode(Token token, ParseNode variableNode, ParseNode expressionNode)
        {
            var moduloNode = new ModuloNode(token, (InterpreterParseNode)variableNode, (InterpreterParseNode)expressionNode);
            return new AssignmentNode(token, (InterpreterParseNode)variableNode, moduloNode);
        }

        /// <summary>
        /// Create an aggregate node from a list of parse nodes.
        /// </summary>
        /// <param name="nodes">The nodes to aggregate.</param>
        public override ParseNode CreateAggregateNode(List<ParseNode> nodes)
        {
            return new AggregateNode(nodes.Select(node => (InterpreterParseNode)node).ToList());
        }

        /// <summary>
        /// Create an if control node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="conditionNode">The if condition node.</param>
        /// <param name="blockNode">The block node.</param>
        public override ParseNode CreateIfControlNode(Token token, ParseNode conditionNode, ParseNode blockNode)
        {
            return new IfControlNode(token, (InterpreterParseNode)conditionNode, (InterpreterParseNode)blockNode);
        }

        /// <summary>
        /// Create an while control node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="conditionNode">The while condition node.</param>
        /// <param name="blockNode">The block node.</param>
        public override ParseNode CreateWhileControlNode(Token token, ParseNode conditionNode, ParseNode blockNode)
        {
            return new WhileControlNode(token, (InterpreterParseNode)conditionNode, (InterpreterParseNode)blockNode);
        }

        /// <summary>
        /// Create a foreach loop control node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="declarationNode">The foreach declaration node.</param>
        /// <param name="variableNode">The variable node to iterate over.</param>
        /// <param name="blockNode">The block to loop.</param>
        public override ParseNode CreateForeachControlNode(Token token, ParseNode declarationNode, ParseNode variableNode, ParseNode blockNode)
        {
            return new ForeachControlNode(token, (InterpreterParseNode)declarationNode, (InterpreterParseNode)variableNode, (InterpreterParseNode)blockNode);
        }

        /// <summary>
        /// Create a for loop control node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="declarationNode">The optional declaration node.</param>
        /// <param name="conditionNode">The optional condition node.</param>
        /// <param name="assignmentNode">The optional assignment node.</param>
        /// <param name="blockNode">The block node.</param>
        public override ParseNode CreateForControlNode(Token token, ParseNode declarationNode, ParseNode conditionNode, ParseNode assignmentNode, ParseNode blockNode)
        {
            return new ForControlNode(token, (InterpreterParseNode)declarationNode, (InterpreterParseNode)conditionNode, (InterpreterParseNode)assignmentNode, (InterpreterParseNode)blockNode);
        }

        /// <summary>
        /// Create a postfix increment node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="node">The node we are incrementing.</param>
        public override ParseNode CreatePostfixIncrementNode(Token token, ParseNode node)
        {
            return new PostfixIncrementNode(token, (InterpreterParseNode)node);
        }

        /// <summary>
        /// Create a postfix decrement node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="node">The node we are decrementing.</param>
        public override ParseNode CreatePostfixDecrementnode(Token token, ParseNode node)
        {
            return new PostfixDecrementNode(token, (InterpreterParseNode)node);
        }

        /// <summary>
        /// Create a prefix increment node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="node">The node we are incrementing.</param>
        public override ParseNode CreatePrefixIncrementNode(Token token, ParseNode node)
        {
            return new PrefixIncrementNode(token, (InterpreterParseNode)node);
        }

        /// <summary>
        /// Create a prefix decrement node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="node">The node we are decrementing.</param>
        public override ParseNode CreatePrefixDecrementNode(Token token, ParseNode node)
        {
            return new PrefixDecrementNode(token, (InterpreterParseNode)node);
        }
        
        /// <summary>
        /// Create a constructor node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="objectSymbol">The symbol for the object we are constructing.</param>
        /// <param name="parameterNodes">The parameters for this constructor.</param>
        public override ParseNode CreateConstructorNode(Token token, ObjectSymbol objectSymbol, List<ParseNode> parameterNodes)
        {
            return new ConstructorNode(token, objectSymbol, parameterNodes.Select(node => (InterpreterParseNode)node).ToArray());
        }

        /// <summary>
        /// Create a function node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="functionSymbol">The symbol for the function.</param>
        public override ParseNode CreateFunctionNode(Token token, FunctionSymbol functionSymbol)
        {
            return new FunctionNode(token, functionSymbol);
        }

        /// <summary>
        /// Create a function call node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="functionNode">The node for the function to call.</param>
        /// <param name="parameterNodes">The nodes for this call's parameters.</param>
        public override ParseNode CreateFunctionCallNode(Token token, ParseNode functionNode, List<ParseNode> parameterNodes)
        {
            return new FunctionCallNode(token, (InterpreterParseNode)functionNode, parameterNodes.Select(node => (InterpreterParseNode)node).ToArray());
        }

        /// <summary>
        /// Create an indexer node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="collectionNode">The node for the collection we are indexing.</param>
        /// <param name="expressionNode">The node for the indexer's expression.</param>
        public override ParseNode CreateIndexerCallNode(Token token, ParseNode collectionNode, ParseNode expressionNode)
        {
            return new IndexerCallNode(token, (InterpreterParseNode)collectionNode, (InterpreterParseNode)expressionNode);
        }

        /// <summary>
        /// Create a return node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        public override ParseNode CreateReturnNode(Token token)
        {
            return new ReturnNode(token);
        }

        /// <summary>
        /// Create a return value node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="returnExpression">The expression for the value to return.</param>
        public override ParseNode CreateReturnValueNode(Token token, ParseNode returnExpression)
        {
            return new ReturnValueNode(token, (InterpreterParseNode)returnExpression);
        }

        /// <summary>
        /// Create a break node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        public override ParseNode CreateBreakNode(Token token)
        {
            return new BreakNode(token);
        }

        /// <summary>
        /// Create an object node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="symbol">The symbol for this node's object.</param>
        public override ParseNode CreateObjectNode(Token token, Symbol symbol)
        {
            return new ObjectNode(token, symbol);
        }

        /// <summary>
        /// Create an add node for the list object.
        /// </summary>
        public override ParseNode CreateListAddNode()
        {
            return new ListAddNode();
        }

        /// <summary>
        /// Create a remove node for the list object.
        /// </summary>
        public override ParseNode CreateListRemoveNode()
        {
            return new ListRemoveNode();
        }

        /// <summary>
        /// Create a remove at node for the list object.
        /// </summary>
        public override ParseNode CreateListRemoveAtNode()
        {
            return new ListRemoveAtNode();
        }

        /// <summary>
        /// Create a count node for the list object.
        /// </summary>
        public override ParseNode CreateListCountNode()
        {
            return new ListCountNode();
        }

        /// <summary>
        /// Create a contains node for the list object.
        /// </summary>
        public override ParseNode CreateListContainsNode()
        {
            return new ListContainsNode();
        }

        /// <summary>
        /// Create a clear node for the list object.
        /// </summary>
        public override ParseNode CreateListClearNode()
        {
            return new ListClearNode();
        }

        /// <summary>
        /// Create a print node for the console object.
        /// </summary>
        public override ParseNode CreateConsolePrintNode()
        {
            return new ConsolePrintNode();
        }

        /// <summary>
        /// Create a print line node for the console object.
        /// </summary>
        public override ParseNode CreateConsolePrintLineNode()
        {
            return new ConsolePrintLineNode();
        }

        /// <summary>
        /// Create an add node for the dictionary object.
        /// </summary>
        public override ParseNode CreateDictionaryAddNode()
        {
            return new DictionaryAddNode();
        }

        /// <summary>
        /// Create a remove node for the dictionary object.
        /// </summary>
        public override ParseNode CreateDictionaryRemoveNode()
        {
            return new DictionaryRemoveNode();
        }

        /// <summary>
        /// Create a contains key node for the dictionary object.
        /// </summary>
        public override ParseNode CreateDictionaryContainsKeyNode()
        {
            return new DictionaryContainsKeyNode();
        }

        /// <summary>
        /// Create a contains value node for the dictionary object.
        /// </summary>
        public override ParseNode CreateDictionaryContainsValueNode()
        {
            return new DictionaryContainsValueNode();
        }

        /// <summary>
        /// Create a count node for the dictionary object.
        /// </summary>
        public override ParseNode CreateDictionaryCountNode()
        {
            return new DictionaryCountNode();
        }

        /// <summary>
        /// Create a clear node for the dictionary object.
        /// </summary>
        public override ParseNode CreateDictionaryClearNode()
        {
            return new DictionaryClearNode();
        }

        /// <summary>
        /// Create an add node for the commands object.
        /// </summary>
        public override ParseNode CreateCommandsAddNode()
        {
            return new CommandsAddNode();
        }

        /// <summary>
        /// Create a remove node for the commands object.
        /// </summary>
        public override ParseNode CreateCommandsRemoveNode()
        {
            return new CommandsRemoveNode();
        }
    }
}
