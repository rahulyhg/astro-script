﻿using System;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// Parse node for the dictionary object's Add function.
    /// </summary>
    public class DictionaryAddNode : InterpreterParseNode
    {
        /// <summary>
        /// Create a new dictionary add node.
        /// </summary>
        public DictionaryAddNode()
            : base(new Token())
        { }

        /// <summary>
        /// Execute the node.
        /// </summary>
        public override Data Execute()
        {
            // Get the key and value data
            var key = mExecutionEnvironment.GetDataSlot("*key").Data;
            var value = mExecutionEnvironment.GetDataSlot("*value").Data;

            // Get the dictionary we are adding to
            var dictionary = ((EngineDictionaryData)mExecutionEnvironment.GetDataSlot("*dictionary").Data).Value;

            // Add the key and value to the dictionary
            try
            {
                dictionary.Add(key, value);
            }
            catch (ArgumentException)
            {
                throw new RuntimeException(this.FindCallingFunctionNode().Token, ErrorCode.TypeMismatch, string.Format("Key '{0}' already exists in the given 'Dictionary'.", key.ToString()));
            }

            return NullData.Instance;
        }
    }
}
