﻿using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A node for a function call.
    /// </summary>
    public class FunctionCallNode : InterpreterParseNode
    {
        private InterpreterParseNode mFunctionNode;
        private InterpreterParseNode[] mParameterNodes;
        private int mParameterCount;
        private Data mReturnData;

        /// <summary>
        /// Create a function call.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="functionNode">The node for the function we are calling.</param>
        /// <param name="parameterNodes">The nodes for this call's parameters.</param>
        public FunctionCallNode(Token token, InterpreterParseNode functionNode, InterpreterParseNode[] parameterNodes)
            :base(token)
        {
            mFunctionNode = functionNode;
            mParameterNodes = parameterNodes;
            mParameterCount = mParameterNodes.Length;
        }

        /// <summary>
        /// The data this function call will return;
        /// </summary>
        public Data ReturnData
        {
            get { return mReturnData; }
            set { mReturnData = value; }
        }

        /// <summary>
        /// Execute this node.
        /// </summary>
        public override Data Execute()
        {
            mReturnData = null;
            mExecutionEnvironment.PushStackFrame();

            try
            {
                // Execute the function node and retrieve its data
                var functionData = (FunctionData)this.ExecuteNode(mFunctionNode);

                // Push the context variables on to the stack
                foreach (var variable in functionData.ContextVariables)
                {
                    mExecutionEnvironment.AddDataSlot(variable.Key, variable.Value);
                }

                // Ensure we have the correct number of parameters
                var parameters = functionData.Parameters;
                var actualParameterCount = parameters.Length;
                if (mParameterCount != actualParameterCount)
                {
                    throw new RuntimeException(mToken, ErrorCode.InsufficientParameters, 
                        string.Format("Function called with {0} parameter(s) when it expects {1}.", mParameterCount, actualParameterCount));
                }

                // Push each variable on to the stack for each parameter and execute its node
                for (int i = 0; i < mParameterCount; i++)
                {
                    mExecutionEnvironment.CreateDataSlot(parameters[i], this.ExecuteNode(mParameterNodes[i]));
                }

                // Call the function's parse tree
                this.ExecuteNode((InterpreterParseNode)functionData.ParseTree);
            }
            finally
            {
                // Ensure stack frame is popped
                mExecutionEnvironment.PopStackFrame();
            }

            return mReturnData;
        }
    }
}
