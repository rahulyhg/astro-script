﻿
namespace Astro.Engine.Interpreter.ExecutionEnvironment
{
    /// <summary>
    /// Interface for the console output class used by the execution environment.
    /// </summary>
    public interface IConsoleOutput
    {
        /// <summary>
        /// Prints the given string then creates a new line.
        /// </summary>
        /// <param name="s">The string to print.</param>
        void PrintLine(string s);

        /// <summary>
        /// Prints the given string.
        /// </summary>
        /// <param name="s"></param>
        void Print(string s);
    }
}
