﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Astro.Engine.Interpreter.ExecutionEnvironment
{
    /// <summary>
    /// Event args for when the commands event listener detectes an event.
    /// </summary>
    public class EventDetectedEventArgs : EventArgs
    {
        private string mEventName;

        /// <summary>
        /// Create new event detected event args.
        /// </summary>
        /// <param name="eventName">The name of the detected event.</param>
        public EventDetectedEventArgs(string eventName)
            :base()
        {
            mEventName = eventName;
        }

        /// <summary>
        /// The name of the detected event.
        /// </summary>
        public string EventName
        {
            get { return mEventName; }
        }
    }
}
