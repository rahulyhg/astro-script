﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Astro.Engine.DataTypes;
using Astro.Engine.Symbols;

namespace Astro.Engine.Interpreter.ExecutionEnvironment
{
    /// <summary>
    /// Manages the storing and calling of commands.
    /// </summary>
    public class CommandsManager : IDisposable
    {
        private InterpreterExecutionEnvironment mExecutionEnvironment;
        private ICommandsEventListener mEventListener;
        private Dictionary<string, List<ObjectData>> mCommands = new Dictionary<string, List<ObjectData>>();

        /// <summary>
        /// Create the commands manager.
        /// </summary>
        /// <param name="executionEnvironment">The execution environment this manager will manage commands for.</param>
        /// <param name="eventListener">The event listener to use.</param>
        public CommandsManager(InterpreterExecutionEnvironment executionEnvironment, ICommandsEventListener eventListener)
        {
            mExecutionEnvironment = executionEnvironment;
            mEventListener = eventListener;

            // Listen to the listener
            mEventListener.EventStartDetectedEvent += mEventListener_EventStartDetectedEvent;
            mEventListener.EventStopDetectedEvent += mEventListener_EventStopDetectedEvent;
            mEventListener.EventRepeatDetectedEvent += mEventListener_EventRepeatDetectedEvent;
        }

        /// <summary>
        /// Whether the manager has commands.
        /// </summary>
        public bool HasCommands
        {
            get { return mCommands.Count > 0; }
        }

        /// <summary>
        /// Add a command object.
        /// </summary>
        /// <param name="objectData">The command object to add.</param>
        /// <param name="eventName">The name of the event to add the object to.</param>
        public void AddCommandObject(ObjectData objectData, string eventName)
        {
            // Normalise the event name to upper case
            eventName = eventName.ToUpper();

            // Check if the event is not already stored
            List<ObjectData> objectList;
            if (!mCommands.TryGetValue(eventName, out objectList))
            {
                objectList = new List<ObjectData>();
                mCommands.Add(eventName, objectList);
            }

            // Add the object to the list
            objectList.Add(objectData);
        }

        /// <summary>
        /// Remove a command object. 
        /// </summary>
        /// <param name="objectData">The command object to remove.</param>
        /// <param name="eventName">The name of the event to remove the object from.</param>
        /// <returns>Whether this command object was found and removed.</returns>
        public bool RemoveCommandObject(ObjectData objectData, string eventName)
        {
            // Normalise the event name to upper case
            eventName = eventName.ToUpper();

            // Check that the event exists
            List<ObjectData> objectList;
            if (!mCommands.TryGetValue(eventName, out objectList))
            {
                return false;
            }

            // Attempt to remove the object from the event
            return objectList.Remove(objectData);
        }

        /// <summary>
        /// Fires the event on any listening objects.
        /// </summary>
        /// <param name="eventName">The event to fire.</param>
        /// <param name="handlerName">The handler to fire.</param>
        private void FireObjectEvents(string eventName, string handlerName)
        {
            // Check to see if the event has been stored
            List<ObjectData> objectList;
            if (!mCommands.TryGetValue(eventName.ToUpper(), out objectList))
            {
                return;
            }

            // Call the OnStarted function on all the objects
            for (int i = objectList.Count - 1; i >= 0; i--)
            {
                var objectData = objectList[i];

                // Find the OnStarted function
                FunctionSymbol function;
                if (objectData.Symbol.MemberFunctions.TryGetValue(handlerName, out function))
                {
                    // Push the object's member variables onto the stack
                    mExecutionEnvironment.PushStackFrame();

                    try
                    {
                        foreach (var variable in objectData.Variables)
                        {
                            mExecutionEnvironment.AddDataSlot(variable.Key, variable.Value);
                        }

                        mExecutionEnvironment.ExecuteProgram((InterpreterParseNode)function.ParseTree);
                    }
                    finally
                    {
                        mExecutionEnvironment.PopStackFrame();
                    }
                }
            }
        }

        /// <summary>
        /// Handles the the event start detected event of the event listener.
        /// </summary>
        /// <param name="sender">The event's sender.</param>
        /// <param name="e">The event arguments.</param>
        private void mEventListener_EventStartDetectedEvent(object sender, EventDetectedEventArgs e)
        {
            this.FireObjectEvents(e.EventName, "OnStart");
        }

        /// <summary>
        /// Handles the the event stop detected event of the event listener.
        /// </summary>
        /// <param name="sender">The event's sender.</param>
        /// <param name="e">The event arguments.</param>
        private void mEventListener_EventStopDetectedEvent(object sender, EventDetectedEventArgs e)
        {
            this.FireObjectEvents(e.EventName, "OnStop");
        }

        /// <summary>
        /// Handles the the event repeat detected event of the event listener.
        /// </summary>
        /// <param name="sender">The event's sender.</param>
        /// <param name="e">The event arguments.</param>
        private void mEventListener_EventRepeatDetectedEvent(object sender, EventDetectedEventArgs e)
        {
            this.FireObjectEvents(e.EventName, "OnRepeat");
        }

        /// <summary>
        /// Dispose of the commands manager.
        /// </summary>
        public void Dispose()
        {
            // Stop listening to the listener
            mEventListener.EventStartDetectedEvent -= mEventListener_EventStartDetectedEvent;
            mEventListener.EventStopDetectedEvent -= mEventListener_EventStopDetectedEvent;
            mEventListener.EventRepeatDetectedEvent -= mEventListener_EventRepeatDetectedEvent;
        }
    }
}
