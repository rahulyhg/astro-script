﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Astro.Engine.Interpreter.ExecutionEnvironment
{
    /// <summary>
    /// The interface for an event listener for the commands manager.
    /// </summary>
    public interface ICommandsEventListener
    {
        /// <summary>
        /// Raised when this listener detects an event's start.
        /// </summary>
        event EventHandler<EventDetectedEventArgs> EventStartDetectedEvent;

        /// <summary>
        /// Raised when this listener detects an event's end.
        /// </summary>
        event EventHandler<EventDetectedEventArgs> EventStopDetectedEvent;

        /// <summary>
        /// Raised when this listener detects an event's repetition.
        /// </summary>
        event EventHandler<EventDetectedEventArgs> EventRepeatDetectedEvent;
    }
}
