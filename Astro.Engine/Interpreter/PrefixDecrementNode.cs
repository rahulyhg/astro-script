﻿using System;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A prefix decrement node.
    /// </summary>
    public class PrefixDecrementNode : InterpreterParseNode
    {
        private DataSlotNode mVariableNode;

        /// <summary>
        /// Create a prefix decrement node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="variableNode">The variable we are decrementing.</param>
        public PrefixDecrementNode(Token token, InterpreterParseNode variableNode)
            : base(token)
        {
            mVariableNode = (DataSlotNode)variableNode;
        }

        /// <summary>
        /// Execute this node.
        /// </summary>
        public override Data Execute()
        {
            var data = this.ExecuteNode(mVariableNode);

            // Type checking
            Data returnData;
            if (data.Type == DataType.Integer)
            {
                // Store and decrement the data to return
                var integerData = (IntegerData)data;
                integerData.Value -= 1;
                returnData = integerData.Copy();
            }
            else if (data.Type == DataType.Float)
            {
                // Store and decrement the data to return
                var floatData = (FloatData)data;
                floatData.Value -= 1;
                returnData = floatData.Copy();
            }
            else
            {
                throw new RuntimeException(mVariableNode.Token, ErrorCode.NonNumericDataType, "Can only increment Integer and Float data types.");
            }

            return returnData;
        }
    }
}
