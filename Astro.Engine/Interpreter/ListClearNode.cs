﻿using System;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// Parse node for the list object's Clear function.
    /// </summary>
    public class ListClearNode : InterpreterParseNode
    {
        /// <summary>
        /// Create a new list clear at node.
        /// </summary>
        public ListClearNode()
            : base(new Token())
        { }

        /// <summary>
        /// Execute the node.
        /// </summary>
        public override Data Execute()
        {
            // Get the list
            var list = ((EngineListData)mExecutionEnvironment.GetDataSlot("*list").Data).Value;
            list.Clear();

            return NullData.Instance;
        }
    }
}
