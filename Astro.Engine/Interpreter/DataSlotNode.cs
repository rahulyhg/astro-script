﻿using Astro.Engine.DataTypes;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// Abstract class for a node which contains a data slot.
    /// </summary>
    public abstract class DataSlotNode : InterpreterParseNode
    {
        /// <summary>
        /// The data slot stored at this node.
        /// </summary>
        protected DataSlot mDataSlot;

        /// <summary>
        /// Create the data slot node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        public DataSlotNode(Token token)
            :base(token)
        { }
            
        /// <summary>
        /// The data slot stored at this node.
        /// </summary>
        public DataSlot DataSlot
        {
            get { return mDataSlot; }
        }
    }
}
