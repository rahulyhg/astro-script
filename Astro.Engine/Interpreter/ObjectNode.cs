﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Astro.Engine.DataTypes;
using Astro.Engine.Symbols;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A node for an object symbol.
    /// </summary>
    public class ObjectNode : InterpreterParseNode
    {
        private Symbol mObjectSymbol;

        /// <summary>
        /// Create a new object node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="objectSymbol">The symbol for the object at this node.</param>
        public ObjectNode(Token token, Symbol objectSymbol)
            :base(token)
        {
            mObjectSymbol = objectSymbol;
        }

        /// <summary>
        /// This object's symbol.
        /// </summary>
        public Symbol Symbol
        {
            get { return mObjectSymbol; }
        }

        /// <summary>
        /// Execute this node.
        /// </summary>
        /// <returns>The data at this node.</returns>
        public override Data Execute()
        {
            return NullData.Instance;
        }
    }
}
