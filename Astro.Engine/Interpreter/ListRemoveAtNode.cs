﻿using System;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// Parse node for the list object's RemoveAt function.
    /// </summary>
    public class ListRemoveAtNode : InterpreterParseNode
    {
        /// <summary>
        /// Create a new list remove at node.
        /// </summary>
        public ListRemoveAtNode()
            : base(new Token())
        { }

        /// <summary>
        /// Execute the node.
        /// </summary>
        public override Data Execute()
        {
            // Get the data
            var removePosition = mExecutionEnvironment.GetDataSlot("*removePosition").Data;

            // Ensure the position is an integer
            if (removePosition.Type != DataType.Integer)
            {
                throw new RuntimeException(this.FindCallingFunctionNode().Token, ErrorCode.TypeMismatch, string.Format("Parameter 'removePosition' for function 'RemoveAt' must be of type 'Integer', not type '{0}'", removePosition.Type.ToString()));
            }

            // Get the list
            var list = ((EngineListData)mExecutionEnvironment.GetDataSlot("*list").Data).Value;

            try
            {
                
                list.RemoveAt(((IntegerData)removePosition).Value);
            }
            catch (ArgumentOutOfRangeException)
            {
                // Find whether the position was too high or too low
                var position = ((IntegerData)removePosition).Value;
                if (position < 0)
                {
                    throw new RuntimeException(this.FindCallingFunctionNode().Token, ErrorCode.TypeMismatch, "Parameter 'removePosition' for function 'RemoveAt' must be greater than or equal to 0.");
                }
                else if (position >= list.Count)
                {
                    throw new RuntimeException(this.FindCallingFunctionNode().Token, ErrorCode.TypeMismatch, "Parameter 'removePosition' for function 'RemoveAt' must be less than the number of list elements.");
                }
                else
                {
                    throw new Exception("Critical error: Remove position is neither too high nor too low.");
                }
            }

            return NullData.Instance;
        }
    }
}
