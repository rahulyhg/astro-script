﻿using System;
using System.Collections;
using System.Collections.Generic;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Interpreter.ExecutionEnvironment;
using Astro.Engine.Parsing;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// The base class for parse nodes for the interpreter.
    /// </summary>
    public abstract class InterpreterParseNode : ParseNode
    {
        /// <summary>
        /// The execution environment for the current interpreted program.
        /// </summary>
        protected InterpreterExecutionEnvironment mExecutionEnvironment;

        /// <summary>
        /// The stack of nodes which have executed this node.
        /// </summary>
        private Stack<InterpreterParseNode> mExecutingNodeStack = new Stack<InterpreterParseNode>();

        /// <summary>
        /// Create an interpreter parse node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        public InterpreterParseNode(Token token)
            :base(token)
        {
            mExecutingNodeStack.Push(null);
        }

        #region Properties

        /// <summary>
        /// The execution environment for the current interpreted program.
        /// </summary>
        public InterpreterExecutionEnvironment ExecutionEnvironment
        {
            get { return mExecutionEnvironment; }
        }

        /// <summary>
        /// The node which has executed this node.
        /// </summary>
        public InterpreterParseNode ExecutingNode
        {
            get { return mExecutingNodeStack.Peek(); }
        }

        #endregion

        /// <summary>
        /// Set the execution environment for this node.
        /// </summary>
        /// <param name="executionEnvironment">The execution environment to set.</param>
        public void SetExecutionEnvironment(InterpreterExecutionEnvironment executionEnvironment)
        {
            // Check that we haven't already set the environment
            if (mExecutionEnvironment != null)
            {
                //throw new Exception("Execution environment has already been set.");
            }

            mExecutionEnvironment = executionEnvironment;
        }

        /// <summary>
        /// Execute the given node and pass on the execution environment.
        /// </summary>
        /// <param name="node">The node to execute.</param>
        [System.Diagnostics.DebuggerStepThrough()]
        protected Data ExecuteNode(InterpreterParseNode node)
        {
            node.mExecutionEnvironment = mExecutionEnvironment;
            node.mExecutingNodeStack.Push(this);

            var data = node.Execute();

            node.mExecutingNodeStack.Pop();
            return data;
        }

        /// <summary>
        /// Resolve the given node. Performs type checking and returns the boolean data.
        /// </summary>
        /// <param name="node">The node to resolve.</param>
        /// <returns>The boolean data at the node.</returns>
        protected BooleanData ExecuteNodeAsBoolean(InterpreterParseNode node)
        {
            // Execute the node
            var data = this.ExecuteNode(node);

            // Check its type
            if (data.Type != DataType.Boolean)
            {
                throw new RuntimeException(node.Token, ErrorCode.NonBooleanDataType, string.Format("'{0}' must be a Boolean.", node.Token.Lexeme));
            }

            // Return its data
            return (BooleanData)data;
        }

        /// <summary>
        /// Searches up the list of executing nodes for the function call node responsible
        /// for executing this node.
        /// </summary>
        /// <returns>The function call node that executed this node.</returns>
        protected FunctionCallNode FindCallingFunctionNode()
        {
            var node = mExecutingNodeStack.Peek();
            while (!(node is FunctionCallNode))
            {
                node = node.ExecutingNode;
            }

            return (FunctionCallNode)node;
        }
    }
}
