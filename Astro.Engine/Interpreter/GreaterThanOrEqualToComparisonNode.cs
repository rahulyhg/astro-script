﻿using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A node for a greater than or equal to comparison.
    /// </summary>
    public class GreaterThanOrEqualToComparisonNode : InterpreterParseNode
    {
        private InterpreterParseNode mLeftNode;
        private InterpreterParseNode mRightNode;

        /// <summary>
        /// Create the greater than or equal to comparison.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="leftNode">The left side node of the comparison.</param>
        /// <param name="rightNode">The right side node of the comparison.</param>
        public GreaterThanOrEqualToComparisonNode(Token token, InterpreterParseNode leftNode, InterpreterParseNode rightNode)
            :base(token)
        {
            mLeftNode = leftNode;
            mRightNode = rightNode;
        }

        /// <summary>
        /// Execute the comparison.
        /// </summary>
        /// <returns>The result of the comparison.</returns>
        public override Data Execute()
        {
            var leftData = this.ExecuteNode(mLeftNode);
            var rightData = this.ExecuteNode(mRightNode);

            // Check types
            if (leftData.Type == DataType.Integer)
            {
                if (rightData.Type == DataType.Integer)
                {
                    return BooleanData.GetBooleanData(((IntegerData)leftData).Value >=
                                                      ((IntegerData)rightData).Value);
                }
                else if (rightData.Type == DataType.Float)
                {
                    return BooleanData.GetBooleanData(((IntegerData)leftData).Value >=
                                                      ((FloatData)rightData).Value);
                }
                else
                {
                    this.ThrowMatchException(leftData.Type, rightData.Type);
                }
            }
            else if (leftData.Type == DataType.Float)
            {
                if (rightData.Type == DataType.Integer)
                {
                    return BooleanData.GetBooleanData(((FloatData)leftData).Value >=
                                                      ((IntegerData)rightData).Value);
                }
                else if (rightData.Type == DataType.Float)
                {
                    return BooleanData.GetBooleanData(((FloatData)leftData).Value >=
                                                      ((FloatData)rightData).Value);
                }
                else
                {
                    this.ThrowMatchException(leftData.Type, rightData.Type);
                }
            }
            else
            {
                this.ThrowMatchException(leftData.Type, rightData.Type);
            }

            return NullData.Instance;
        }

        /// <summary>
        /// Throw an exception because there was not a match between the two types.
        /// </summary>
        /// <param name="left">The left type.</param>
        /// <param name="right">The right type.</param>
        private void ThrowMatchException(DataType left, DataType right)
        {
            throw new RuntimeException(mToken, ErrorCode.TypeMismatch, string.Format("Cannot perform a greater than or equal to comparison on types {0} and {1}.", left.ToString(), right.ToString()));
        }
    }
}
