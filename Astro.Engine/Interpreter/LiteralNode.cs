﻿using System;
using Astro.Engine.DataTypes;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A node for a literal value.
    /// </summary>
    public class LiteralNode : InterpreterParseNode
    {
        private Data mData;

        /// <summary>
        /// Create the literal node.
        /// </summary>
        /// <param name="token">The token this literal value will be created from.</param>
        public LiteralNode(Token token)
            :base(token)
        {
            // Create the data type depending on the token type
            var type = token.Type;
            var lexeme = token.Lexeme;
            if (type == TokenType.NumericLiteral)
            {
                // Try parse int
                int intValue;
                if (int.TryParse(lexeme, out intValue))
                {
                    mData = new IntegerData(intValue);
                    return;
                }

                // try parse float
                float floatValue;
                if (float.TryParse(lexeme, out floatValue))
                {
                    mData = new FloatData(floatValue);
                }
                else
                {
                    throw new Exception("Error parsing lexeme for numeric data types.");
                }
            }
            else if (type == TokenType.StringLiteral)
            {
                mData = new StringData(lexeme.Substring(1, lexeme.Length - 2));
            }
            else if (type == TokenType.True || type == TokenType.False)
            {
                // Try parse
                bool boolValue;
                if (bool.TryParse(lexeme, out boolValue))
                {
                    mData = (boolValue ? BooleanData.TrueInstance : BooleanData.FalseInstance);
                }
                else
                {
                    throw new Exception("Error parsing lexeme for boolean data type.");
                }
            }
            else if (type == TokenType.Null)
            {
                mData = NullData.Instance;
            }
            else
            {
                // Unrecognised literal
                throw new Exception("Unrecognised literal token type.");
            }
        }

        /// <summary>
        /// Execute this node.
        /// </summary>
        /// <returns>The data.</returns>
        public override Data Execute()
        {
            return mData.Copy();
        }
    }
}
