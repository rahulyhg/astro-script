﻿using System;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// Parse node for the dictionary object's Clear function.
    /// </summary>
    public class DictionaryClearNode : InterpreterParseNode
    {
        /// <summary>
        /// Create a new dictionary clear at node.
        /// </summary>
        public DictionaryClearNode()
            : base(new Token())
        { }

        /// <summary>
        /// Execute the node.
        /// </summary>
        public override Data Execute()
        {
            // Get the dictionary
            var dictionary = ((EngineDictionaryData)mExecutionEnvironment.GetDataSlot("*dictionary").Data).Value;
            dictionary.Clear();

            return NullData.Instance;
        }
    }
}
