﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// Parse node for the commands' Remove function.
    /// </summary>
    public class CommandsRemoveNode : InterpreterParseNode
    {
        /// <summary>
        /// Create a new commands remove node.
        /// </summary>
        public CommandsRemoveNode()
            : base(new Token())
        { }

        /// <summary>
        /// Execute the node.
        /// </summary>
        public override Data Execute()
        {
            // Get the command object
            var commandData = mExecutionEnvironment.GetDataSlot("*commandObject").Data;
            var commandObject = commandData as ObjectData;

            // Get the event data
            var data = mExecutionEnvironment.GetDataSlot("*event").Data;
            var eventData = data as StringData;

            // Ensure the event data is a string
            if (eventData == null)
            {
                throw new RuntimeException(this.FindCallingFunctionNode().Token, ErrorCode.TypeMismatch, string.Format("Parameter 'event' for function 'Add' must be of type 'String', not type '{0}'", data.Type.ToString()));
            }

            // Ensure the command object is an object
            if (commandData == null)
            {
                throw new RuntimeException(this.FindCallingFunctionNode().Token, ErrorCode.TypeMismatch, string.Format("Parameter 'commandObject' for function 'Add' must be of type 'Object', not type '{0}'", commandData.Type.ToString()));
            }

            // Remove the event
            if (mExecutionEnvironment.CommandsManager.RemoveCommandObject(commandObject, eventData.Value))
            {
                return BooleanData.TrueInstance;
            }
            else
            {
                return BooleanData.FalseInstance;
            }
        }
    }
}
