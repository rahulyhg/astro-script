﻿using System;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A postfix decrement node.
    /// </summary>
    public class PostfixDecrementNode : InterpreterParseNode
    {
        private DataSlotNode mVariableNode;

        /// <summary>
        /// Create a postfix decrement node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="variableNode">The variable we are decrementing.</param>
        public PostfixDecrementNode(Token token, InterpreterParseNode variableNode)
            : base(token)
        {
            mVariableNode = (DataSlotNode)variableNode;
        }

        /// <summary>
        /// Execute this node.
        /// </summary>
        public override Data Execute()
        {
            var data = this.ExecuteNode(mVariableNode);

            // Type checking
            Data returnData;
            if (data.Type == DataType.Integer)
            {
                // Store the data to return
                var integerData = (IntegerData)data;
                returnData = integerData.Copy();

                // Decrement the value
                integerData.Value -= 1;
            }
            else if (data.Type == DataType.Float)
            {
                // Store the data to return
                var floatData = (FloatData)data;
                returnData = floatData.Copy();

                // Decrement the value
                floatData.Value -= 1;
            }
            else
            {
                throw new RuntimeException(mVariableNode.Token, ErrorCode.NonNumericDataType, "Can only decrement Integer and Float data types.");
            }

            return returnData;
        }
    }
}
