﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// Parse node for the console object's Print function.
    /// </summary>
    public class ConsolePrintNode : InterpreterParseNode
    {
        /// <summary>
        /// Create a new console print node.
        /// </summary>
        public ConsolePrintNode()
            :base(new Token())
        { }

        /// <summary>
        /// Execute the node.
        /// </summary>
        public override Data Execute()
        {
            // Get the data
            var data = mExecutionEnvironment.GetDataSlot("*message").Data;

            // Send the message to the console
            mExecutionEnvironment.Console.Print(data.ToString());

            return NullData.Instance;
        }
    }
}
