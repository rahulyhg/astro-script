﻿using Astro.Engine.DataTypes;
using Astro.Engine.Symbols;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A node for constructing a new object.
    /// </summary>
    public class ConstructorNode : InterpreterParseNode
    {
        private ObjectSymbol mObjectSymbol;
        private InterpreterParseNode[] mParameters;

        /// <summary>
        /// Create a new constructor node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="objectSymbol">The symbol for the object we are constructing.</param>
        /// <param name="parameters">The parameters for the constructor call.</param>
        public ConstructorNode(Token token, ObjectSymbol objectSymbol, InterpreterParseNode[] parameters)
            :base(token)
        {
            mObjectSymbol = objectSymbol;
            mParameters = parameters;
        }

        /// <summary>
        /// Execute this node.
        /// </summary>
        /// <returns></returns>
        public override Data Execute()
        {
            // Create the object data
            var data = mObjectSymbol.CreateNewObjectData();

            // Don't bother if the object doesn't have a constructor
            if (mObjectSymbol.HasConstructor)
            {
                var objectData = (ObjectData)data;

                // Push a stack frame and store this object's member variables
                mExecutionEnvironment.PushStackFrame();
                foreach (var variable in objectData.Variables)
                {
                    mExecutionEnvironment.AddDataSlot(variable.Key, variable.Value);
                }

                try
                {
                    // Add the constructor's parameters to the frame
                    var symbol = mObjectSymbol.Constructor;
                    var parameters = symbol.Parameters;
                    for (int i = 0, length = parameters.Length; i < length; i++)
                    {
                        // Push a variable on to the stack for each parameter and execute its node
                        mExecutionEnvironment.CreateDataSlot(parameters[i], this.ExecuteNode(mParameters[i]));
                    }

                    // Call the constructor's parse tree
                    this.ExecuteNode((InterpreterParseNode)symbol.ParseTree);
                }
                finally
                {
                    // Pop the stack frame and return the object
                    mExecutionEnvironment.PopStackFrame();
                }
            }

            return data;
        }
    }
}
