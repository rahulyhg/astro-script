﻿using Astro.Engine.DataTypes;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A while loop node.
    /// </summary>
    public class WhileControlNode : LoopNode
    {
        private InterpreterParseNode mConditionNode;
        private InterpreterParseNode mBlockNode;

        /// <summary>
        /// Create the while control node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="conditionNode">The conditional expression node.</param>
        /// <param name="blockNode">The node for the while loop's block.</param>
        public WhileControlNode(Token token, InterpreterParseNode conditionNode, InterpreterParseNode blockNode)
            :base(token)
        {
            mConditionNode = conditionNode;
            mBlockNode = blockNode;
        }

        /// <summary>
        /// Execute this node.
        /// </summary>
        public override Data Execute()
        {
            mIsLoopBroken = false;

            // Loop while the condition is true
            while (!mIsLoopBroken && this.ExecuteNodeAsBoolean(mConditionNode).Value)
            {
                this.ExecuteNode(mBlockNode);
            }

            return null;
        }
    }
}
