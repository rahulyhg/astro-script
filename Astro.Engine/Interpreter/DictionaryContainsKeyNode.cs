﻿using System;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// Parse node for the dictionary object's ContainsKey function.
    /// </summary>
    public class DictionaryContainsKeyNode : InterpreterParseNode
    {
        /// <summary>
        /// Create a new dictionary contains key node.
        /// </summary>
        public DictionaryContainsKeyNode()
            : base(new Token())
        { }

        /// <summary>
        /// Execute the node.
        /// </summary>
        public override Data Execute()
        {
            // Get the data
            var key = mExecutionEnvironment.GetDataSlot("*key").Data;

            // Get the dictionary
            var dictionary = ((EngineDictionaryData)mExecutionEnvironment.GetDataSlot("*dictionary").Data).Value;

            // Check whether the dictionary contains the key
            if (dictionary.ContainsKey(key))
            {
                this.FindCallingFunctionNode().ReturnData = BooleanData.TrueInstance;
            }
            else
            {
                this.FindCallingFunctionNode().ReturnData = BooleanData.FalseInstance;
            }
            return null;
        }
    }
}
