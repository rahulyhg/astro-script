﻿using System.Collections.Generic;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Parsing;
using Astro.Engine.Tokenization;
using System.Linq;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// An aggregation of multiple nodes.
    /// </summary>
    public class AggregateNode : InterpreterParseNode
    {
        private List<InterpreterParseNode> mNodes;
        private int mLength;

        /// <summary>
        /// Create the aggregate node.
        /// </summary>
        /// <param name="nodes">The nodes to aggregate.</param>
        public AggregateNode(List<InterpreterParseNode> nodes)
            :base(new Token())
        {
            mNodes = nodes;
        }

        /// <summary>
        /// Execute this node.
        /// </summary>
        public override Data Execute()
        {
            // Execute each node
            Data data = NullData.Instance;
            mLength = mNodes.Count;
            for (int i = 0; i < mLength; i++)
            {
                data = this.ExecuteNode(mNodes[i]);
            }

            return data;
        }

        /// <summary>
        /// Adds a node to this aggregate node.
        /// </summary>
        /// <param name="node">The node to add.</param>
        public void AddNode(InterpreterParseNode node)
        {
            mNodes.Add(node);
        }

        /// <summary>
        /// Ends the execution of the nodes on this aggregate.
        /// </summary>
        public void EndExecution()
        {
            mLength = int.MinValue;
        }
    }
}
