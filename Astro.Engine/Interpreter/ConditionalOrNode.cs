﻿using Astro.Engine.DataTypes;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A node for a conditional OR.
    /// </summary>
    public class ConditionalOrNode : InterpreterParseNode
    {
        private InterpreterParseNode mLeftNode;
        private InterpreterParseNode mRightNode;

        /// <summary>
        /// Create the conditional OR node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="leftNode">The left node for the OR.</param>
        /// <param name="rightNode">The right node for the OR.</param>
        public ConditionalOrNode(Token token, InterpreterParseNode leftNode, InterpreterParseNode rightNode)
            :base(token)
        {
            mLeftNode = leftNode;
            mRightNode = rightNode;
        }

        /// <summary>
        /// Execute this conditional OR.
        /// </summary>
        /// <returns>The result of the OR.</returns>
        public override Data Execute()
        {
            if (this.ExecuteNodeAsBoolean(mLeftNode).Value || this.ExecuteNodeAsBoolean(mRightNode).Value)
            {
                return BooleanData.TrueInstance;
            }
            else
            {
                return BooleanData.FalseInstance;
            }
        }
    }
}
