﻿using Astro.Engine.DataTypes;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A node for a logical AND.
    /// </summary>
    public class LogicalAndNode : InterpreterParseNode
    {
        private InterpreterParseNode mLeftNode;
        private InterpreterParseNode mRightNode;

        /// <summary>
        /// Create the logical AND node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="leftNode">The left node for the AND.</param>
        /// <param name="rightNode">The right node for the AND.</param>
        public LogicalAndNode(Token token, InterpreterParseNode leftNode, InterpreterParseNode rightNode)
            :base(token)
        {
            mLeftNode = leftNode;
            mRightNode = rightNode;
        }

        /// <summary>
        /// Execute this logical AND.
        /// </summary>
        /// <returns>The result of the AND.</returns>
        public override Data Execute()
        {
            if (this.ExecuteNodeAsBoolean(mLeftNode).Value & this.ExecuteNodeAsBoolean(mRightNode).Value)
            {
                return BooleanData.TrueInstance;
            }
            else
            {
                return BooleanData.FalseInstance;
            }
        }
    }
}
