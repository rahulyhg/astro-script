﻿using Astro.Engine.DataTypes;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// Parse node for the list object's Remove function.
    /// </summary>
    public class ListRemoveNode : InterpreterParseNode
    {
        /// <summary>
        /// Create a new list remove node.
        /// </summary>
        public ListRemoveNode()
            : base(new Token())
        { }

        /// <summary>
        /// Execute the node.
        /// </summary>
        public override Data Execute()
        {
            // Get the data
            var toRemove = mExecutionEnvironment.GetDataSlot("*toRemove").Data;

            // Get the list
            var list = ((EngineListData)mExecutionEnvironment.GetDataSlot("*list").Data).Value;

            // Remove the item from the list
            if (list.Remove(toRemove))
            {
                this.FindCallingFunctionNode().ReturnData = BooleanData.TrueInstance;
            }
            else
            {
                this.FindCallingFunctionNode().ReturnData = BooleanData.FalseInstance;
            }
            return null;
        }
    }
}
