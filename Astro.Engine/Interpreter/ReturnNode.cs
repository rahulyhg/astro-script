﻿using Astro.Engine.DataTypes;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// Node for returning from function calls.
    /// </summary>
    public class ReturnNode : InterpreterParseNode
    {
        protected Data mReturnData;

        /// <summary>
        /// Create the return node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        public ReturnNode(Token token)
            :base(token)
        { }

        /// <summary>
        /// Execute this node.
        /// </summary>
        /// <returns>The data at this node.</returns>
        public override Data Execute()
        {
            // Move up the chain of executing nodes and end execution at aggregate nodes
            var executingNode = this.ExecutingNode;
            while (executingNode != null)
            {
                // End aggregate execution
                var aggregateNode = executingNode as AggregateNode;
                if (aggregateNode != null)
                {
                    aggregateNode.EndExecution();
                }
                else
                {
                    // Break loops
                    var loopNode = executingNode as LoopNode;
                    if (loopNode != null)
                    {
                        loopNode.BreakLoop();
                    }
                    else
                    {
                        var functionCallNode = executingNode as FunctionCallNode;
                        if (functionCallNode != null)
                        {
                            // Break since we've found the calling node
                            functionCallNode.ReturnData = mReturnData;
                            break;
                        }
                    }
                }

                executingNode = executingNode.ExecutingNode;
            }

            return NullData.Instance;
        }
    }
}
