﻿using Astro.Engine.DataTypes;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A node for breaking out of loops.
    /// </summary>
    public class BreakNode : InterpreterParseNode
    {
        /// <summary>
        /// Create a break node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        public BreakNode(Token token)
            :base(token)
        { }

        /// <summary>
        /// Execute this node.
        /// </summary>
        /// <returns>The data at this node.</returns>
        public override Data Execute()
        {
            // Move up the chain of executing nodes until we find the loop node
            var executingNode = this.ExecutingNode;
            while (executingNode != null)
            {
                // End execution at aggregates
                var aggregateNode = executingNode as AggregateNode;
                if (aggregateNode != null)
                {
                    aggregateNode.EndExecution();
                }
                else
                {
                    // Break the loop
                    var loopNode = executingNode as LoopNode;
                    if (loopNode != null)
                    {
                        loopNode.BreakLoop();
                        break;
                    }
                }

                executingNode = executingNode.ExecutingNode;
            }

            return null;
        }
    }
}
