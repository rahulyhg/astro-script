﻿using Astro.Engine.DataTypes;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A node for a conditional AND.
    /// </summary>
    public class ConditionalAndNode : InterpreterParseNode
    {
        private InterpreterParseNode mLeftNode;
        private InterpreterParseNode mRightNode;

        /// <summary>
        /// Create the conditional AND node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="leftNode">The left node for the AND.</param>
        /// <param name="rightNode">The right node for the AND.</param>
        public ConditionalAndNode(Token token, InterpreterParseNode leftNode, InterpreterParseNode rightNode)
            :base(token)
        {
            mLeftNode = leftNode;
            mRightNode = rightNode;
        }

        /// <summary>
        /// Execute this conditional AND.
        /// </summary>
        /// <returns>The result of the AND.</returns>
        public override Data Execute()
        {
            if (this.ExecuteNodeAsBoolean(mLeftNode).Value && this.ExecuteNodeAsBoolean(mRightNode).Value)
            {
                return BooleanData.TrueInstance;
            }
            else
            {
                return BooleanData.FalseInstance;
            }
        }
    }
}
