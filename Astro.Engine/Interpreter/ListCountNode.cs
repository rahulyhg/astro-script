﻿using System;
using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// Parse node for the list object's Count function.
    /// </summary>
    public class ListCountNode : InterpreterParseNode
    {
        /// <summary>
        /// Create a new list count node.
        /// </summary>
        public ListCountNode()
            : base(new Token())
        { }

        /// <summary>
        /// Execute the node.
        /// </summary>
        public override Data Execute()
        {
            // Get the list and return its count
            ((FunctionCallNode)this.FindCallingFunctionNode()).ReturnData = new IntegerData(((EngineListData)mExecutionEnvironment.GetDataSlot("*list").Data).Value.Count);
            return null;
        }
    }
}
