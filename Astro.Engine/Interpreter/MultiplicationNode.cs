﻿using Astro.Engine.DataTypes;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A node for multiplying two data types together.
    /// </summary>
    public class MultiplicationNode : InterpreterParseNode
    {
        private InterpreterParseNode mLeftNode;
        private InterpreterParseNode mRightNode;

        /// <summary>
        /// Create the multiplication node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="leftNode">The left node for the multiplication.</param>
        /// <param name="rightNode">The right node for the multiplication.</param>
        public MultiplicationNode(Token token, InterpreterParseNode leftNode, InterpreterParseNode rightNode)
            :base(token)
        {
            mLeftNode = leftNode;
            mRightNode = rightNode;
        }

        /// <summary>
        /// Execute this multiplication.
        /// </summary>
        /// <returns>The result of the multiplication.</returns>
        public override Data Execute()
        {
            // Resolve the left and right nodes
            var leftData = this.ExecuteNode(mLeftNode);
            var rightData = this.ExecuteNode(mRightNode);
            var leftType = leftData.Type;
            var rightType = rightData.Type;

            // Left hand integer
            if (leftType == DataType.Integer)
            {
                // Right hand integer
                if (rightType == DataType.Integer)
                {
                    return new IntegerData(((IntegerData)leftData).Value *
                                           ((IntegerData)rightData).Value);
                }
                // Right hand float
                else if (rightType == DataType.Float)
                {
                    return new FloatData(((IntegerData)leftData).Value *
                                         ((FloatData)rightData).Value);
                }
                else
                {
                    this.ThrowMatchException(leftType, rightType);
                }
            }
            // Left hand float
            else if (leftType == DataType.Float)
            {
                // Right hand integer
                if (rightType == DataType.Integer)
                {
                    return new FloatData(((FloatData)leftData).Value *
                                         ((IntegerData)rightData).Value);
                }
                // Right hand float
                else if (rightType == DataType.Float)
                {
                    return new FloatData(((FloatData)leftData).Value *
                                         ((FloatData)rightData).Value);
                }
                else
                {
                    this.ThrowMatchException(leftType, rightType);
                }
            }
            else
            {
                this.ThrowMatchException(leftType, rightType);
            }

            return NullData.Instance;
        }

        /// <summary>
        /// Throw an exception because there was not a match between the two types.
        /// </summary>
        /// <param name="left">The left type.</param>
        /// <param name="right">The right type.</param>
        private void ThrowMatchException(DataType left, DataType right)
        {
            throw new RuntimeException(mToken, ErrorCode.TypeMismatch, string.Format("Cannot perform a multiplication on types {0} and {1}.", left.ToString(), right.ToString()));
        }
    }
}