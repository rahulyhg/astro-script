﻿using Astro.Engine.DataTypes;
using Astro.Engine.Symbols;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Interpreter
{
    /// <summary>
    /// A variable node.
    /// </summary>
    public class VariableNode : DataSlotNode
    {
        /// <summary>
        /// This variable's symbol.
        /// </summary>
        protected Symbol mVariableSymbol;

        /// <summary>
        /// Create the variable node.
        /// </summary>
        /// <param name="token">The token responsible for this node.</param>
        /// <param name="variableSymbol">The symbol for this variable.</param>
        public VariableNode(Token token, Symbol variableSymbol)
            :base(token)
        {
            mVariableSymbol = variableSymbol;
        }

        /// <summary>
        /// The symbol for this node's variable.
        /// </summary>
        public Symbol Symbol
        {
            get { return mVariableSymbol; }
        }

        /// <summary>
        /// Execute this node.
        /// </summary>
        /// <returns>The variable's data.</returns>
        public override Data Execute()
        {
            // Return this variable's data
            mDataSlot = mExecutionEnvironment.GetDataSlot(mVariableSymbol.Identifier);
            return mDataSlot.Data;
        }
    }
}
