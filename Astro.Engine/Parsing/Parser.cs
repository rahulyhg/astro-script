﻿using System;
using System.Linq;
using System.Collections.Generic;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Symbols;
using Astro.Engine.Tokenization;

namespace Astro.Engine.Parsing
{
    /// <summary>
    /// Handles the parsing of Astro.
    /// </summary>
    public class Parser
    {
        /// <summary>
        /// Stores expression operator token types and their precedences.
        /// </summary>
        private Dictionary<TokenType, int> mExpressionOperators = new Dictionary<TokenType, int>()
        {
            { TokenType.ParenthesisBlockStart, -1},
            { TokenType.ConditionalOr, 0 },
            { TokenType.ConditionalAnd, 1 },
            { TokenType.LogicalOr, 2 },
            { TokenType.LogicalAnd, 3 },
            { TokenType.EqualsComparision, 4 },
            { TokenType.NotEqualComparison, 4 },
            { TokenType.GreaterThanComparison, 5 },
            { TokenType.GreaterThanOrEqualToComparison, 5 },
            { TokenType.LessThanComparison, 5 },
            { TokenType.LessThanOrEqualToComparison, 5 },
            { TokenType.Addition, 6 },
            { TokenType.Subtraction, 6 },
            { TokenType.Multiplication, 7 },
            { TokenType.Division, 7 },
            { TokenType.Modulo, 7 },
            { TokenType.BooleanNegation, 8 },
            { TokenType.NumericNegation, 8 },
        };

        /// <summary>
        /// Stores expression value token types.
        /// </summary>
        private HashSet<TokenType> mExpressionValues = new HashSet<TokenType>()
        {
            TokenType.Name,
            TokenType.NumericLiteral,
            TokenType.StringLiteral,
            TokenType.True,
            TokenType.False,
            TokenType.Null,
            TokenType.New, // Constructor calls
            TokenType.Increment, // For prefix incremented variables
            TokenType.Decrement, // For prefix decremented variables
        };

        private SymbolTable mSymbolTable;
        private ParseNodeFactory mParseNodeFactory;
        private ErrorLog mErrorLog;

        /// <summary>
        /// Create the parser.
        /// </summary>
        /// <param name="symbolTable">The symbolvtable this parser will store and read symbols from.</param>
        /// <param name="parseNodeFactory">The parse node factory used to create the parse nodes.</param>
        /// <param name="errorLog">The error log to log errors in.</param>
        public Parser(SymbolTable symbolTable, ParseNodeFactory parseNodeFactory, ErrorLog errorLog)
        {
            mSymbolTable = symbolTable;
            mParseNodeFactory = parseNodeFactory;
            mErrorLog = errorLog;
        }

        /// <summary>
        /// Parse the given token stream and output the parse tree for the program.
        /// </summary>
        /// <param name="stream">The token stream we are parsing.</param>
        /// <param name="program">The parse tree for the program.</param>
        /// <returns>Whether this program was successfully parsed.</returns>
        public bool Parse(TokenStream stream, out ParseNode program)
        {
            try
            {
                // Scan for global functions and object, adding them to the symbol
                // table but leaving the parsing of their bodies for later
                var uninitialisedFunctions = new List<Tuple<FunctionSymbol, TokenStream, Token[]>>();
                var uninitialisedObjects = new List<Tuple<ObjectSymbol, TokenStream>>();
                while (true)
                {
                    // Declare output variables
                    FunctionSymbol functionSymbol;
                    ObjectSymbol objectSymbol;
                    TokenStream blockStream;
                    Token[] parameters;

                    // Parse a function
                    if (this.FunctionDefinition(stream, out functionSymbol, out blockStream, out parameters))
                    {
                        uninitialisedFunctions.Add(Tuple.Create(functionSymbol, blockStream, parameters));
                    }
                    else if (this.ObjectDefinition(stream, out objectSymbol, out blockStream))
                    {
                        uninitialisedObjects.Add(Tuple.Create(objectSymbol, blockStream));
                    }
                    else
                    {
                        // Move the stream along and check for the end of the stream
                        if (stream.Read().Type == TokenType.EndOfStream)
                        {
                            break;
                        }
                    }
                }

                // Parse the global code/statements now that we have the symbols for 
                // the global functions and objects.
                stream.Reset();
                ParseNode globalStatements;
                this.Statement_0(stream, out globalStatements);

                // Check whether there are still tokens to parse
                if (stream.Position < stream.Tokens.Count - 2)
                {
                    var token = stream.Read();
                    throw new InvalidSyntaxException(token, ErrorCode.InvalidStatement, string.Format("Invalid statement: {0}", token.Lexeme));
                }

                // Parse the global function bodies
                foreach (var function in uninitialisedFunctions)
                {
                    ParseNode parseTree;
                    this.FunctionBody(function.Item2, function.Item3, out parseTree);
                    function.Item1.Initialise(parseTree, function.Item3.Select(p => p.Lexeme).ToArray());
                }

                // Parse object bodies
                foreach (var objectDefinition in uninitialisedObjects)
                {
                    string[] memberVariables;
                    string[] staticVariables;
                    FunctionSymbol[] memberFunctions;
                    FunctionSymbol[] staticFunctions;
                    FunctionSymbol constructor;
                    this.ObjectBody(objectDefinition.Item2, out memberVariables, out staticVariables, out memberFunctions, out staticFunctions, out constructor);
                    objectDefinition.Item1.Initialise(memberVariables, staticVariables, memberFunctions, staticFunctions, constructor);
                }

                // Create an execution environment node and return it with the global statements
                program = globalStatements;
                return true;
            }
            catch (InvalidSyntaxException e)
            {
                program = null;
                mErrorLog.LogError(e.ErrorCode, e.Message, e.Token.Line, e.Token.Column);
                return false;
            }
        }

        #region Object Definition Parsing

        /// <summary>
        /// Parse an object defintiion.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <returns>Whether the parse was successful.</returns>
        private bool ObjectDefinition(TokenStream stream, out ObjectSymbol objectSymbol, out TokenStream objectBlockStream)
        {
            var resetPosition = stream.Position;

            // First match the define keyword
            var token = stream.Read();
            if (token.Type != TokenType.Define)
            {
                stream.SetPosition(resetPosition);
                objectSymbol = null;
                objectBlockStream = null;
                return false;
            }
            var objectStart = stream.Position;

            // Match a name for the object definition
            token = stream.Read();
            if (token.Type != TokenType.Name)
            {
                throw new InvalidSyntaxException(token, ErrorCode.NameExpected, "Name expected for object definition.");
            }

            // Check that the identifier isn't already used
            Symbol symbol = null;
            if (mSymbolTable.TryGetSymbol(token, out symbol))
            {
                this.NameAlreadyInUse(token, symbol);
            }

            // Extract the token stream for this function's block
            if (!this.ExtractBraceBlockStream(stream, out objectBlockStream))
            {
                throw new InvalidSyntaxException(stream.CurrentToken, ErrorCode.BlockExpected, "Expected '{ }' block for object definition.");
            }

            // Remove the tokens used in this object and set the position
            var objectEnd = stream.Position + 1;
            stream.Tokens.RemoveRange(objectStart, objectEnd - objectStart);
            stream.SetPosition(objectStart);

            // Create the symbol
            objectSymbol = mSymbolTable.CreateObjectSymbol(token);
            return true;
        }

        /// <summary>
        /// Parse an object body and return its member variables and functions.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="memberVariables">The object's member variables.</param>
        /// <param name="staticVariables">The object's static variables.</param>
        /// <param name="memberFunctions">The object's member functions.</param>
        /// <param name="staticFunctions">The obbject's static functions.</param>
        /// <param name="constructor">The object's constructor.</param>
        private void ObjectBody(TokenStream stream, out string[] memberVariables, out string[] staticVariables, 
                                                    out FunctionSymbol[] memberFunctions, out FunctionSymbol[] staticFunctions, out FunctionSymbol constructor)
        {
            // Push the object's scope
            mSymbolTable.PushScope(ScopeType.Object);
            memberFunctions = null;
            memberVariables = null;
            staticFunctions = null;
            staticVariables = null;
            constructor = null;
            var uninitialisedStaticFunctions = new List<Tuple<FunctionSymbol, TokenStream, Token[]>>();
            var staticVariablesList = new List<Token>();
            try
            {
                // Parse this object's function headers before the declarations
                var uninitialisedFunctions = new List<Tuple<FunctionSymbol, TokenStream, Token[]>>();
                Tuple<FunctionSymbol, TokenStream, Token[]> constructorFunction = null;
                
                while (true)
                {
                    // Declare output variables
                    FunctionSymbol functionSymbol;
                    TokenStream blockStream;
                    Token[] parameters;

                    // Parse functions
                    if (this.FunctionDefinition(stream, out functionSymbol, out blockStream, out parameters))
                    {
                        uninitialisedFunctions.Add(Tuple.Create(functionSymbol, blockStream, parameters));
                    }
                    else if (this.StaticFunctionDefinition(stream, out functionSymbol, out blockStream, out parameters))
                    {
                        uninitialisedStaticFunctions.Add(Tuple.Create(functionSymbol, blockStream, parameters));
                    }
                    else if (this.ConstructorDefinition(stream, out functionSymbol, out blockStream, out parameters, constructorFunction != null))
                    {
                        constructorFunction = Tuple.Create(functionSymbol, blockStream, parameters);
                    }
                    else
                    {
                        // Move the stream along and check for the end of the stream
                        if (stream.Read().Type == TokenType.EndOfStream)
                        {
                            break;
                        }
                    }
                }

                // Parse this object's declarations
                stream.Reset();
                var memberVariablesList = new List<Token>();
                while (true)
                {
                    // Remove whitespace
                    this.ReadNewlines(stream);

                    // Parse a declaration
                    Token token;
                    var staticDeclaration = this.StaticDeclaration(stream, out token);
                    if (staticDeclaration || this.Declaration_2(stream, out token))
                    {
                        // Match an end of statement
                        if (!this.MultipleEndOfStatement(stream))
                        {
                            throw new InvalidSyntaxException(stream.Peek(), ErrorCode.EndOfStatementExpected, "Expected ';' or newline after variable declaration.");
                        }

                        // Ensure this name isn't already in use
                        Symbol symbol;
                        if (mSymbolTable.TryGetSymbol(token, out symbol))
                        {
                            this.NameAlreadyInUse(token, symbol);
                        }

                        // Create the symbol for the variable and store the token
                        mSymbolTable.CreateVariableSymbol(token);

                        // If we've parsed a static declaration add it to the list
                        if (staticDeclaration)
                        {
                            staticVariablesList.Add(token);
                        }
                        else
                        {
                            memberVariablesList.Add(token);
                        }
                    }
                    else
                    {
                        // Check for an end of stream
                        token = stream.Read();
                        if (token.Type == TokenType.EndOfStream)
                        {
                            break;
                        }
                        else
                        {
                            throw new NotImplementedException("Deal with check for different symbols.");
                        }
                    }
                }
                memberVariables = memberVariablesList.Select(v => v.Lexeme).ToArray();

                // Parse the object's constructor
                if (constructorFunction != null)
                {
                    ParseNode parseTree;
                    this.FunctionBody(constructorFunction.Item2, constructorFunction.Item3, out parseTree);
                    constructor = constructorFunction.Item1;
                    constructor.Initialise(parseTree, constructorFunction.Item3.Select(p => p.Lexeme).ToArray());
                }

                // Parse this object's function bodies
                var memberFunctionsList = new List<FunctionSymbol>();
                foreach (var function in uninitialisedFunctions)
                {
                    // parse the function body
                    ParseNode parseTree;
                    this.FunctionBody(function.Item2, function.Item3, out parseTree);

                    // Initialise the function
                    var symbol = function.Item1;
                    symbol.Initialise(parseTree, function.Item3.Select(p => p.Lexeme).ToArray());
                    memberFunctionsList.Add(symbol);
                }
                memberFunctions = memberFunctionsList.ToArray();
            }
            finally
            {
                // Pop the scope
                mSymbolTable.PopScope();
            }

            // Push the static scope and define the static variables and functions
            mSymbolTable.PushScope(ScopeType.Object);
            try
            {
                // Create the symbols for the variables again in this scope
                foreach (var token in staticVariablesList)
                {
                    mSymbolTable.CreateVariableSymbol(token);
                }
                staticVariables = staticVariablesList.Select(v => v.Lexeme).ToArray();

                // Initialise the static functions on the object symbol
                var staticFunctionsList = new List<FunctionSymbol>();
                foreach (var function in uninitialisedStaticFunctions)
                {
                    // parse the function body
                    ParseNode parseTree;
                    this.FunctionBody(function.Item2, function.Item3, out parseTree);

                    // Initialise the function
                    var symbol = function.Item1;
                    symbol.Initialise(parseTree, function.Item3.Select(p => p.Lexeme).ToArray());
                    staticFunctionsList.Add(symbol);
                }
                staticFunctions = staticFunctionsList.ToArray();
            }
            finally
            {
                mSymbolTable.PopScope();
            }
        }

        #endregion

        #region Function Definition Parsing

        /// <summary>
        /// Parse a static function definition.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="functionSymbol">The symbol for the parsed function.</param>
        /// <returns>Whether the parse was successful.</returns>
        private bool StaticFunctionDefinition(TokenStream stream, out FunctionSymbol functionSymbol, out TokenStream functionBlockStream, out Token[] parameters)
        {
            var resetPosition = stream.Position;

            // First match the static keyword
            var type = stream.Read().Type;
            if (type != TokenType.Static)
            {
                stream.SetPosition(resetPosition);
                functionSymbol = null;
                functionBlockStream = null;
                parameters = null;
                return false;
            }
            var functionStart = stream.Position;

            // Second match the function keyword
            var token = stream.Read();
            if (token.Type != TokenType.Function)
            {
                stream.SetPosition(resetPosition);
                functionSymbol = null;
                functionBlockStream = null;
                parameters = null;
                return false;
            }

            // Match a name for the function
            token = stream.Read();
            if (token.Type != TokenType.Name)
            {
                throw new InvalidSyntaxException(token, ErrorCode.NameExpected, "Name expected for function definition.");
            }

            // Check that the identifier isn't already used
            Symbol symbol = null;
            if (mSymbolTable.TryGetSymbol(token, out symbol))
            {
                this.NameAlreadyInUse(token, symbol);
            }

            // Parse the parameter list
            List<Token> parameterList;
            if (!this.ParameterList_0(stream, out parameterList))
            {
                throw new InvalidSyntaxException(stream.Peek(), ErrorCode.ParameterListExpected, "Parameter list expected after function name.");
            }
            parameters = parameterList.ToArray();

            // Extract the token stream for this function's block
            if (!this.ExtractBraceBlockStream(stream, out functionBlockStream))
            {
                throw new InvalidSyntaxException(stream.Peek(), ErrorCode.BlockExpected, "Expected '{ }' block for function definition.");
            }

            // Remove the tokens used in this function and set the position
            var functionEnd = stream.Position + 1;
            stream.Tokens.RemoveRange(functionStart, functionEnd - functionStart);
            stream.SetPosition(functionStart);

            // Create the uninitialised function symbol
            functionSymbol = mSymbolTable.CreateFunctionSymbol(token);
            return true;
        }

        /// <summary>
        /// Parse a function definition.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="functionSymbol">The symbol for the parsed function.</param>
        /// <param name="functionBlockStream">The stream for this function's block.</param>
        /// <param name="parameters">The parameters for this function.</param>
        /// <returns>Whether the parse was successful.</returns>
        private bool FunctionDefinition(TokenStream stream, out FunctionSymbol functionSymbol, out TokenStream functionBlockStream, out Token[] parameters)
        {
            var resetPosition = stream.Position;

            // First match the function keyword
            var token = stream.Read();
            if (token.Type != TokenType.Function)
            {
                stream.SetPosition(resetPosition);
                functionSymbol = null;
                functionBlockStream = null;
                parameters = null;
                return false;
            }
            var functionStart = stream.Position;

            // Match a name for the function
            token = stream.Read();
            if (token.Type != TokenType.Name)
            {
                throw new InvalidSyntaxException(token, ErrorCode.NameExpected, "Name expected for function definition.");
            }

            // Check that the identifier isn't already used
            Symbol symbol = null;
            if (mSymbolTable.TryGetSymbol(token, out symbol))
            {
                this.NameAlreadyInUse(token, symbol);
            }

            // Parse the parameter list
            List<Token> parameterList;
            if (!this.ParameterList_0(stream, out parameterList))
            {
                throw new InvalidSyntaxException(stream.Peek(), ErrorCode.ParameterListExpected, "Parameter list expected after function name.");
            }
            parameters = parameterList.ToArray();

            // Extract the token stream for this function's block
            if (!this.ExtractBraceBlockStream(stream, out functionBlockStream))
            {
                throw new InvalidSyntaxException(stream.Peek(), ErrorCode.BlockExpected, "Expected '{ }' block for function definition.");
            }

            // Remove the tokens used in this function and set the position
            var functionEnd = stream.Position + 1;
            stream.Tokens.RemoveRange(functionStart, functionEnd - functionStart);
            stream.SetPosition(functionStart);

            // Create the uninitialised function symbol
            functionSymbol = mSymbolTable.CreateFunctionSymbol(token);
            return true;
        }

        /// <summary>
        /// Parse an object's constructor
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="functionSymbol">The function symbol for this constructor.</param>
        /// <param name="constructorParsed">Whether a constructor has already been parsed.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool ConstructorDefinition(TokenStream stream, out FunctionSymbol functionSymbol, out TokenStream functionBlockStream, out Token[] parameters, bool constructorParsed)
        {
            var resetPosition = stream.Position;

            // First match the constructor keyword
            var token = stream.Read();
            if (token.Type != TokenType.Constructor)
            {
                stream.SetPosition(resetPosition);
                functionSymbol = null;
                functionBlockStream = null;
                parameters = null;
                return false;
            }
            var functionStart = stream.Position;

            // Throw an error if we've already parsed a constructor for the current object
            if (constructorParsed)
            {
                throw new InvalidSyntaxException(token, ErrorCode.ConstructorAlreadyExists, "A constructor has already been defined for this object.");
            }

            // Parse the parameter list
            List<Token> parameterList;
            if (!this.ParameterList_0(stream, out parameterList))
            {
                throw new InvalidSyntaxException(stream.Peek(), ErrorCode.ParameterListExpected, "Parameter list expected after 'constructor' keyword.");
            }
            parameters = parameterList.ToArray();

            // Extract the token stream for this function's block
            if (!this.ExtractBraceBlockStream(stream, out functionBlockStream))
            {
                throw new InvalidSyntaxException(stream.Peek(), ErrorCode.BlockExpected, "Expected '{ }' block for constructor definition.");
            }

            // Remove the tokens used in this function and set the position
            var functionEnd = stream.Position + 1;
            stream.Tokens.RemoveRange(functionStart, functionEnd - functionStart);
            stream.SetPosition(functionStart);

            // Create the uninitialised function symbol
            functionSymbol = mSymbolTable.CreateFunctionSymbol(token);
            return true;
        }
        
        /// <summary>
        /// Parse the body of a function and return its parse tree and parameters.
        /// </summary>
        /// <param name="stream">The stream for this function body.</param>
        /// <param name="parameters">The parameters of this function.</param>
        /// <param name="parseTree">The parse tree for this function body.</param>
        private void FunctionBody(TokenStream stream, Token[] parameters, out ParseNode parseTree)
        {
            // Push the function's scope
            mSymbolTable.PushScope(ScopeType.Function);

            try
            {
                // Create symbols for the parameters
                foreach (var parameter in parameters)
                {
                    // Ensure a symbol does not already exist for this parameter
                    Symbol symbol;
                    if (mSymbolTable.TryGetSymbol(parameter, out symbol))
                    {
                        this.NameAlreadyInUse(parameter, symbol);
                    }

                    // Create the symbol
                    mSymbolTable.CreateVariableSymbol(parameter);
                }

                // Parse the statements
                this.Statement_0(stream, out parseTree);
            }
            finally
            {
                // Pop the scope
                mSymbolTable.PopScope();
            }
        }

        #endregion

        #region Parameter List Parsing

        /// <summary>
        /// Parse a list of parameters.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="parameterList">The list of parameter tokens we are returning.</param>
        /// <returns></returns>
        private bool ParameterList_0(TokenStream stream, out List<Token> parameterList)
        {
            var resetPosition = stream.Position;

            // First match an opening parenthesis
            var token = stream.Read();
            if (token.Type != TokenType.ParenthesisBlockStart)
            {
                stream.SetPosition(resetPosition);
                parameterList = null;
                return false;
            }

            // Parse the parameters
            parameterList = new List<Token>();
            this.ParameterList_1(stream, parameterList);

            // Match the closing parenthesis
            token = stream.Read();
            if (token.Type != TokenType.ParenthesisBlockEnd)
            {
                throw new InvalidSyntaxException(token, ErrorCode.ClosingParenthesisMissing, "Closing ')' missing from function parameter list.");
            }
            return true;
        }

        /// <summary>
        /// Parse a parameter name and check for another.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="parameterList">The list of parameters.</param>
        private bool ParameterList_1(TokenStream stream, List<Token> parameterList)
        {
            var resetPosition = stream.Position;

            // Match a name
            var token = stream.Read();
            if (token.Type != TokenType.Name)
            {
                stream.SetPosition(resetPosition);
                return false;
            }
            parameterList.Add(token);

            // Match a comma token
            resetPosition = stream.Position;
            token = stream.Read();
            if (token.Type == TokenType.Comma)
            {
                // Parse another parameter
                if (!this.ParameterList_1(stream, parameterList))
                {
                    throw new InvalidSyntaxException(stream.Peek(), ErrorCode.NameExpected, "Additional parameter name expected after ','.");
                }
            }
            else
            {
                stream.SetPosition(resetPosition);
            }
            return true;
        }

        #endregion

        #region Statement Parsing

        /// <summary>
        /// Parse statements and aggregate them as one node.
        /// </summary>
        /// <param name="stream">The stream we are reading.</param>
        /// <param name="node">The aggregate node.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool Statement_0(TokenStream stream, out ParseNode node)
        {
            
            // Parse statements until there are no more
            var nodes = new List<ParseNode>();
            while (true)
            {
                // Remove white space
                this.ReadNewlines(stream);

                // Parse the statement and add it to the list
                ParseNode statementNode = null;
                if (!this.Statement_1(stream, out statementNode))
                {
                    break;
                }
                nodes.Add(statementNode);
            }

            // Create an aggregate node from the node list
            node = mParseNodeFactory.CreateAggregateNode(nodes);
            return true;
        }

        /// <summary>
        /// Parse a statement.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="node">The parse node for this statement.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool Statement_1(TokenStream stream, out ParseNode node)
        {
            // Check for a control structure
            if (this.IfControl_0(stream, out node) ||
                this.WhileControl(stream, out node) ||
                this.ForeachControl(stream, out node) ||
                this.ForControl(stream, out node))
            {
                return true;
            }

            // Check for a standard statement
            if (this.Statement_2(stream, out node))
            {
                // Match an end of statement
                if (!this.MultipleEndOfStatement(stream))
                {
                    // Check for a closing brace
                    if (stream.Peek().Type != TokenType.BraceBlockEnd)
                    {
                        throw new InvalidSyntaxException(stream.Peek(), ErrorCode.EndOfStatementExpected, "Expected ';' or newline for end of statement.");
                    }
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Parse a statement.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="node">The parse node for this statement.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool Statement_2(TokenStream stream, out ParseNode node)
        {
            // Check for a declaration
            if (this.Declaration_0(stream, out node))
            {
                return true;
            }

            // Check for a function call
            if (this.FunctionCall(stream, out node))
            {
                return true;
            }

            // Check for an assignment
            if (this.Assignment(stream, out node))
            {
                return true;
            }

            // Check for a return statement
            if (this.Return(stream, out node))
            {
                return true;
            }

            // Check for a break statement
            if (this.Break(stream, out node))
            {
                return true;
            }

            return false;
        }

        #endregion

        #region Break Parsing

        /// <summary>
        /// Parse a break statement.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="node">The break node.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool Break(TokenStream stream, out ParseNode node)
        {
            var resetPosition = stream.Position;

            // Check for a break token
            var token = stream.Read();
            if (token.Type != TokenType.Break)
            {
                node = null;
                stream.SetPosition(resetPosition);
                return false;
            }

            // Check that the current scope is valid
            var scope = mSymbolTable.CurrentScope;
            while (true)
            {
                // If we find a global, object, or function scope before a loop scope then we have an error
                if (scope == null)
                {
                    throw new Exception("Critical error: Scope should not be null in Return statement parse.");
                }
                else if (scope.Type == ScopeType.Object || scope.Type == ScopeType.Global || scope.Type == ScopeType.Function)
                {
                    throw new InvalidSyntaxException(token, ErrorCode.ReturnNotInFunction, "Break statements can only be inside loop blocks.");
                }
                else if (scope.Type == ScopeType.Loop)
                {
                    break;
                }
                scope = scope.Parent;
            }

            // Create the break node
            node = mParseNodeFactory.CreateBreakNode(token);
            return true;
        }

        #endregion

        #region Return Parsing

        /// <summary>
        /// Parse a return statement.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="node">The return statement node.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool Return(TokenStream stream, out ParseNode node)
        {
            var resetPosition = stream.Position;

            // Check for a return token
            var token = stream.Read();
            if (token.Type != TokenType.Return)
            {
                node = null;
                stream.SetPosition(resetPosition);
                return false;
            }

            // Check that the current scope is valid
            var scope = mSymbolTable.CurrentScope;
            while (true)
            {
                // If we find a global or object scope before a function scope then we have an error
                if (scope == null)
                {
                    throw new Exception("Critical error: Scope should not be null in Return statement parse.");
                }
                else if (scope.Type == ScopeType.Object || scope.Type == ScopeType.Global)
                {
                    throw new InvalidSyntaxException(token, ErrorCode.ReturnNotInFunction, "Return statements can only be inside function blocks.");
                }
                else if (scope.Type == ScopeType.Function)
                {
                    break;
                }
                scope = scope.Parent;
            }

            // Parse the expression
            ParseNode expressionNode;
            if (this.Expression(stream, out expressionNode))
            {
                node = mParseNodeFactory.CreateReturnValueNode(token, expressionNode);
            }
            else
            {
                node = mParseNodeFactory.CreateReturnNode(token);
            }

            return true;
        }

        #endregion

        #region Assignment Parsing

        /// <summary>
        /// Parse an assignment.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="node">The assignment node.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool Assignment(TokenStream stream, out ParseNode node)
        {
            var resetPosition = stream.Position;

            // Attempt to parse an increment/decrement
            if (this.IncrementDecrement(stream, out node))
            {
                return true;
            }
            else
            {
                stream.SetPosition(resetPosition);
            }

            // Parse the member we are assigning to
            if (!this.MemberAccessor_0(stream, out node))
            {
                return false;
            }

            // Get the assignment operator
            var token = stream.Read();
            var type = token.Type;
            if (!(type == TokenType.Assignment || type == TokenType.AdditionAssignment ||
                type == TokenType.SubtractionAssignment || type == TokenType.MultiplicationAssignment ||
                type == TokenType.DivisionAssignment || type == TokenType.ModuloAssignment))
            {
                throw new InvalidSyntaxException(token, ErrorCode.AssignmentOperatorExpected, "Assignment operator expected.");
            }

            // Parse the expression
            ParseNode expressionNode = null;
            if (!this.Expression(stream, out expressionNode))
            {
                throw new InvalidSyntaxException(stream.Peek(), ErrorCode.ExpressionExpected, "Expression expected after assignment operator.");
            }

            // Create the assignment node
            switch (type)
            {
                case TokenType.Assignment:
                    node = mParseNodeFactory.CreateAssignmentNode(token, node, expressionNode);
                    break;
                case TokenType.AdditionAssignment:
                    node = mParseNodeFactory.CreateAdditionAssignmentNode(token, node, expressionNode);
                    break;
                case TokenType.SubtractionAssignment:
                    node = mParseNodeFactory.CreateSubtractionAssignmentNode(token, node, expressionNode);
                    break;
                case TokenType.MultiplicationAssignment:
                    node = mParseNodeFactory.CreateMultiplicationAssignmentNode(token, node, expressionNode);
                    break;
                case TokenType.DivisionAssignment:
                    node = mParseNodeFactory.CreateDivisionAssignmentNode(token, node, expressionNode);
                    break;
                case TokenType.ModuloAssignment:
                    node = mParseNodeFactory.CreateModuloAssignmentNode(token, node, expressionNode);
                    break;
                default:
                    throw new Exception("Unrecognised assignment type.");
            }
            return true;
        }

        #endregion 

        #region Declaration Parsing

        /// <summary>
        /// Parse a variable declaration with the chance of an assignment.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="node">The parse node for this declaration.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool Declaration_0(TokenStream stream, out ParseNode node)
        {
            // Check for a declaration without an assignment first
            if (!this.Declaration_1(stream, out node))
            {
                return false;
            }

            // Check for an assignment
            var resetPosition = stream.Position;
            var token = stream.Read();
            var type = token.Type;
            if (type != TokenType.Assignment)
            {
                // No assignment but a successful declaration
                stream.SetPosition(resetPosition);
                return true;
            }

            // Parse the expression for the assignment
            ParseNode expressionNode = null;
            if (!this.Expression(stream, out expressionNode))
            {
                throw new InvalidSyntaxException(stream.Peek(), ErrorCode.ExpressionExpected, "Expression expected for assignment.");
            }

            // Create the assignment node
            node = mParseNodeFactory.CreateAssignmentNode(token, node, expressionNode);
            return true;
        }

        /// <summary>
        /// Parse a variable declaration on its own.
        /// </summary>
        /// <param name="stream">The stream we are reading.</param>
        /// <param name="node">The parsed node.</param>
        /// <returns>Whether the parse was successful.</returns>
        private bool Declaration_1(TokenStream stream, out ParseNode node)
        {
            // Parse the tokens
            Token token;
            if (!Declaration_2(stream, out token))
            {
                node = null;
                return false;
            }

            // Check that the identifier isn't already used
            Symbol symbol = null;
            if (mSymbolTable.TryGetSymbol(token, out symbol))
            {
                this.NameAlreadyInUse(token, symbol);
            }

            // Create the symbol and node
            var variableSymbol = mSymbolTable.CreateVariableSymbol(token);
            node = mParseNodeFactory.CreateDeclarationNode(token, variableSymbol);
            return true;
        }

        /// <summary>
        /// Parse a variable declaration and return the name token.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="token">The name token for this declaration.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool Declaration_2(TokenStream stream, out Token token)
        {
            var resetPosition = stream.Position;
            var type = stream.Read().Type;

            // First token should be declaration keyword
            if (type != TokenType.Declaration)
            {
                stream.SetPosition(resetPosition);
                token = null;
                return false;
            }

            // Next token must be a name token
            token = stream.Read();
            if (token.Type != TokenType.Name)
            {
                throw new InvalidSyntaxException(token, ErrorCode.NameExpected, "Variable name expected for declaration.");
            }
            return true;
        }

        /// <summary>
        /// Parses a static variable declaration.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="token">The output token for this declaration.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool StaticDeclaration(TokenStream stream, out Token token)
        {
            var resetPosition = stream.Position;
            var type = stream.Read().Type;

            // First token should be the static keyword
            if (type != TokenType.Static)
            {
                stream.SetPosition(resetPosition);
                token = null;
                return false;
            }

            // Second token should be a declaration keyword
            type = stream.Read().Type;
            if (type != TokenType.Declaration)
            {
                stream.SetPosition(resetPosition);
                token = null;
                return false;
            }

            // Next token must be a name token
            token = stream.Read();
            if (token.Type != TokenType.Name)
            {
                throw new InvalidSyntaxException(token, ErrorCode.NameExpected, "Variable name expected for declaration.");
            }
            return true;
        }

        #endregion

        #region For Parsing

        /// <summary>
        /// Parse a for control.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="node">The parse node for the control.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool ForControl(TokenStream stream, out ParseNode node)
        {
            var resetPosition = stream.Position;

            // Match a for token
            var token = stream.Read();
            if (token.Type != TokenType.For)
            {
                node = null;
                stream.SetPosition(resetPosition);
                return false;
            }

            // Push a control scope
            mSymbolTable.PushScope(ScopeType.Loop);

            ParseNode declarationNode = null;
            ParseNode conditionNode = null;
            ParseNode assignmentNode = null;
            ParseNode blockNode = null;
            try
            {
                // Check for an enclosing parenthesis
                var isEnclosed = false;
                resetPosition = stream.Position;
                token = stream.Read();
                if (token.Type != TokenType.ParenthesisBlockStart)
                {
                    stream.SetPosition(resetPosition);
                }
                else
                {
                    isEnclosed = true;
                }

                // Parse an optional declaration
                var optionalFound = this.Declaration_0(stream, out declarationNode);

                // Match an end of statement
                if (!this.EndOfStatement(stream, false))
                {   
                    if (optionalFound)
                    {
                        throw new InvalidSyntaxException(stream.Peek(), ErrorCode.EndOfStatementExpected, "End of statement expected after optional declaration.");
                    }
                    else
                    {
                        throw new InvalidSyntaxException(stream.Peek(), ErrorCode.EndOfStatementExpected, "Invalid declaration.");
                    }
                }

                // Parse an optional condition
                optionalFound = this.Expression(stream, out conditionNode);

                // Match an end of statement
                if (!this.EndOfStatement(stream, false))
                {
                    if (optionalFound)
                    {
                        throw new InvalidSyntaxException(stream.Peek(), ErrorCode.EndOfStatementExpected, "End of statement expected after optional condition.");
                    }
                    else
                    {
                        throw new InvalidSyntaxException(stream.Peek(), ErrorCode.EndOfStatementExpected, "Invalid conditional.");
                    }
                }

                // Parse an optional assignment
                this.Assignment(stream, out assignmentNode);

                // Parse the closing parenthesis
                if (isEnclosed)
                {
                    token = stream.Read();
                    if (token.Type != TokenType.ParenthesisBlockEnd)
                    {
                        throw new InvalidSyntaxException(token, ErrorCode.ClosingParenthesisMissing, "Closing ')' expected after for statement.");
                    }
                }

                // Parse the block
                if (!this.Block(stream, out blockNode, ScopeType.Control, false))
                {
                    throw new InvalidSyntaxException(stream.Peek(), ErrorCode.BlockExpected, "Expected '{ }' block after 'for' statement.");
                }
            }
            finally
            {
                // Pop the scope
                mSymbolTable.PopScope();
            }

            // Create the for control node
            node = mParseNodeFactory.CreateForControlNode(token, declarationNode, conditionNode, assignmentNode, blockNode);
            return true;
        }

        #endregion

        #region Foreach Parsing

        /// <summary>
        /// Parse a foreach control.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="node">The parse node for the control.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool ForeachControl(TokenStream stream, out ParseNode node)
        {
            var resetPosition = stream.Position;

            // Match a foreach token
            var token = stream.Read();
            if (token.Type != TokenType.Foreach)
            {
                stream.SetPosition(resetPosition);
                node = null;
                return false;
            }

            // Push a control scope for our declared variables
            mSymbolTable.PushScope(ScopeType.Loop);

            ParseNode declarationNode = null;
            ParseNode variableNode = null;
            ParseNode blockNode = null;
            try
            {
                // Check for an enclosing parenthesis
                var isEnclosed = false;
                resetPosition = stream.Position;
                token = stream.Read();
                if (token.Type != TokenType.ParenthesisBlockStart)
                {
                    stream.SetPosition(resetPosition);
                }
                else
                {
                    isEnclosed = true;
                }

                // Parse the variable declaration
                if (!this.Declaration_1(stream, out declarationNode))
                {
                    throw new InvalidSyntaxException(stream.Peek(), ErrorCode.DeclarationExpected, "Variable declaration expected for 'foreach' statement.");
                }

                // Match the in token
                var inToken = stream.Read();
                if (inToken.Type != TokenType.In)
                {
                    throw new InvalidSyntaxException(inToken, ErrorCode.KeywordExpected, "Keyword 'in' expected after variable declaration for 'foreach' statement.");
                }

                // Parse the variable for our foreach loop
                if (!this.VariableAccessor(stream, out variableNode))
                {
                    throw new InvalidSyntaxException(stream.Peek(), ErrorCode.VariableExpected, "Variable expected for 'foreach' statement.");
                }

                // Parse the closing parenthesis
                if (isEnclosed)
                {
                    token = stream.Read();
                    if (token.Type != TokenType.ParenthesisBlockEnd)
                    {
                        throw new InvalidSyntaxException(token, ErrorCode.ClosingParenthesisMissing, "Closing ')' expected after foreach statement.");
                    }
                }

                // Parse the block for this foreach statement
                if (!this.Block(stream, out blockNode, ScopeType.Control, false))
                {
                    throw new InvalidSyntaxException(stream.Peek(), ErrorCode.BlockExpected, "Expected '{ }' block after 'foreach' statement.");
                }
            }
            finally
            {
                // Pop the scope
                mSymbolTable.PopScope();
            }

            // Create the foreach node
            node = mParseNodeFactory.CreateForeachControlNode(token, declarationNode, variableNode, blockNode);
            return true;
        }

        #endregion

        #region While Control Parsing

        /// <summary>
        /// Parse a while control.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="node">The parse node for the while statement.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool WhileControl(TokenStream stream, out ParseNode node)
        {
            var resetPosition = stream.Position;

            // Match a while token
            var token = stream.Read();
            if (token.Type != TokenType.While)
            {
                stream.SetPosition(resetPosition);
                node = null;
                return false;
            }

            // Parse an expression for the condition
            ParseNode conditionNode = null;
            if (!this.Expression(stream, out conditionNode))
            {
                throw new InvalidSyntaxException(stream.Peek(), ErrorCode.ExpressionExpected, "Expression expected for 'while' statement.");
            }

            // Parse the control block
            ParseNode controlBlockNode = null;
            if (!this.Block(stream, out controlBlockNode, ScopeType.Loop))
            {
                throw new InvalidSyntaxException(stream.Peek(), ErrorCode.BlockExpected, "Expected '{ }' block after 'while' statement.");
            }

            // Create the while node
            node = mParseNodeFactory.CreateWhileControlNode(token, conditionNode, controlBlockNode);
            return true;
        }

        #endregion

        #region If Control Parsing

        /// <summary>
        /// Parse an if control and check for an else or else if control.
        /// </summary>
        /// <param name="stream">The stream we are reading.</param>
        /// <param name="node">The parsed if node.</param>
        /// <returns>Whether parsing was successful.</returns>
        private bool IfControl_0(TokenStream stream, out ParseNode node)
        {
            // Parse the first part of the control
            if (!this.IfControl_1(stream, out node))
            {
                return false;
            }

            // Check for an else
            var resetPosition = stream.Position;
            var token = stream.Read();
            if (token.Type != TokenType.Else)
            {
                // No else statement
                stream.SetPosition(resetPosition);
                return true;
            }

            // Since we are parsing an else, negate our if result
            node = mParseNodeFactory.CreateBooleanNegationNode(token, node);

            // Attempt to parse another if control
            ParseNode elseIf = null;
            if (this.IfControl_0(stream, out elseIf))
            {
                // Create the else if
                node = mParseNodeFactory.CreateIfControlNode(token, node, elseIf);
                return true;
            }

            // Parse an else block
            ParseNode elseBlockNode = null;
            if (!this.ControlBlock(stream, out elseBlockNode))
            {
                throw new InvalidSyntaxException(stream.Peek(), ErrorCode.BlockExpected, "Expected '{ }' block after 'else'.");
            }

            // Create the else
            node = mParseNodeFactory.CreateIfControlNode(token, node, elseBlockNode);
            return true;
        }

        /// <summary>
        /// Parse an if control.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="node">The parse node for this statement.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool IfControl_1(TokenStream stream, out ParseNode node)
        {
            var resetPosition = stream.Position;
            var token = stream.Read();

            // Match an If token
            if (token.Type != TokenType.If)
            {
                stream.SetPosition(resetPosition);
                node = null;
                return false;
            }

            // Parse the expression
            ParseNode expressionNode = null;
            if (!this.Expression(stream, out expressionNode))
            {
                throw new InvalidSyntaxException(stream.Peek(), ErrorCode.ExpressionExpected, "Expression expected for 'if' statement.");
            }

            // Parse the if statement's control block
            ParseNode blockNode = null;
            if (!this.ControlBlock(stream, out blockNode))
            {
                throw new InvalidSyntaxException(stream.Peek(), ErrorCode.BlockExpected, "Expected '{ }' block after 'if' statement.");
            }

            // Create the if node
            node = mParseNodeFactory.CreateIfControlNode(token, expressionNode, blockNode);
            return true;
        }

        #endregion

        #region Function/Control Block Parsing

        /// <summary>
        /// Parse a control block and aggregate its parse nodes.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="node">The aggregate node.</param>
        /// <returns>Whether the parse was successful.</returns>
        private bool ControlBlock(TokenStream stream, out ParseNode node)
        {
            return this.Block(stream, out node, ScopeType.Control);
        }

        /// <summary>
        /// Parse a function or control block and aggregate its parse nodes.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="node">The aggregate node.</param>
        /// <param name="scopeType">The scope type for this block.</param>
        /// <param name="createScope">Whether scope should be created as part of this block call.</param>
        /// <returns>Whether the parse was successful.</returns>
        private bool Block(TokenStream stream, out ParseNode node, ScopeType scopeType, bool createScope = true)
        {
            var resetPosition = stream.Position;

            // Eat any newlines
            this.ReadNewlines(stream);

            // Match an opening brace
            var type = stream.Read().Type;
            if (type != TokenType.BraceBlockStart)
            {
                stream.SetPosition(resetPosition);
                node = null;
                return false;
            }

            // Eat more newlines
            this.ReadNewlines(stream);

            // Push the new scope
            if (createScope)
            {
                mSymbolTable.PushScope(scopeType);
            }

            try
            {
                // Parse the statements
                this.Statement_0(stream, out node);
            }
            finally
            {
                // Pop the scope
                if (createScope)
                {
                    mSymbolTable.PopScope();
                }
            }

            // Eat more newlines
            this.ReadNewlines(stream);

            // Match the closing brace
            var token = stream.Read();
            if (token.Type != TokenType.BraceBlockEnd)
            {
                var blockType = "";
                switch (scopeType)
                {
                    case ScopeType.Control:
                        blockType = "control";
                        break;
                    case ScopeType.Function:
                        blockType = "function";
                        break;
                    default:
                        throw new Exception("Unrecognised scope type for this block.");
                }
                throw new InvalidSyntaxException(token, ErrorCode.ClosingBraceMissing, string.Format("Missing '{0}' to close {1} block.", "}", blockType));
            }

            // Eat more newlines
            this.ReadNewlines(stream);

            return true;
        }

        #endregion

        #region Expression Parsing

        /// <summary>
        /// Parse an expression and output its node.
        /// </summary>
        /// <param name="stream">The token stream to read from.</param>
        /// <param name="node">The expression node as an output parameter.</param>
        /// <returns>Whether the parse we successful.</returns>
        private bool Expression(TokenStream stream, out ParseNode node)
        {
            // Collect the tokens and nodes used in this expression
            var expressionTokens = new List<Tuple<Token, ParseNode>>();
            if (!this.Expression_0(stream, expressionTokens))
            {
                // Expression could not be parsed
                node = null;
                return false;
            }

            // Organise the expression tokens into postfix
            var postfix = this.InfixToPostfix(expressionTokens);

            // Create the final node for this expression
            node = this.CreateExpressionNode(postfix);
            return true;
        }

        /// <summary>
        /// Parses an expression.
        /// </summary>
        /// <param name="stream">The token stream we are reading from.</param>
        /// <param name="expressionTokens">The list of tokens encountered in this expression so far.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool Expression_0(TokenStream stream, List<Tuple<Token, ParseNode>> expressionTokens)
        {
            // First part should be a term
            if (!this.Term_0(stream, expressionTokens))
            {
                // Could not parse term, no harm
                return false;
            }

            // Parse the next part of the expression
            if (!this.Expression_1(stream, expressionTokens))
            {
                throw new InvalidSyntaxException(stream.Peek(), ErrorCode.InvalidExpressionTerm, "Invalid expression term.");
            }

            return true;
        }

        /// <summary>
        /// Parses part of an expression.
        /// </summary>
        /// <param name="stream">The token stream we are reading from.</param>
        /// <param name="expressionTokens">The list of tokens encountered in this expression so far.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool Expression_1(TokenStream stream, List<Tuple<Token, ParseNode>> expressionTokens)
        {
            var resetPosition = stream.Position;
            var token = stream.Read();
            var type = token.Type;

            // First token should be an expression operator
            if (mExpressionOperators.ContainsKey(type) && type != TokenType.BooleanNegation && type != TokenType.NumericNegation)
            {
                expressionTokens.Add(new Tuple<Token, ParseNode>(token, null));
            }
            else if (type == TokenType.ParenthesisBlockEnd && this.ClosingParenthesisExpected(expressionTokens))
            {
                // Closing parenthesis finishes current expression
                expressionTokens.Add(new Tuple<Token, ParseNode>(token, null));
                return true;
            }
            else
            {
                // No matching operator, return true as this will be the end of the statement
                stream.SetPosition(resetPosition);
                return true;
            }

            // Match the next part of the expression
            if (!this.Expression_0(stream, expressionTokens))
            {
                throw new InvalidSyntaxException(stream.Peek(), ErrorCode.InvalidExpressionTerm, "Invalid expression term.");
            }
            return true;
        }

        /// <summary>
        /// Parses a term and updates the token and node stacks for the expression.
        /// </summary>
        /// <param name="stream">The token stream we are reading from.</param>
        /// <param name="expressionTokens">The list of tokens encountered in this expression so far.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool Term_0(TokenStream stream, List<Tuple<Token, ParseNode>> expressionTokens)
        {
            var resetPosition = stream.Position;
            var token = stream.Read();
            var type = token.Type;

            // Check for negation
            if (type == TokenType.BooleanNegation ||
                type == TokenType.NumericNegation)
            {
                // Push the token
                expressionTokens.Add(new Tuple<Token, ParseNode>(token, null));
            }
            else
            {
                // Pop the reset position
                stream.SetPosition(resetPosition);
            }

            // Parse Term_1
            if (!this.Term_1(stream, expressionTokens))
            {
                // Term could not be parsed
                stream.SetPosition(resetPosition);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Parses a term and updates the token and node stacks for the expression.
        /// </summary>
        /// <param name="stream">The token stream we are reading from.</param>
        /// <param name="expressionTokens">The list of tokens encountered in this expression so far.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool Term_1(TokenStream stream, List<Tuple<Token, ParseNode>> expressionTokens)
        {
            var resetPosition = stream.Position;
            var token = stream.Read();

            // Check token type
            var type = token.Type;
            if (type == TokenType.NumericLiteral || type == TokenType.StringLiteral ||
                type == TokenType.True || type == TokenType.False || type == TokenType.Null)
            {
                expressionTokens.Add(new Tuple<Token, ParseNode>(token, mParseNodeFactory.CreateLiteralNode(token)));
            }
            else if (type == TokenType.ParenthesisBlockStart)
            {
                expressionTokens.Add(new Tuple<Token, ParseNode>(token, null));

                // Parse the Expression within this parenthesis block
                if (!this.Expression_0(stream, expressionTokens))
                {
                    throw new InvalidSyntaxException(token, ErrorCode.InvalidExpressionTerm, string.Format("Invalid expression term '{0}'.", token.Lexeme));
                }
            }
            else
            {
                stream.SetPosition(resetPosition);

                // No literals or parentheses were found, so check for a variable
                ParseNode parseNode = null;
                if (this.Variable_0(stream, out parseNode))
                {
                    expressionTokens.Add(new Tuple<Token, ParseNode>(token, parseNode));
                }
                else
                {
                    // Token cannot be parsed by Term_1
                    stream.SetPosition(resetPosition);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Organises a list of expression tokens into postfix notation from infix notation.
        /// </summary>
        /// <param name="expressionTokens">The infix list of expression tokens</param>
        /// <returns>The postfix queue of expression</returns>
        private List<Tuple<Token, ParseNode>> InfixToPostfix(List<Tuple<Token, ParseNode>> expressionTokens)
        {
            var postfix = new List<Tuple<Token, ParseNode>>();
            var stack = new Stack<Tuple<Token, ParseNode>>();
            stack.Push(null); // Null terminal

            // Loop over each infix token
            foreach (var tuple in expressionTokens)
            {
                var type = tuple.Item1.Type;

                // Add literals and variables straight to the postfix queue
                if (mExpressionValues.Contains(type))
                {
                    postfix.Add(tuple);
                }
                    // Push parenthesis block start onto the stack
                else if (type == TokenType.ParenthesisBlockStart)
                {
                    stack.Push(tuple);
                }
                    // Pop until a parenthesis block is found
                else if (type == TokenType.ParenthesisBlockEnd)
                {
                    var peek = stack.Peek();
                    while (peek.Item1.Type != TokenType.ParenthesisBlockStart)
                    {
                        postfix.Add(stack.Pop());
                        peek = stack.Peek();
                    }
                    stack.Pop();
                }
                    // Pop operators of a higher precendence from the stack, then push current operator
                else if (mExpressionOperators.ContainsKey(type))
                {
                    var operatorPrecedence = mExpressionOperators[type];
                    var peek = stack.Peek();
                    while (peek != null && mExpressionOperators[peek.Item1.Type] >= operatorPrecedence)
                    {
                        postfix.Add(stack.Pop());
                        peek = stack.Peek();
                    }
                    stack.Push(tuple);
                }
            }

            // Pop remaining stack to list
            while (stack.Peek() != null)
            {
                postfix.Add(stack.Pop());
            }

            return postfix;
        }

        /// <summary>
        /// Takes a list of expression tokens and creates the parse tree for the expression node.
        /// </summary>
        /// <param name="expressionTokens">The queue of expression nodes organised into postfix.</param>
        private ParseNode CreateExpressionNode(List<Tuple<Token, ParseNode>> expressionTokens)
        {
            var stack = new Stack<ParseNode>();
            foreach (var tuple in expressionTokens)
            {
                var type = tuple.Item1.Type;

                // Push value nodes straight onto the stack
                if (mExpressionValues.Contains(type))
                {
                    stack.Push(tuple.Item2);
                }
                    // Check for negation nodes
                else if (type == TokenType.BooleanNegation)
                {
                    var node = stack.Pop();
                    stack.Push(mParseNodeFactory.CreateBooleanNegationNode(tuple.Item1, node));
                }
                else if (type == TokenType.NumericNegation)
                {
                    var node = stack.Pop();
                    stack.Push(mParseNodeFactory.CreateNumericNegationNode(tuple.Item1, node));
                }
                    // Pop two nodes and create an operator node
                else if (mExpressionOperators.ContainsKey(type))
                {
                    var rightNode = stack.Pop();
                    var leftNode = stack.Pop();
                    stack.Push(mParseNodeFactory.CreateOperatorNode(tuple.Item1, leftNode, rightNode));
                }
                    // Unrecognised token
                else
                {
                    throw new Exception(string.Format("Unrecognised expression token: {0}", type.ToString()));
                }
            }

            // Check that there is one node left in the stack
            if (stack.Count != 1)
            {
                throw new Exception("Error creating expression node");
            }

            return stack.Pop();
        }

        /// <summary>
        /// Determines whether a closing parenthesis is expected in the list of expression tokens.
        /// </summary>
        /// <param name="expressionTokens">The list of expression tokens.</param>
        /// <returns>Whether a closing parenthesis is expected.</returns>
        private bool ClosingParenthesisExpected(List<Tuple<Token, ParseNode>> expressionTokens)
        {
            var parenthesisCount = 0;
            foreach (var tuple in expressionTokens)
            {
                var tokenType = tuple.Item1.Type;
                if (tokenType == TokenType.ParenthesisBlockStart)
                {
                    parenthesisCount += 1;
                }
                else if (tokenType == TokenType.ParenthesisBlockEnd)
                {
                    parenthesisCount -= 1;
                }
            }

            return parenthesisCount > 0;
        }

        #endregion

        #region Variable Parsing

        /// <summary>
        /// Parse a variable.
        /// </summary>
        /// <param name="stream">The token stream we are reading from.</param>
        /// <param name="node">The variable node as an output parameter.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool Variable_0(TokenStream stream, out ParseNode node)
        {
            var resetPosition = stream.Position;
            var token = stream.Read();

            // First check whether we're creating a new object
            if (token.Type == TokenType.New)
            {
                var nameToken = stream.Read();

                // Next token should be a name token
                if (nameToken.Type != TokenType.Name)
                {
                    throw new InvalidSyntaxException(nameToken, ErrorCode.ObjectDefinitionNameExpected, "Object name expected after 'new' keyword.");
                }

                // Find the object symbol for this name
                Symbol symbol;
                if (mSymbolTable.TryGetSymbol(nameToken, out symbol))
                {
                    if (symbol.Type != SymbolType.ObjectName)
                    {
                        throw new InvalidSyntaxException(nameToken, ErrorCode.NotAnObject, string.Format("The 'new' keyword must be followed by the name of an Object definition."));
                    }
                }
                else
                {
                    throw new InvalidSyntaxException(nameToken, ErrorCode.UnknownObjectName, string.Format("The name '{0}' was not recognised.", nameToken.Lexeme));
                }
                var objectSymbol = (ObjectSymbol)symbol;

                // Finally a parameter call
                var expressionList = new List<ParseNode>();
                if (!this.ParameterCall_0(stream, expressionList))
                {
                    throw new InvalidSyntaxException(stream.Peek(), ErrorCode.ParameterCallExpected, "Constructor parameter call expected after object name.");
                }

                // Create the constructor node
                node = mParseNodeFactory.CreateConstructorNode(token, objectSymbol, expressionList);
            }
            else
            {
                stream.SetPosition(resetPosition);

                // Not creating a new object, so parse variable as normal
                if (!this.Variable_1(stream, out node))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Parse a variable.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="node">The parse node for this variable.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool Variable_1(TokenStream stream, out ParseNode node)
        {
            var resetPosition = stream.Position;

            // Check for a function call
            if (this.FunctionCall(stream, out node))
            {
                return true;
            }
            else
            {
                stream.SetPosition(resetPosition);
            }

            // Check for a prefix increment/decrement
            var token = stream.Read();
            var prefixType = token.Type;
            var prefix = prefixType == TokenType.Increment || prefixType == TokenType.Decrement;
            if (!prefix)
            {
                stream.SetPosition(resetPosition);
            }

            // Check for a name
            if (!this.MemberAccessor_0(stream, out node))
            {
                if (!prefix)
                {
                    // No prefix so no error
                    return false;
                }

                throw new InvalidSyntaxException(stream.Peek(), ErrorCode.VariableExpected, string.Format("Variable expected after '{0}' prefix.", token.Lexeme));
            }

            // Check for a postfix increment/decrement
            resetPosition = stream.Position;
            token = stream.Read();
            var postfixType = token.Type;
            if (postfixType == TokenType.Increment || postfixType == TokenType.Decrement)
            {
                if (prefix)
                {
                    throw new InvalidSyntaxException(token, ErrorCode.PrefixAndPostfix, "Both a prefix and a postfix increment/decrement cannot be used on the same variable.");
                }

                // Create the postfix node
                if (postfixType == TokenType.Increment)
                {
                    node = mParseNodeFactory.CreatePostfixIncrementNode(token, node);
                }
                else if (postfixType == TokenType.Decrement)
                {
                    node = mParseNodeFactory.CreatePostfixDecrementnode(token, node);
                }
            }
            else
            {
                stream.SetPosition(resetPosition);
            }

            // Create the prefix increment/decrement
            if (prefix)
            {
                // Create the prefix node
                if (prefixType == TokenType.Increment)
                {
                    node = mParseNodeFactory.CreatePrefixIncrementNode(token, node);
                }
                else if (prefixType == TokenType.Decrement)
                {
                    node = mParseNodeFactory.CreatePrefixDecrementNode(token, node);
                }
            }
            
            return true;
        }

        #endregion

        #region Increment Decrement Parsing

        /// <summary>
        /// Parse only a variable with an increment or a decrement.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="node">The parse node for this increment/decrement.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool IncrementDecrement(TokenStream stream, out ParseNode node)
        {
            var resetPosition = stream.Position;

            // Check for a prefix increment/decrement
            var token = stream.Read();
            var prefixType = token.Type;
            var prefix = prefixType == TokenType.Increment || prefixType == TokenType.Decrement;
            if (!prefix)
            {
                stream.SetPosition(resetPosition);
            }

            // Check for a name
            if (!this.MemberAccessor_0(stream, out node))
            {
                if (!prefix)
                {
                    // No prefix so no error
                    return false;
                }

                throw new InvalidSyntaxException(stream.Peek(), ErrorCode.VariableExpected, string.Format("Variable expected after '{0}' prefix.", token.Lexeme));
            }

            // Check for a postfix increment/decrement
            token = stream.Read();
            var postfixType = token.Type;
            if (postfixType == TokenType.Increment || postfixType == TokenType.Decrement)
            {
                if (prefix)
                {
                    throw new InvalidSyntaxException(token, ErrorCode.PrefixAndPostfix, "Both a prefix and a postfix increment/decrement cannot be used on the same variable.");
                }

                // Create the postfix node
                if (postfixType == TokenType.Increment)
                {
                    node = mParseNodeFactory.CreatePostfixIncrementNode(token, node);
                }
                else if (postfixType == TokenType.Decrement)
                {
                    node = mParseNodeFactory.CreatePostfixDecrementnode(token, node);
                }
            }
            else if (prefix)
            {
                // Create the prefix node
                if (prefixType == TokenType.Increment)
                {
                    node = mParseNodeFactory.CreatePrefixIncrementNode(token, node);
                }
                else if (prefixType == TokenType.Decrement)
                {
                    node = mParseNodeFactory.CreatePrefixDecrementNode(token, node);
                }
            }
            else
            {
                // False since we only care about a variable with an increment or decrement
                stream.SetPosition(resetPosition);
                return false;
            }

            return true;
        }

        #endregion

        #region Member Accessor Parsing

        /// <summary>
        /// Returns a member that is being accessed.
        /// </summary>
        /// <param name="stream">The token stream we are reading from.</param>
        /// <param name="node">This accessor's node.</param>
        /// <returns>Whether this parse succeeded.</returns>
        private bool MemberAccessor_0(TokenStream stream, out ParseNode node)
        {
            var resetPosition = stream.Position;
            var token = stream.Read();

            // Check for a name token
            if (token.Type != TokenType.Name)
            {
                // No name token here
                stream.SetPosition(resetPosition);
                node = null;
                return false;
            }

            // Ensure The symbol for this name exists
            Symbol symbol = null;
            if (!mSymbolTable.TryGetSymbol(token, out symbol))
            {
                // Name has not been declared
                throw new InvalidSyntaxException(token, ErrorCode.ScopeDoesNotContainName, 
                                                 string.Format("The name '{0}' does not exist in the current scope.", token.Lexeme));
            }
            
            // Check recursively for other parts in the access
            var nameTokens = new List<Token>();
            if (!this.MemberAccessor_1(stream, nameTokens))
            {
                throw new InvalidSyntaxException(stream.Peek(), ErrorCode.MemberNameExpected, "Invalid member accessor.");
            }

            // Create the node for the initial symbol
            if (symbol.Type == SymbolType.VariableName)
            {
                node = mParseNodeFactory.CreateVariableNode(token, symbol);
            }
            else if (symbol.Type == SymbolType.FunctionName)
            {
                // Check that we aren't accessing a function as if it were a variable
                if (nameTokens.Count > 0)
                {
                    throw new InvalidSyntaxException(token, ErrorCode.FunctionUsedAsVariable, 
                                                     string.Format("Function '{0}' is used as if it is a variable.", token.Lexeme));
                }

                node = mParseNodeFactory.CreateFunctionNode(token, (FunctionSymbol)symbol);
            }
            else if (symbol.Type == SymbolType.ObjectName)
            {
                // Check that we aren't accessing a function as if it were a variable
                if (nameTokens.Count == 0)
                {
                    throw new InvalidSyntaxException(token, ErrorCode.ObjectUsedAsVariableOrFunction,
                                                     string.Format("Object '{0}' is used as if it is a variable or function.", token.Lexeme));
                }

                node = mParseNodeFactory.CreateObjectNode(token, symbol);
            }
            else
            {
                throw new Exception("Symbol type not recognised for name.");
            }

            // Add each name accessor as an accessor node
            foreach (var name in nameTokens)
            {
                node = mParseNodeFactory.CreateAccessorNode(name, node);
            }
            return true;
        }

        /// <summary>
        /// Recursively builds up a member accessor.
        /// </summary>
        /// <param name="stream">The token stream we are reading from.</param>
        /// <param name="nameTokens">The name tokens used in this accessor.</param>
        /// <returns>Whether the parse was successful.</returns>
        private bool MemberAccessor_1(TokenStream stream, List<Token> nameTokens)
        {
            var resetPosition = stream.Position;
            var token = stream.Read();

            // Match a member accessor
            if (token.Type != TokenType.MemberAccessor)
            {
                stream.SetPosition(resetPosition);
                return true;
            }

            // Match a name
            token = stream.Read();
            if (token.Type != TokenType.Name)
            {
                throw new InvalidSyntaxException(token, ErrorCode.MemberNameExpected, "Invalid member name.");
            }
            nameTokens.Add(token);

            // Check for the next part
            if (!this.MemberAccessor_1(stream, nameTokens))
            {
                // Parse failed
                stream.SetPosition(resetPosition);
                return false;
            }

            return true;
        }

        #endregion

        #region Variable Accessor Parsing

        /// <summary>
        /// Parse a variable accessor. This is for when a variable is needed from a standard
        /// variable or a return value from a function but we don't want an increment or decrement operator.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="node">The node for the variable.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool VariableAccessor(TokenStream stream, out ParseNode node)
        {
            // Attempt to parse a function call
            if (this.FunctionCall(stream, out node))
            {
                return true;
            }

            // Attempt to parse a member accessor instead
            if (this.MemberAccessor_0(stream, out node))
            {
                return true;
            }
            return false;
        }

        #endregion

        #region Function Call Parsing

        /// <summary>
        /// Parse a function call.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="node">The node for this function call.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool FunctionCall(TokenStream stream, out ParseNode node)
        {
            var resetPosition = stream.Position;

            // Parse the member accessor
            ParseNode accessorNode;
            if (!this.MemberAccessor_0(stream, out accessorNode))
            {
                stream.SetPosition(resetPosition);
                node = null;
                return false;
            }

            // Check for a parameter call
            var expressionList = new List<ParseNode>();
            if (this.ParameterCall_0(stream, expressionList))
            {
                // Create the function call node
                node = mParseNodeFactory.CreateFunctionCallNode(stream.CurrentToken, accessorNode, expressionList);
                return true;
            }

            // Check for an indexer call
            ParseNode expression = null;
            if (!this.IndexerCall(stream, out expression))
            {
                stream.SetPosition(resetPosition);
                node = null;
                return false;
            }

            // Create the indexer call node
            node = mParseNodeFactory.CreateIndexerCallNode(stream.CurrentToken, accessorNode, expression);
            return true;
        }

        #endregion

        #region Indexer Call Parsing

        /// <summary>
        /// Parse an indexer call and its parameter expression.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="expression">The indexer expression.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool IndexerCall(TokenStream stream, out ParseNode expression)
        {
            var resetPosition = stream.Position;

            // Match an opening bracket
            var token = stream.Read();
            if (token.Type != TokenType.BracketBlockStart)
            {
                stream.SetPosition(resetPosition);
                expression = null;
                return false;
            }

            // Parse the expression
            if (!this.Expression(stream, out expression))
            {
                throw new InvalidSyntaxException(stream.Peek(), ErrorCode.ExpressionExpected, "Expression for indexer call expected.");
            }

            // Match the closing parenthesis
            token = stream.Read();
            if (token.Type != TokenType.BracketBlockEnd)
            {
                throw new InvalidSyntaxException(token, ErrorCode.ClosingBracketMissing, "Closing ']' missing.");
            }
            return true;
        }

        #endregion

        #region Parameter Call Parsing

        /// <summary>
        /// Parse a parameter call and collect the parameter expressions.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="expressionList">The list of expression nodes.</param>
        /// <returns>Whether this parse was successful.</returns>
        private bool ParameterCall_0(TokenStream stream, List<ParseNode> expressionList)
        {
            var resetPosition = stream.Position;

            // Match an opening parenthesis
            var token = stream.Read();
            if (token.Type != TokenType.ParenthesisBlockStart)
            {
                stream.SetPosition(resetPosition);
                return false;
            }

            // Parse the parameter names
            this.ParameterCall_1(stream, expressionList);
            
            // Match the closing parenthesis
            token = stream.Read();
            if (token.Type != TokenType.ParenthesisBlockEnd)
            {
                throw new InvalidSyntaxException(token, ErrorCode.ClosingParenthesisMissing, "Closing ')' missing.");
            }
            return true;
        }

        /// <summary>
        /// Collect parameter expressions for the parameter call.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="expressionList">The list to store the expression nodes in.</param>
        /// <returns>Whether an expression node was found.</returns>
        private bool ParameterCall_1(TokenStream stream, List<ParseNode> expressionList)
        {
            // Parse an expression
            ParseNode expressionNode = null;
            if (!this.Expression(stream, out expressionNode))
            {
                return false;
            }
            expressionList.Add(expressionNode);

            // Match a comma separator
            var resetPosition = stream.Position;
            var token = stream.Read();
            if (token.Type != TokenType.Comma)
            {
                stream.SetPosition(resetPosition);
                return true;
            }

            // Parse more parameters
            if (!this.ParameterCall_1(stream, expressionList))
            {
                throw new InvalidSyntaxException(stream.Peek(), ErrorCode.ParameterExpected, string.Format("Parameter expected after '{0}'.", token.Lexeme));
            }
            return true;
        }

        #endregion

        #region End of Statement Parsing

        /// <summary>
        /// Match multiple end of statement tokens.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="acceptEndOfStream">Whether the end of stream token is acceptable.</param>
        /// <returns>Whether an end of statement was found.</returns>
        private bool MultipleEndOfStatement(TokenStream stream, bool acceptEndOfStream = true)
        {
            // Match an end of statement
            if (this.EndOfStatement(stream, acceptEndOfStream))
            {
                // Set acceptEndOfStream to false to stop infinite loops.
                this.MultipleEndOfStatement(stream, false);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Match an end of statement.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="acceptEndOfStream">Whether the end of stream token is acceptable.</param>
        /// <returns>Whether an end of statement was found.</returns>
        private bool EndOfStatement(TokenStream stream, bool acceptEndOfStream)
        {
            var resetPosition = stream.Position;

            // Check for end of statement
            var type = stream.Read().Type;
            if (type == TokenType.ImplicitEndOfStatement ||
                type == TokenType.EndOfStatement ||
                (acceptEndOfStream && type == TokenType.EndOfStream))
            {
                return true;
            }

            stream.SetPosition(resetPosition);
            return false;
        }

        /// <summary>
        /// Read new line tokens until a different token is encountered.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        private void ReadNewlines(TokenStream stream)
        {
            var resetPosition = stream.Position;
            var type = stream.Read().Type;
            if (type != TokenType.ImplicitEndOfStatement)
            {
                stream.SetPosition(resetPosition);
                return;
            }

            this.ReadNewlines(stream);
            return;
        }

        #endregion

        /// <summary>
        /// Called when a name is used which is already in use.
        /// </summary>
        /// <param name="token">The token for the name which is already in use.</param>
        /// <param name="symbol">The symbol for the pre-existing name.</param>
        private void NameAlreadyInUse(Token token, Symbol symbol)
        {
            // Find the usage
            var usage = "";
            switch (symbol.Type)
            {
                case SymbolType.VariableName:
                    usage = " as a Variable name";
                    break;
                case SymbolType.FunctionName:
                    usage = " as a Function name";
                    break;
                case SymbolType.ObjectName:
                    usage = " as an Object name";
                    break;
            }

            // Check the context
            var context = "";
            if (symbol.Scope.Type == ScopeType.Global)
            {
                context = " in the global scope";
            }

            // Create the message and throw the exception
            var message = string.Format("Name '{0}' is already in use{1}{2}.", token.Lexeme, usage, context);
            throw new InvalidSyntaxException(token, ErrorCode.NameAlreadyInUse, message);
        }

        /// <summary>
        /// Extracts the tokens inside a brace block from the given token stream and creates
        /// a new token stream from them.
        /// </summary>
        /// <param name="stream">The stream we are reading from.</param>
        /// <param name="extractStream">The extracted block stream.</param>
        /// <returns>Whether this extraction was a success.</returns>
        private bool ExtractBraceBlockStream(TokenStream stream, out TokenStream extractStream)
        {
            // Remove any separating new lines
            this.ReadNewlines(stream);

            // First token must be a brace block
            var token = stream.Read();
            if (token.Type != TokenType.BraceBlockStart)
            {
                extractStream = null;
                return false;
            }
            var streamStart = stream.Position + 1; // Plus one so that the start brace is not included
            var streamEnd = 0;
            var scopeDepth = 0;

            // Read tokens until a this block's matching end is found
            while (true)
            {
                token = stream.Read();
                if (token.Type == TokenType.BraceBlockStart)
                {
                    scopeDepth += 1;
                }
                else if (token.Type == TokenType.BraceBlockEnd)
                {
                    if (scopeDepth == 0)
                    {
                        // Block end found
                        streamEnd = stream.Position;
                        break;
                    }
                    else if (scopeDepth < 0)
                    {
                        throw new InvalidSyntaxException(token, ErrorCode.OpeningBraceMissing, "No matching opening brace found.");
                    }
                    else
                    {
                        scopeDepth -= 1;
                    }
                }
            }

            // Extract the stream
            extractStream = stream.ExtractStream(streamStart, streamEnd);

            // Reset the position
            stream.SetPosition(streamStart);
            return true;
        }
    }
}
