﻿using System;
using System.Collections.Generic;

namespace Astro.Engine.Tokenization
{
    /// <summary>
    /// A stream of tokens.
    /// </summary>
    public class TokenStream
    {
        private List<Token> mTokens = new List<Token>();
        private Token mCurrentToken;
        private int mPosition = -1;

        /// <summary>
        /// Create a new token stream.
        /// </summary>
        public TokenStream()
        { }

        #region Properties

        /// <summary>
        /// The position in the 
        /// </summary>
        public int Position
        {
            get { return mPosition; }
        }

        /// <summary>
        /// The number of tokens this stream contains.
        /// </summary>
        public int Count
        {
            get { return mTokens.Count; }
        }

        /// <summary>
        /// Whether the stream position is at the end.
        /// </summary>
        public bool IsFinished
        {
            get { return mPosition == mTokens.Count; }
        }

        /// <summary>
        /// The tokens within this stream.
        /// </summary>
        public List<Token> Tokens
        {
            get { return mTokens; }
        }

        /// <summary>
        /// The last token that was read.
        /// </summary>
        public Token CurrentToken
        {
            get { return mCurrentToken; }
        }

        #endregion

        /// <summary>
        /// Add a token to the stream.
        /// </summary>
        /// <param name="token">The token to add.</param>
        public void AddToken(Token token)
        {
            mTokens.Add(token);
        }

        /// <summary>
        /// Read the next token.
        /// </summary>
        public Token Read()
        {
            if (++mPosition >= mTokens.Count)
            {
                mPosition = mTokens.Count;
                return mTokens[mTokens.Count - 1];
            }
            mCurrentToken = mTokens[mPosition];
            return mCurrentToken;
        }

        /// <summary>
        /// Peek the next token.
        /// </summary>
        public Token Peek()
        {
            if (mPosition + 1 == mTokens.Count)
            {
                return mTokens[mPosition];
            }
            return mTokens[mPosition + 1];
        }

        /// <summary>
        /// Set this stream's position.
        /// </summary>
        /// <param name="position">The new position.</param>
        public void SetPosition(int position)
        {
            if (position < 0)
            {
                mPosition = -1;
                mCurrentToken = null;
                return;
            }
            else if (position > mTokens.Count)
            {
                throw new Exception("TokenStream position cannot exceed Token count.");
            }
            mPosition = position;
            mCurrentToken = mTokens[mPosition];
        }

        /// <summary>
        /// Set this stream's position back to the beginning.
        /// </summary>
        public void Reset()
        {
            mPosition = -1;
            mCurrentToken = null;
        }

        /// <summary>
        /// Extract a token stream from this stream.
        /// </summary>
        /// <param name="start">The inclusive start position for the extraction.</param>
        /// <param name="end">The exclusive end position for the extraction.</param>
        /// <returns>The extracted stream.</returns>
        public TokenStream ExtractStream(int start, int end)
        {
            var count = end - start;
            var extractedStream = new TokenStream();

            // Add the extracted tokens to the stream and remove them from the current
            for (int i = start; i < end; i++)
            {
                extractedStream.AddToken(mTokens[i]);
            }
            extractedStream.AddToken(new Token(TokenType.EndOfStream, "", extractedStream.Count, extractedStream.Count, 
                                               (extractedStream.Count > 0 ? extractedStream.Tokens[extractedStream.Count - 1].Line : 0), 
                                               0));
            mTokens.RemoveRange(start, count);

            return extractedStream;
        }
    }
}
