﻿using System;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Symbols;

namespace Astro.Engine.Tokenization
{
    /// <summary>
    /// The tokenizer for Astro; takes source code as its input and outputs
    /// a stream of tokens.
    /// </summary>
    public class Tokenizer
    {
        private SymbolTable mSymbolTable;
        private ErrorLog mErrorLog;
        private TokenStream mTokenStream;
        private string mSource;
        private int mPosition;
        private char mCharacter;
        private bool mIsSourceFinished;
        private int mLineCount;
        private int mLastLinePosition;

        /// <summary>
        /// Create a tokenizer.
        /// </summary>
        /// <param name="symbolTable">The table to store symbols in.</param>
        /// <param name="errorLog">The error log to report errors to..</param>
        public Tokenizer(SymbolTable symbolTable, ErrorLog errorLog)
        {
            mSymbolTable = symbolTable;
            mErrorLog = errorLog;
        }

        /// <summary>
        /// Tokenizes the given source code.
        /// </summary>
        /// <param name="source">The source code to tokenize.</param>
        /// <param name="tokenStream">The stream of tokens.</param>
        public bool Tokenize(string source, out TokenStream tokenStream)
        {
            mTokenStream = tokenStream = new TokenStream();
            mSource = source;
            mPosition = -1;
            mLineCount = 1;
            mLastLinePosition = 0;
            mIsSourceFinished = false;

            try
            {
                // Read the first token and begin tokenization
                this.Read();
                this.CreateTokens(mTokenStream, TokenType.None);
            }
            catch (InvalidSyntaxException e)
            {
                mErrorLog.LogError(e.ErrorCode, e.Message, e.Token.Line, e.Token.Column);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Create tokens and add them to the given stream.
        /// </summary>
        /// <param name="stream">The token stream to add tokens to.</param>
        /// <param name="context">The context we are creating tokens for..</param>
        private void CreateTokens(TokenStream stream, TokenType context)
        {
            var state = 0;
            var startPosition = 0;
            var lexeme = "";

            // Loop over the state
            while (state != -1)
            {
                switch (state)
                {
                    case 0: // Start state
                        
                        // Ignore whitespace
                        while (mCharacter == ' ' || mCharacter == '\t')
                        {  
                            // Check for source end
                            if (mIsSourceFinished)
                            {
                                // Add the end of stream token
                                var endOfStreamToken = new Token(TokenType.EndOfStream, "", mPosition, mPosition, mLineCount, mPosition - mLastLinePosition); 
                                mTokenStream.AddToken(endOfStreamToken);

                                // Check that we aren't expecting a closing brace or parenthesis
                                if (context == TokenType.BraceBlockStart)
                                {
                                    throw new InvalidSyntaxException(endOfStreamToken, ErrorCode.ClosingBraceMissing, "Closing '}' missing.");
                                }
                                else if (context == TokenType.ParenthesisBlockStart)
                                {
                                    throw new InvalidSyntaxException(endOfStreamToken, ErrorCode.ClosingParenthesisMissing, "Closing ')' missing.");
                                }
                                else if (context == TokenType.BracketBlockStart)
                                {
                                    throw new InvalidSyntaxException(endOfStreamToken, ErrorCode.ClosingBracketMissing, "Closing ']' missing.");
                                }
                                
                                state = -1;
                                return;
                            }
                            this.Read();
                        }
                        startPosition = mPosition;

                        // Check for letters and digits
                        if (char.IsLetter(mCharacter))
                        {
                            // Reading identifier
                            state = 1;
                        }
                        else if (char.IsDigit(mCharacter))
                        {
                            // Reading number
                            state = 2;
                        }
                        else
                        {
                            // Switch over individual characters
                            switch (mCharacter)
                            {
                                case '\n':  // Reading implicit end of statement
                                    state = 4;
                                    break;
                                case '\r':  // Reading implicit end of statement
                                    state = 4;
                                    break;
                                case ';':   // Reading explicit end of statement
                                    state = 5;
                                    break;
                                case '"':   // Reading string literal
                                    state = 6;
                                    break;
                                case ',':   // Reading comma operator
                                    state = 7;
                                    break;
                                case '+':   // Reading addition operator
                                    state = 8;
                                    break;
                                case '-':   // Reading subtraction or negation operator
                                    state = 9;
                                    break;
                                case '&':   // Reading boolean and operator
                                    state = 10;
                                    break;
                                case '|':   // Reading boolean or operator
                                    state = 11;
                                    break;
                                case '.':   // Reading member accessor
                                    state = 12;
                                    break;
                                case '=':   // Reading assignment or equals comparison
                                    this.CheckForAdditionalEquals(stream, startPosition, TokenType.Assignment, TokenType.EqualsComparision);
                                    break;
                                case '*':   // Reading multiplication operator
                                    this.CheckForAdditionalEquals(stream, startPosition, TokenType.Multiplication, TokenType.MultiplicationAssignment);
                                    break;
                                case '/':   // Reading division operator
                                    this.CheckForAdditionalEquals(stream, startPosition, TokenType.Division, TokenType.DivisionAssignment);
                                    break;
                                case '%':   // Reading Modulo operator
                                    this.CheckForAdditionalEquals(stream, startPosition, TokenType.Modulo, TokenType.ModuloAssignment);
                                    break;
                                case '!':   // Reading boolean negation operator
                                    this.CheckForAdditionalEquals(stream, startPosition, TokenType.BooleanNegation, TokenType.NotEqualComparison);
                                    break;
                                case '>':   // Reading greater than comparison
                                    this.CheckForAdditionalEquals(stream, startPosition, TokenType.GreaterThanComparison, TokenType.GreaterThanOrEqualToComparison);
                                    break;
                                case '<':   // Reading lesser than comparison
                                    this.CheckForAdditionalEquals(stream, startPosition, TokenType.LessThanComparison, TokenType.LessThanOrEqualToComparison);
                                    break;
                                case '{':   // Reading block start
                                    this.StartBlockToken(stream, startPosition, TokenType.BraceBlockStart);
                                    break;
                                case '(':   // Reading parenthesised block start
                                    this.StartBlockToken(stream, startPosition, TokenType.ParenthesisBlockStart);
                                    break;
                                case '[':   // Reading bracket block start
                                    this.StartBlockToken(stream, startPosition, TokenType.BracketBlockStart);
                                    break;
                                case '}':   // Reading block end
                                    if (context != TokenType.BraceBlockStart)
                                    {
                                        throw new InvalidSyntaxException(this.CreateErrorToken(), ErrorCode.OpeningBraceMissing, "Opening '{' missing.");
                                    }
                                    this.Read();
                                    this.AddToken(stream, startPosition, TokenType.BraceBlockEnd);
                                    state = -1;
                                    break;
                                case ')':   // Reading parenthesised block end
                                    if (context != TokenType.ParenthesisBlockStart)
                                    {
                                        throw new InvalidSyntaxException(this.CreateErrorToken(), ErrorCode.OpeningParenthesisMissing, "Opening '(' missing.");
                                    }
                                    this.Read();
                                    this.AddToken(stream, startPosition, TokenType.ParenthesisBlockEnd);
                                    state = -1;
                                    break;
                                case ']':   // Reading bracket block end
                                    if (context != TokenType.BracketBlockStart)
                                    {
                                        throw new InvalidSyntaxException(this.CreateErrorToken(), ErrorCode.OpeningBracketMissing, "Opening '[' missing.");
                                    }
                                    this.Read();
                                    this.AddToken(stream, startPosition, TokenType.BracketBlockEnd);
                                    state = -1;
                                    break;
                                default:
                                    // Character not recognised
                                    throw new InvalidSyntaxException(this.CreateErrorToken(), ErrorCode.UnrecognisedCharacter, string.Format("Unrecognised character: {0}", mCharacter));
                            }
                        }
                        break;
                    case 1: // Reading identifier

                        // Read until a non-digit or non-letter character is found
                        while (true)
                        {
                            this.Read();
                            if (!char.IsLetterOrDigit(mCharacter))
                            {
                                // Get the lexeme
                                lexeme = mSource.Substring(startPosition, mPosition - startPosition);

                                // See if there is already a symbol for this identifier
                                var tokenType = TokenType.Name;
                                Symbol symbol = null;
                                if (mSymbolTable.TryGetKeywordSymbol(lexeme, out symbol))
                                {
                                    tokenType = (TokenType)symbol.Type;
                                }

                                // Create the token from the returned symbol
                                stream.AddToken(new Token(tokenType, lexeme, startPosition, mPosition, mLineCount, mPosition - mLastLinePosition));

                                // Reset state
                                state = 0;
                                break;
                            }
                        }
                        break;
                    case 2: // Reading integer

                        // Read until a non-digit character or a decimal place is found
                        while (true)
                        {
                            this.Read();
                            if (mCharacter == '.')
                            {
                                // Reading decimal part
                                state = 3;
                                break;
                            }
                            else if (!char.IsDigit(mCharacter))
                            {
                                // Add the token
                                this.AddToken(stream, startPosition, TokenType.NumericLiteral);
                                
                                // Reset state
                                state = 0;
                                break;
                            }
                        }
                        break;
                    case 3: // Reading decimal part

                        // Ensure we have at least one digit
                        this.Read();
                        if (!char.IsDigit(mCharacter))
                        {
                            throw new InvalidSyntaxException(this.CreateErrorToken(), ErrorCode.MissingFractionalNumbers, "Missing fractional digits after decimal part.");
                        }
                        else
                        {
                            // Read until a non-digit character is found
                            while (true)
                            {
                                this.Read();
                                if (!char.IsDigit(mCharacter))
                                {
                                    // Add the token
                                    this.AddToken(stream, startPosition, TokenType.NumericLiteral);

                                    // Reset state
                                    state = 0;
                                    break;
                                }
                            }
                        }
                        break;
                    case 4: // Reading implicit end of statement

                        // Check for a carriage return
                        var carriageReturn = mCharacter == '\r';

                        // Move the position along
                        this.Read();

                        // If the last character was a carriage return and current is a newline, then skip the current
                        if (carriageReturn && mCharacter == '\n')
                        {
                            this.Read();
                        }

                        // If the last token was an implicit end of statement then we can ignore this one
                        if (stream.Count > 0 && stream.Tokens[stream.Count - 1].Type != TokenType.ImplicitEndOfStatement)
                        {
                            this.AddToken(stream, startPosition, TokenType.ImplicitEndOfStatement);
                        }

                        // Reset the state
                        state = 0;

                        // Since this is a new line, increase the line count and last line position
                        mLineCount++;
                        mLastLinePosition = mPosition;

                        break;
                    case 5: // Reading explicit end of statement

                        // Move the position along
                        this.Read();

                        // Add the token
                        this.AddToken(stream, startPosition, TokenType.EndOfStatement);

                        // Reset the state
                        state = 0;
                        break;
                    case 6: // Reading a string literal

                        // Read until we find the closing quotation marks
                        while (true)
                        {
                            this.Read();
                            if (mCharacter == '"')
                            {
                                // Move the position along
                                this.Read();

                                // Add the token
                                this.AddToken(stream, startPosition, TokenType.StringLiteral);

                                // Reset the state
                                state = 0;
                                break;
                            }
                            else if (mIsSourceFinished)
                            {
                                // Source has finished so no closing quotation was found
                                throw new InvalidSyntaxException(this.CreateErrorToken(), ErrorCode.DoubleQuotationExpected, "Expected double quotation mark (\") to close string.");
                            }
                        }
                        break;
                    case 7: // Reading comma operator

                        // Move the position along
                        this.Read();

                        // Add the token
                        this.AddToken(stream, startPosition, TokenType.Comma);

                        // Reset the state
                        state = 0;
                        break;
                    case 8: // Reading addition operator

                        // Move the position along
                        this.Read();

                        // Check for an addition assignment or increment
                        var additionTokenType = TokenType.Addition;
                        if (mCharacter == '=')
                        {
                            this.Read();
                            additionTokenType = TokenType.AdditionAssignment;
                        }
                        else if (mCharacter == '+')
                        {
                            this.Read();
                            additionTokenType = TokenType.Increment;
                        }

                        // Add the token
                        this.AddToken(stream, startPosition, additionTokenType);

                        // Reset the state
                        state = 0;
                        break;
                    case 9: // Reading subtraction or negation operator

                        // Peek the next character
                        var peeked = ' ';
                        if (!stream.IsFinished)
                        {
                            peeked = (char)this.Peek();
                        }
                        else
                        {
                            // Source has ended abruptly
                            throw new NotImplementedException();
                        }

                        // Check for a subtraction assignment or decrement operator
                        var subtractionTokenType = TokenType.Subtraction;
                        if (peeked == '=')
                        {
                            this.Read();
                            subtractionTokenType = TokenType.SubtractionAssignment;
                        }
                        else if (peeked == '-')
                        {
                            this.Read();
                            subtractionTokenType = TokenType.Decrement;
                        }
                        else if (stream.Count > 0)
                        {
                            // Check the previous token to see if this is a negation operator
                            var previousToken = stream.Tokens[stream.Count - 1].Type;
                            if (previousToken != TokenType.NumericLiteral && previousToken != TokenType.Name &&
                                previousToken != TokenType.ParenthesisBlockEnd)
                            {
                                // This is a numeric negation token
                                subtractionTokenType = TokenType.NumericNegation;
                            }
                        }

                        // Move the position along
                        this.Read();

                        // Add the token
                        this.AddToken(stream, startPosition, subtractionTokenType);

                        // Reset the state
                        state = 0;
                        break;
                    case 10: // Reading boolean and operator

                        // Move the position along
                        this.Read();

                        // Check for another & symbol
                        var andTokenType = TokenType.LogicalAnd;
                        if (mCharacter == '&')
                        {
                            this.Read();
                            andTokenType = TokenType.ConditionalAnd;
                        }

                        // Add the token
                        this.AddToken(stream, startPosition, andTokenType);

                        // Reset the state
                        state = 0;
                        break;
                    case 11: // Reading boolean or operator

                        // Move the position along
                        this.Read();

                        // Check for another | symbol
                        var orTokenType = TokenType.LogicalOr;
                        if (mCharacter == '|')
                        {
                            this.Read();
                            orTokenType = TokenType.ConditionalOr;
                        }

                        // Add the token
                        this.AddToken(stream, startPosition, orTokenType);

                        // Reset the state
                        state = 0;
                        break;
                    case 12: // Reading member accessor operator

                        // Move the position along
                        this.Read();

                        // Add the token
                        this.AddToken(stream, startPosition, TokenType.MemberAccessor);

                        // Reset the state
                        state = 0;
                        break;
                }
            }
        }

        /// <summary>
        /// Starts a block token and begins tokenizing its contents as inner tokens.
        /// </summary>
        /// <param name="stream">The stream we're currently adding tokens to.</param>
        /// <param name="startPosition">The start position of this token.</param>
        /// <param name="type">The type of block token.</param>
        private void StartBlockToken(TokenStream stream, int startPosition, TokenType type)
        {
            // Move the position on
            this.Read();

            // Add the block start token
            var token = this.AddToken(stream, startPosition, type);

            // Recurse through this function until we reach the end of the block
            this.CreateTokens(stream, type);
        }

        /// <summary>
        /// Checks for an additional equals symbol after the current symbol. If there is no additional
        /// equals symbol then a first choice token will be created, if there is an additional equals
        /// symbol then a second choice token will be created.
        /// </summary>
        /// <param name="stream">The stream we're adding the token to.</param>
        /// <param name="startPosition">The start position of this token.</param>
        /// <param name="type">The first choice token type.</param>
        /// <param name="typeWithEquals">The second choice token type to use if an additional equals is found.</param>
        private void CheckForAdditionalEquals(TokenStream stream, int startPosition, TokenType type, TokenType typeWithEquals)
        {
            // Move the position along
            this.Read();

            // Check for a greater than or equal to comparison
            var tokenType = type;
            if (mCharacter == '=')
            {
                this.Read();
                tokenType = typeWithEquals;
            }

            // Add the token
            this.AddToken(stream, startPosition, tokenType);
        }

        /// <summary>
        /// Creates and adds a token to the given stream.
        /// </summary>
        /// <param name="stream">The stream to add the token to.</param>
        /// <param name="startPosition">The start position of the token.</param>
        /// <param name="type">The type of the token to add.</param>
        private Token AddToken(TokenStream stream, int startPosition, TokenType type)
        {
            // Get the lexeme
            var lexeme = mSource.Substring(startPosition, mPosition - startPosition);

            // Create the token
            var token = new Token(type, lexeme, startPosition, mPosition, mLineCount, mPosition - mLastLinePosition);
            stream.AddToken(token);

            return token;
        }

        /// <summary>
        /// Create an error token for instances where a token is needed but none are present.
        /// </summary>
        /// <returns>The error token.</returns>
        private Token CreateErrorToken()
        {
            return new Token(TokenType.Error, "", mPosition, mPosition, mLineCount, mPosition - mLastLinePosition);
        }

        /// <summary>
        /// Read the next char code from the source and move the position along.
        /// </summary>
        private void Read()
        {
            mPosition++;
            if (mPosition >= mSource.Length)
            {
                // End of source.
                mCharacter = ' ';
                mIsSourceFinished = true;
            }
            else
            {
                mCharacter = mSource[mPosition];
            }
        }

        /// <summary>
        /// Peek the next char code from the source.
        /// </summary>
        /// <returns></returns>
        private int Peek()
        {
            return mSource[mPosition + 1];
        }
    }
}
