﻿
namespace Astro.Engine.Tokenization
{
    /// <summary>
    /// Defines the types of tokens.
    /// </summary>
    public enum TokenType
    {
        /// <summary>
        /// Does not represent any token.
        /// </summary>
        None = -1,

        /// <summary>
        /// Represents an identifier for a name.
        /// </summary>
        Name = 0,

        /// <summary>
        /// Represents a declaration token.
        /// </summary>
        Declaration = 1,

        /// <summary>
        /// Represents an if token.
        /// </summary>
        If = 2,

        /// <summary>
        /// Represents an else token.
        /// </summary>
        Else = 3,

        /// <summary>
        /// Represents a while token.
        /// </summary>
        While = 4,

        /// <summary>
        /// Represents a for token.
        /// </summary>
        For = 5,

        /// <summary>
        /// Represents a foreach token.
        /// </summary>
        Foreach = 6,

        /// <summary>
        /// Represents an in token.
        /// </summary>
        In = 7,

        /// <summary>
        /// Represents a define token.
        /// </summary>
        Define = 8,

        /// <summary>
        /// Represents a function token.
        /// </summary>
        Function = 9,

        /// <summary>
        /// Represents a true token.
        /// </summary>
        True = 10,

        /// <summary>
        /// Represents a false token.
        /// </summary>
        False = 11,

        /// <summary>
        /// Represents a new keyword token.
        /// </summary>
        New = 12,

        /// <summary>
        /// Represents a return token.
        /// </summary>
        Return = 13,

        /// <summary>
        /// Represents a constructor token.
        /// </summary>
        Constructor = 14,

        /// <summary>
        /// Represents a break token.
        /// </summary>
        Break = 15,

        /// <summary>
        /// Represents a static token.
        /// </summary>
        Static = 16,

        /// <summary>
        /// Represents a null token.
        /// </summary>
        Null = 17,

        /// <summary>
        /// Represents a numeric literal token.
        /// </summary>
        NumericLiteral,

        /// <summary>
        /// Represents an implicit end of statement token.
        /// </summary>
        ImplicitEndOfStatement,

        /// <summary>
        /// Represents an explicit end of statement token.
        /// </summary>
        EndOfStatement,

        /// <summary>
        /// Represents an assignment token.
        /// </summary>
        Assignment,

        /// <summary>
        /// Represents an equals comparison.
        /// </summary>
        EqualsComparision,

        /// <summary>
        /// Represents a string literal.
        /// </summary>
        StringLiteral,

        /// <summary>
        /// Represents a comma operator.
        /// </summary>
        Comma,

        /// <summary>
        /// Represents an addition operator.
        /// </summary>
        Addition,

        /// <summary>
        /// Represents an addition assignment operator.
        /// </summary>
        AdditionAssignment,

        /// <summary>
        /// Represents an increment operator.
        /// </summary>
        Increment,

        /// <summary>
        /// Represents a subtraction operator.
        /// </summary>
        Subtraction,

        /// <summary>
        /// Represents a subtraction assignment operator.
        /// </summary>
        SubtractionAssignment,

        /// <summary>
        /// Represents a decrement operator.
        /// </summary>
        Decrement,

        /// <summary>
        /// Represents a numeric negation operator.
        /// </summary>
        NumericNegation,

        /// <summary>
        /// Represents a multiplcation operator.
        /// </summary>
        Multiplication,

        /// <summary>
        /// Represents a multiplication assignment operator.
        /// </summary>
        MultiplicationAssignment,

        /// <summary>
        /// Represents a division operator.
        /// </summary>
        Division,

        /// <summary>
        /// Represents a division assignment operator.
        /// </summary>
        DivisionAssignment,

        /// <summary>
        /// Represents a modulo operator.
        /// </summary>
        Modulo,

        /// <summary>
        /// Represents a modulo assignment operator.
        /// </summary>
        ModuloAssignment,

        /// <summary>
        /// Represents a boolean negation operator.
        /// </summary>
        BooleanNegation,

        /// <summary>
        /// Represents a not equal comparision.
        /// </summary>
        NotEqualComparison,

        /// <summary>
        /// Represents the greater than comparison.
        /// </summary>
        GreaterThanComparison,

        /// <summary>
        /// Represents the greater than or equal to comparison.
        /// </summary>
        GreaterThanOrEqualToComparison,

        /// <summary>
        /// Represents the less than comparison.
        /// </summary>
        LessThanComparison,

        /// <summary>
        /// Represents the less than or equal to comparison.
        /// </summary>
        LessThanOrEqualToComparison,

        /// <summary>
        /// Represents a boolean and operator.
        /// </summary>
        LogicalAnd,

        /// <summary>
        /// Represents a boolean or operator.
        /// </summary>
        LogicalOr,

        /// <summary>
        /// Represents a short circuiting boolean and operator.
        /// </summary>
        ConditionalAnd,

        /// <summary>
        /// Represents a short circuiting boolean or operator.
        /// </summary>
        ConditionalOr,

        /// <summary>
        /// Represents the start of a brace block.
        /// </summary>
        BraceBlockStart,

        /// <summary>
        /// Represents the end of a brace block.
        /// </summary>
        BraceBlockEnd,

        /// <summary>
        /// Represents the start of a parenthesis block.
        /// </summary>
        ParenthesisBlockStart,

        /// <summary>
        /// Represents the end of a parenthesis block.
        /// </summary>
        ParenthesisBlockEnd,

        /// <summary>
        /// Represents the start of a bracket block.
        /// </summary>
        BracketBlockStart,

        /// <summary>
        /// Represents the end of a bracket block.
        /// </summary>
        BracketBlockEnd,

        /// <summary>
        /// Represents the end of the token stream.
        /// </summary>
        EndOfStream,

        /// <summary>
        /// Represents the member accessor token.
        /// </summary>
        MemberAccessor,

        /// <summary>
        /// Token used in the place of an error.
        /// </summary>
        Error,
    }
}
