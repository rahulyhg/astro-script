﻿using System.Diagnostics;

namespace Astro.Engine.Tokenization
{
    /// <summary>
    /// A token created by the tokenizer.
    /// </summary>
    [DebuggerDisplay("Token: {mTokenType}")]
    public class Token
    {
        private TokenType mTokenType;
        private string mLexeme;
        private int mStartPosition;
        private int mEndPosition;
        private int mLine;
        private int mColumn;

        /// <summary>
        /// Creates a new token.
        /// </summary>
        /// <param name="type">The token's type.</param>
        /// <param name="lexeme">The lexeme for this token.</param>
        /// <param name="startPosition">The start position of this token in the source.</param>
        /// <param name="endPosition">The end position of this token in the source.</param>
        /// <param name="line">The line this token occurs on.</param>
        /// <param name="column">The column number for the start of this token.</param>
        public Token(TokenType type, string lexeme, int startPosition, int endPosition, int line, int column)
        {
            mTokenType = type;
            mLexeme = lexeme;
            mStartPosition = startPosition;
            mEndPosition = endPosition;
            mLine = line;
            mColumn = column;
        }

        /// <summary>
        /// Creates a blank token.
        /// </summary>
        public Token()
        {
            mTokenType = TokenType.None;
            mLexeme = string.Empty;
        }

        /// <summary>
        /// Creates a blank named token.
        /// </summary>
        /// <param name="lexeme">The lexeme for this token.</param>
        public Token(string lexeme)
        {
            mTokenType = TokenType.None;
            mLexeme = lexeme;
        }

        #region Properties

        /// <summary>
        /// The type of the token.
        /// </summary>
        public TokenType Type
        {
            get { return mTokenType; }
        }

        /// <summary>
        /// The lexeme for this token.
        /// </summary>
        public string Lexeme
        {
            get { return mLexeme; }
        }

        /// <summary>
        /// The start position of this token in the source.
        /// </summary>
        public int StartPosition
        {
            get { return mStartPosition; }
        }

        /// <summary>
        /// The end position of this token in the source.
        /// </summary>
        public int EndPosition
        {
            get { return mEndPosition; }
        }

        /// <summary>
        /// The line this token occured on.
        /// </summary>
        public int Line
        {
            get { return mLine; }
        }

        /// <summary>
        /// The column of the start of this token.
        /// </summary>
        public int Column
        {
            get { return mColumn; }
        }

        #endregion
    }
}
