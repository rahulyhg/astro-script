﻿using System;
using System.Collections.Generic;
using Astro.Engine.Parsing;
using Astro.Engine.Symbols;
using Astro.Engine.Tokenization;

namespace Astro.Engine.DataTypes
{
    /// <summary>
    /// The function data type.
    /// </summary>
    public class FunctionData : Data
    {
        private FunctionSymbol mFunctionSymbol;
        private Dictionary<string, DataSlot> mContextVariables;

        /// <summary>
        /// Create the function data.
        /// </summary>
        /// <param name="functionSymbol">The symbol representing this function.</param>
        public FunctionData(FunctionSymbol functionSymbol)
        {
            mFunctionSymbol = functionSymbol;
            mContextVariables = new Dictionary<string, DataSlot>();
        }

        /// <summary>
        /// Create the function data.
        /// </summary>
        /// <param name="functionSymbol">The symbol representing this function.</param>
        /// <param name="contextVariables">The context variables for this function.</param>
        public FunctionData(FunctionSymbol functionSymbol, Dictionary<string, DataSlot> contextVariables)
        {
            mFunctionSymbol = functionSymbol;
            mContextVariables = contextVariables;
        }

        #region Properties

        /// <summary>
        /// This data's type.
        /// </summary>
        public override DataType Type
        {
            get { return DataType.Function; }
        }

        /// <summary>
        /// The parse tree for this function.
        /// </summary>
        public ParseNode ParseTree
        {
            get { return mFunctionSymbol.ParseTree; }
        }
        
        /// <summary>
        /// The parameters this function takes.
        /// </summary>
        public string[] Parameters
        {
            get { return mFunctionSymbol.Parameters; }
        }

        /// <summary>
        /// The variables present in this function's context.
        /// </summary>
        public Dictionary<string, DataSlot> ContextVariables
        {
            get { return mContextVariables; }
        }

        #endregion

        /// <summary>
        /// Create a copy of this data.
        /// </summary>
        /// <returns>A new Data instance with this Data's value.</returns>
        public override Data Copy()
        {
            // Should not be implemented
            return null;
        }

        /// <summary>
        /// Compare this data with another and return whether they are equal.
        /// </summary>
        /// <param name="data">The data to compare with this data.</param>
        /// <returns>The result as a boolean data.</returns>
        public override bool EqualityComparison(Data data)
        {
            // Should not be implemented
            throw new NotImplementedException();
        }
    }
}
