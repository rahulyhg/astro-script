﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Astro.Engine.DataTypes
{
    /// <summary>
    /// The data type for an internal dictionary, only to be utilised by engine code.
    /// </summary>
    public class EngineDictionaryData : Data
    {
        private Dictionary<Data, Data> mValue = new Dictionary<Data, Data>();

        /// <summary>
        /// Create the engine list data.
        /// </summary>
        public EngineDictionaryData()
        { }

        /// <summary>
        /// The dictionary value for this data.
        /// </summary>
        public Dictionary<Data, Data> Value
        {
            get { return mValue; }
        }

        /// <summary>
        /// The type of this data.
        /// </summary>
        public override DataType Type
        {
            get { return DataType.Engine; }
        }

        /// <summary>
        /// Create a copy of this data.
        /// </summary>
        /// <returns>A new Data instance with this Data's value.</returns>
        public override Data Copy()
        {
            // Data should not be copied
            return null;
        }

        /// <summary>
        /// Compare this data with another data type.
        /// </summary>
        /// <param name="data">The other data to compare with.</param>
        /// <returns>Whether the two data are equal.</returns>
        public override bool EqualityComparison(Data data)
        {
            return this == data;
        }
    }
}
