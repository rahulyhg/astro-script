﻿using System;
using System.Collections.Generic;
using Astro.Engine.ErrorReporting;
using Astro.Engine.Symbols;
using Astro.Engine.Symbols.TypeSymbols;
using Astro.Engine.Tokenization;

namespace Astro.Engine.DataTypes
{
    /// <summary>
    /// The list data type.
    /// </summary>
    public class ListData : ObjectData
    {
        private List<Data> mList;

        /// <summary>
        /// Create this object data.
        /// </summary>
        /// <param name="listSymbol">The symbol for this object.</param>
        public ListData(ListSymbol listSymbol)
            :base(listSymbol)
        {
            // Add the engine list data
            var engineListData = new EngineListData();
            mList = engineListData.Value;
            mVariables.Add("*list", new DataSlot(engineListData));
        }

        /// <summary>
        /// Index the object with the given value.
        /// </summary>
        /// <param name="index">The index to retrieve data from.</param>
        /// <returns>The data held at the given index.</returns>
        public override Data Index(Data index)
        {
            // Check that the index data is an integer
            if (index.Type != DataType.Integer)
            {
                throw new RuntimeException(new Token(), ErrorCode.TypeMismatch, "Objects of type 'List' can only be indexed by values of type 'Integer'.");
            }

            // Attempt to index the list
            var i = ((IntegerData)index).Value;
            try
            {
                return mList[i];
            }
            catch (ArgumentOutOfRangeException)
            {
                // Find whether the index was too high or too low
                if (i < 0)
                {
                    throw new RuntimeException(new Token(), ErrorCode.TypeMismatch, "Index must be greater than or equal to 0.");
                }
                else if (i >= mList.Count)
                {
                    throw new RuntimeException(new Token(), ErrorCode.TypeMismatch, "Index must be less than the number of list elements.");
                }
                else
                {
                    throw new Exception("Critical error: Index is neither too high nor too low.");
                }
            }
        }

        /// <summary>
        /// Get the string representation of this data.
        /// </summary>
        /// <returns>The string representation of this data.</returns>
        public override string ToString()
        {
            return "List";
        }
    }
}
