﻿namespace Astro.Engine.DataTypes
{
    /// <summary>
    /// The null data type.
    /// </summary>
    public class NullData : Data
    {
        /// <summary>
        /// The singleton instance of the null data.
        /// </summary>
        public static NullData Instance = new NullData();

        /// <summary>
        /// The type of this data.
        /// </summary>
        public override DataType Type
        {
            get { return DataType.Null; }
        }

        /// <summary>
        /// Create a copy of this data.
        /// </summary>
        /// <returns>A new Data instance with this Data's value.</returns>
        public override Data Copy()
        {
            return this;
        }

        /// <summary>
        /// Compare this data with another and return whether they are equal.
        /// </summary>
        /// <param name="data">The data to compare with this data.</param>
        /// <returns>The result.</returns>
        public override bool EqualityComparison(Data data)
        {
            return data.Type == DataType.Null;
        }

        /// <summary>
        /// Get the string representation of this data.
        /// </summary>
        /// <returns>The string representation of this data.</returns>
        public override string ToString()
        {
            return "null";
        }
    }
}
