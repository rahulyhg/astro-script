﻿
namespace Astro.Engine.DataTypes
{
    /// <summary>
    /// Base class for all data types.
    /// </summary>
    public abstract class Data
    {
        /// <summary>
        /// The type of this data.
        /// </summary>
        public abstract DataType Type
        {
            get;
        }

        /// <summary>
        /// Create a copy of this data.
        /// </summary>
        /// <returns>A new Data instance with this Data's value.</returns>
        public abstract Data Copy();

        /// <summary>
        /// Compare this data with another and return whether they are equal.
        /// </summary>
        /// <param name="data">The data to compare with this data.</param>
        /// <returns>Whether this data is equal to the parameter data.</returns>
        public abstract bool EqualityComparison(Data data);

        /// <summary>
        /// Checks this data for equality with another object.
        /// </summary>
        /// <param name="obj">The other object to check.</param>
        /// <returns>Whether this object and the other object are equal.</returns>
        public override bool Equals(object obj)
        {
            // Check that the object is of type data
            var data = obj as Data;
            if (data == null)
            {
                return false;
            }

            // Use the equality comparison
            return this.EqualityComparison(data);
        }

        /// <summary>
        /// Get the string representation of this data.
        /// </summary>
        /// <returns>The string representation of this data.</returns>
        public override string ToString()
        {
            return "Default Data";
        }
    }
}
