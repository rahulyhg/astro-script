﻿using System;

namespace Astro.Engine.DataTypes
{
    /// <summary>
    /// The string data type.
    /// </summary>
    public class StringData : Data
    {
        private string mValue;

        /// <summary>
        /// Create the string data type.
        /// </summary>
        /// <param name="value">This string's value.</param>
        public StringData(string value)
        {
            mValue = value;
        }

        /// <summary>
        /// The type of this data.
        /// </summary>
        public override DataType Type
        {
            get { return DataType.String; }
        }

        /// <summary>
        /// This string's value.
        /// </summary>
        public string Value
        {
            get { return mValue; }
            set { mValue = value; }
        }

        /// <summary>
        /// Create a copy of this data.
        /// </summary>
        /// <returns>A new Data instance with this Data's value.</returns>
        public override Data Copy()
        {
            return new StringData(mValue);
        }

        /// <summary>
        /// Compare this data with another and return whether they are equal.
        /// </summary>
        /// <param name="data">The data to compare with this data.</param>
        /// <returns>The result.</returns>
        public override bool EqualityComparison(Data data)
        {
            if (data.Type == DataType.String)
            {
                return mValue.Equals(((StringData)data).Value, StringComparison.Ordinal);
            }
            return false;
        }

        /// <summary>
        /// Gets the hash code for this data's value.
        /// </summary>
        /// <returns>The data's hash code.</returns>
        public override int GetHashCode()
        {
            return mValue.GetHashCode();
        }

        /// <summary>
        /// Get the string representation of this data.
        /// </summary>
        /// <returns>The string representation of this data.</returns>
        public override string ToString()
        {
            return mValue;
        }
    }
}
