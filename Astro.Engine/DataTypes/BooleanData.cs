﻿namespace Astro.Engine.DataTypes
{
    /// <summary>
    /// The boolean data type.
    /// </summary>
    public class BooleanData : Data
    {
        /// <summary>
        /// The singleton instance for the true boolean data.
        /// </summary>
        public static BooleanData TrueInstance = new BooleanData(true);

        /// <summary>
        /// The singleton instance for the false boolean data. 
        /// </summary>
        public static BooleanData FalseInstance = new BooleanData(false);

        private bool mValue;

        /// <summary>
        /// Create the boolean data type.
        /// </summary>
        /// <param name="value">This boolean's value.</param>
        private BooleanData(bool value)
        {
            mValue = value;
        }

        /// <summary>
        /// The type of this data.
        /// </summary>
        public override DataType Type
        {
            get { return DataType.Boolean; }
        }

        /// <summary>
        /// The boolean's value.
        /// </summary>
        public bool Value
        {
            get { return mValue; }
        }

        /// <summary>
        /// Returns the negation of this boolean value.
        /// </summary>
        public BooleanData Negate()
        {
            return (mValue ? FalseInstance : TrueInstance);
        }

        /// <summary>
        /// Returns the correct singleton instance for the given boolean value.
        /// </summary>
        /// <param name="boolean">The boolean value.</param>
        /// <returns>The boolean data for the given value.</returns>
        public static BooleanData GetBooleanData(bool boolean)
        {
            return (boolean ? BooleanData.TrueInstance : BooleanData.FalseInstance);
        }

        /// <summary>
        /// Create a copy of this data.
        /// </summary>
        /// <returns>A new Data instance with this Data's value.</returns>
        public override Data Copy()
        {
            return this;
        }

        /// <summary>
        /// Compare this data with another and return whether they are equal.
        /// </summary>
        /// <param name="data">The data to compare with this data.</param>
        /// <returns>The result as a boolean data.</returns>
        public override bool EqualityComparison(Data data)
        {
            if (data.Type == DataType.Boolean)
            {
                return mValue == ((BooleanData)data).Value;
            }
            return false;
        }

        /// <summary>
        /// Gets the hash code for this data's value.
        /// </summary>
        /// <returns>The data's hash code.</returns>
        public override int GetHashCode()
        {
            return mValue.GetHashCode();
        }

        /// <summary>
        /// Get the string representation of this data.
        /// </summary>
        /// <returns>The string representation of this data.</returns>
        public override string ToString()
        {
            return mValue.ToString();
        }
    }
}
