﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Astro.Engine.DataTypes
{
    /// <summary>
    /// The data type for an internal list, only to be utilised by engine code.
    /// </summary>
    public class EngineListData : Data
    {
        private List<Data> mValue = new List<Data>();

        /// <summary>
        /// Create the engine list data.
        /// </summary>
        public EngineListData()
        { }

        /// <summary>
        /// The list value for this data.
        /// </summary>
        public List<Data> Value
        {
            get { return mValue; }
        }

        /// <summary>
        /// The type of this data.
        /// </summary>
        public override DataType Type
        {
            get { return DataType.Engine; }
        }

        /// <summary>
        /// Create a copy of this data.
        /// </summary>
        /// <returns>A new Data instance with this Data's value.</returns>
        public override Data Copy()
        {
            // Data should not be copied
            return null;
        }

        /// <summary>
        /// Compare this data with another data type.
        /// </summary>
        /// <param name="data">The other data to compare with.</param>
        /// <returns>Whether the two data are equal.</returns>
        public override bool EqualityComparison(Data data)
        {
            if (data.Type == DataType.Engine)
            {
                return this == data;
            }
            else
            {
                return false;
            }
        }
    }
}
